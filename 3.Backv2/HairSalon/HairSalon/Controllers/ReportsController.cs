﻿using HairSalon.Common;
using System.Configuration;
using Telerik.Reporting.Cache.File;
using Telerik.Reporting.Services;
using Telerik.Reporting.Services.WebApi;

namespace HairSalon.Controllers
{
    public class ReportsController : ReportsControllerBase
    {
        static ReportServiceConfiguration configurationInstance;

        public ReportsController()
        {
            var resolver = new CustomReportResolver();
            string reportTemplate = ConfigurationSettings.AppSettings["ReportTemplate"];
            configurationInstance = new ReportServiceConfiguration
            {
                HostAppId = reportTemplate,
                Storage = new FileStorage(),
                ReportResolver = resolver,
                // ReportSharingTimeout = 0,
                // ClientSessionTimeout = 15,
            };
            this.ReportServiceConfiguration = configurationInstance;
        }
    }
}