﻿using HairSalon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HairSalon.Controllers
{
    public class BaseController : ApiController
    {
        public  HairSalonContext context;

        public BaseController()
        {
            if (context == null)
            {
                context =  new HairSalonContext();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}