﻿using HairSalon.Common;
using HairSalon.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/report")]
    public class ReportForTransactionController : BaseController
    {
        [Route("rpt-transaction"), HttpPost]
        public IHttpActionResult RptTransaction(BaseRereportRequest request)
        {
            decimal salonId = Utility.SalonId();
            var fromDate = request.FromDate;
            var toDate = request.ToDate;
            var transactions = context.Transactions.Where(e => e.SalonId == salonId
              && e.PostedTime.HasValue
              && DbFunctions.TruncateTime(e.PostedTime.Value) >= fromDate
              && DbFunctions.TruncateTime(e.PostedTime.Value) <= toDate)
              .Join(context.TransactionCodes, t => t.TransactionCode, tc => tc.Code, (t, tc) => new
              {
                  transaction = t,
                  config = tc
              }).ToList()
              .Select(e => new
              {
                  e.transaction.TransactionId,
                  e.transaction.BillId,
                  PostedTime = e.transaction.PostedTime.Value.ToLocalTime(),
                  e.transaction.PostedUser,
                  e.transaction.TransactionCode,
                  e.config.Name,
                  Amount = e.transaction.Amount ?? 0,
                  e.transaction.Note
              }).OrderBy(e => e.BillId).ToList();
            var sum = transactions.GroupBy(g => new { g.TransactionCode, g.Name })
                .Select(t => new
                {
                    t.Key.TransactionCode,
                    t.Key.Name,
                    TotalAmount = t.Sum(e => e.Amount)
                }).ToList();
            return Ok(new
            {
                transactions = transactions,
                sumTransactions = sum,
            });
        }
    }
}