﻿using HairSalon.Common;
using HairSalon.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/salary")]
    public class SalaryController : BaseController
    {

        [Route("bill-by-staff")]
        public IHttpActionResult BillByStaff(BillByStaffRequest request)
        {
            decimal salonId = Utility.SalonId();
            List<Bill> bill = context.Bills.Where(e => e.SalonId == salonId
                                          && e.ArrivalDate >= request.FromDate
                                          && e.ArrivalDate <= request.ToDate
                                          && (e.StaffMain == request.StaffId
                                              || e.StaffSub == request.StaffId
                                              || e.StaffShampoo == request.StaffId)).ToList();
            List<decimal> billIds = bill.Select(e => e.BillId).ToList();
            List<BillDetail> billDetailFromContext = context.BillDetails.Where(e => e.BillId.HasValue
                            && billIds.Contains(e.BillId.Value)).ToList();

            List<decimal?> staffMainIds = bill.Select(e => e.StaffMain).ToList();
            List<decimal?> staffSubIds = bill.Select(e => e.StaffSub).ToList();
            List<decimal?> staffShamppoIds = bill.Select(e => e.StaffShampoo).ToList();
            List<Staff> staffFromContext = context.Staffs.Where(e => staffMainIds.Contains(e.StaffId)
                        || staffSubIds.Contains(e.StaffId)
                        || staffShamppoIds.Contains(e.StaffId)).ToList();
            var result = bill.Select(e => new
            {
                e.BillId,
                e.ArrivalDate,
                e.GuestName,
                e.StaffMain,
                StaffMainName = staffFromContext.FirstOrDefault(s => s.StaffId == e.StaffMain)?.Name,
                e.StaffSub,
                StaffSubName = staffFromContext.FirstOrDefault(s => s.StaffId == e.StaffSub)?.Name,
                e.StaffShampoo,
                StaffShampooName = staffFromContext.FirstOrDefault(s => s.StaffId == e.StaffShampoo)?.Name,
                BillAmount = billDetailFromContext.Where(d => d.BillId == e.BillId).Sum(d => d.Price * d.Quantity),
            });
            return Ok(result);
        }

        [Route("calculate-commission")]
        public IHttpActionResult CalculateCommission(CalculateSalaryRequest request)
        {
            var salonId = Utility.SalonId();
            var result = context.CalculateCommission(request.FromDate, request.ToDate, request.StaffId, salonId);
            return Ok(result);
        }

        [Route("initdata"),HttpGet]
        public IHttpActionResult InitData()
        {
            var salonId = Utility.SalonId();
            var staffs = context.Staffs.Where(e => e.SalonId == salonId && (e.IsActive ?? false));
            return Ok(new
            {
                staffs = staffs
            });
        }
    }
}