﻿using HairSalon.Common;
using HairSalon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/timeline")]
    public class TimelineController : BaseController
    {
        [Route("cancel/{billId}"), HttpPost]
        public IHttpActionResult CancelBill(decimal billId)
        {
            var bill = context.Bills.FirstOrDefault(e => e.BillId == billId);
            if (bill == null)
            {
                return Ok(ApiResponse.Response(322, "Không tìm thấy hóa đơn!"));
            }
            bill.Status = 0;
            bill.CancelTime = DateTime.UtcNow;
            bill.CancelUser = Utility.User();
            context.SaveChanges();
            return Ok(ApiResponse.Response(Message: "Cập nhật dữ liệu thành công!"));
        }

        [Route("checkin/{billId}"), HttpPost]
        public IHttpActionResult CheckinBill(decimal billId)
        {
            var bill = context.Bills.FirstOrDefault(e => e.BillId == billId);
            if (bill == null)
            {
                return Ok(ApiResponse.Response(322, "Không tìm thấy hóa đơn!"));
            }
            bill.Status = 2;
            bill.CheckinTime = DateTime.UtcNow;
            bill.CheckinUser = Utility.User();
            context.SaveChanges();
            return Ok(ApiResponse.Response(Message: "Cập nhật dữ liệu thành công!"));
        }

        [Route("updatebill"), HttpPost]
        public IHttpActionResult UpdateBill(UpdateBillRequest request)
        {
            var bill = context.Bills.FirstOrDefault(e => e.BillId == request.BillId);
            if (bill == null)
            {
                return Ok(ApiResponse.Response(322, "Không tìm thấy hóa đơn!"));
            }
            bill.ArrivalDate = request.ArrivalDate;
            bill.ArrivalTime = request.ArrivalTime;
            bill.DepartureTime = request.DepartureTime;
            bill.StaffMain = request.StaffId;
            bill.LastChangetime = DateTime.UtcNow;
            bill.LastChangeUser = Utility.User();
            context.SaveChanges();
            return Ok(ApiResponse.Response(Message: "Cập nhật dữ liệu thành công!"));
        }

        [Route("bills")]
        public IHttpActionResult GetBill()
        {
            decimal salonId = Utility.SalonId();
            var reuslt = context.Bills.Where(e => e.SalonId == salonId).ToList();
            return Ok(reuslt);
        }

        [Route("staffs"), HttpGet]
        public IHttpActionResult GetStaff()
        {
            decimal salonId = Utility.SalonId();
            List<Staff> result = context.Staffs.Where(e => e.SalonId == salonId && e.StaffGroupId == 1).ToList();
            return Ok(result);
        }
    }
}