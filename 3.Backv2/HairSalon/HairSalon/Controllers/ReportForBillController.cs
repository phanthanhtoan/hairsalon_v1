﻿using HairSalon.Common;
using HairSalon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HairSalon.Controllers
{

    [Authorize]
    [RoutePrefix("api/report")]
    public class ReportForBillController : BaseController
    {

        [Route("bill-commission"), HttpPost]
        public IHttpActionResult RptBillCommission(BaseRereportRequest request)
        {
            var salonId = Utility.SalonId();
            var result = context.Revenue(request.FromDate, request.ToDate, salonId).ToList();
            return Ok(result);
        }

        [Route("bill-detail/{billId}"), HttpGet]
        public IHttpActionResult RptBillDetail(decimal billId)
        {
            var result = context.BillDetails.Where(e => e.BillId == billId)
                .Join(context.ItemLists, b => b.ItemId, i => i.ItemId, (b, i) => new
                {
                    b.ItemId,
                    i.ItemCode,
                    i.ItemName,
                    b.Quantity,
                    b.Price
                })
                .ToList();
            return Ok(result);
        }

        [Route("rptbill"), HttpPost]
        public IHttpActionResult RptBill(BaseRereportRequest request)
        {
            decimal salonId = Utility.SalonId();
            List<Bill> bill = context.Bills.Where(e => e.SalonId == salonId
                                          && e.ArrivalDate >= request.FromDate
                                          && e.ArrivalDate <= request.ToDate).ToList();
            List<decimal> billIds = bill.Select(e => e.BillId).ToList();
            List<BillDetail> billDetailFromContext = context.BillDetails.Where(e => e.BillId.HasValue
                            && billIds.Contains(e.BillId.Value)).ToList();

            List<decimal?> staffMainIds = bill.Select(e => e.StaffMain).ToList();
            List<decimal?> staffSubIds = bill.Select(e => e.StaffSub).ToList();
            List<decimal?> staffShamppoIds = bill.Select(e => e.StaffShampoo).ToList();
            List<Staff> staffFromContext = context.Staffs.Where(e => staffMainIds.Contains(e.StaffId)
                        || staffSubIds.Contains(e.StaffId)
                        || staffShamppoIds.Contains(e.StaffId)).ToList();
            var result = bill.Select(e => new
            {
                e.BillId,
                e.ArrivalDate,
                e.GuestName,
                e.StaffMain,
                StaffMainName = staffFromContext.FirstOrDefault(s => s.StaffId == e.StaffMain)?.Name,
                e.StaffSub,
                StaffSubName = staffFromContext.FirstOrDefault(s => s.StaffId == e.StaffSub)?.Name,
                e.StaffShampoo,
                StaffShampooName = staffFromContext.FirstOrDefault(s => s.StaffId == e.StaffShampoo)?.Name,
                BillAmount = billDetailFromContext.Where(d => d.BillId == e.BillId).Sum(d => d.Price * d.Quantity),
            });
            return Ok(result);
        }
    }
}