﻿using HairSalon.Common;
using HairSalon.Models;
using System.Linq;
using System.Web.Http;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/staff")]
    public class StaffController : BaseController
    {

        [Route("active"), HttpPost]
        public IHttpActionResult ActiveStaff(Staff staff)
        {
            var staffOld = context.Staffs.Find(staff.StaffId);
            if (staffOld == null)
            {
                return Ok(ApiResponse.Response(322, "Không tồn tại nhân viên!"));
            }
            staffOld.IsActive = !(staffOld.IsActive ?? false);
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }


        [Route("delete"), HttpPost]
        public IHttpActionResult DeleteStaff(Staff staff)
        {
            var staffOld = context.Staffs.Find(staff.StaffId);
            if (staffOld == null)
            {
                return Ok(ApiResponse.Response(322, "Không tồn tại nhân viên!"));
            }
            if (context.Bills.Any(e => e.StaffMain == staff.StaffId || e.StaffSub == staff.StaffId || e.StaffShampoo == staff.StaffId))
            {
                return Ok(ApiResponse.Response(322, "Nhân viên đã làm việc không thể xóa!"));
            }
            context.Staffs.Remove(staffOld);
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }

        [Route("edit"), HttpPost]
        public IHttpActionResult EditStaff(Staff staff)
        {
            var staffOld = context.Staffs.Find(staff.StaffId);
            if (staffOld == null)
            {
                return Ok(ApiResponse.Response(322, "Không tồn tại nhân viên!"));
            }
            //if (context.Bills.Any(e => e.StaffMain == staff.StaffId || e.StaffSub==staff.StaffId ||e.StaffShampoo==staff.StaffId))
            //{
            //    return Ok(ApiResponse.Response(322, "Nhân viên đã làm việc không thể sửa!"));
            //}
            staffOld.Name = staff.Name;
            staffOld.BeginWork = staff.BeginWork;
            staffOld.StaffGroupId = staff.StaffGroupId;
            staffOld.Basicsalary = staff.Basicsalary;
            staffOld.Commission = staff.Commission;
            staffOld.Address = staff.Address;
            staffOld.Phone = staff.Phone;
            staffOld.Facebook = staff.Facebook;
            staffOld.Email = staff.Email;
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }

        [Route("add"), HttpPost]
        public IHttpActionResult AddStaff(Staff staff)
        {
            staff.SalonId = Utility.SalonId();
            staff.IsActive = true;
            context.Staffs.Add(staff);
            context.SaveChanges();
            return Ok(ApiResponse.Response(Data: staff));
        }

        [Route("getall"), HttpGet]
        public IHttpActionResult GetAll()
        {
            var salonId = Utility.SalonId();
            var result = context.Staffs.Where(e => e.SalonId == salonId)
                .Join(context.StaffGroups, s => s.StaffGroupId, sg => sg.StaffGroupId, (s, sg) => new
                {
                    s.StaffId,
                    s.Address,
                    s.Basicsalary,
                    s.BeginWork,
                    s.Code,
                    s.Commission,
                    s.Email,
                    s.Facebook,
                    s.IsActive,
                    s.Name,
                    s.Phone,
                    s.StaffGroupId,
                    s.Website,
                    GroupName = sg.Name,
                    GroupCode = sg.Code,
                }).ToList();
            return Ok(result);
        }
    }
}