﻿using HairSalon.Common;
using HairSalon.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/bill-information")]
    public class BillInformationController : BaseController
    {
        [Route("save")]
        public IHttpActionResult SaveBill(AddBillModel request)
        {
            DateTime now = DateTime.UtcNow;
            var user = Utility.User();
            var bill = context.Bills.FirstOrDefault(e => e.BillId == request.Bill.BillId);
            var requestBill = request.Bill;
            if (bill != null)
            {
                bill.LastChangeUser = Utility.User();
                bill.LastChangetime = DateTime.UtcNow;
                AssignBillInfo(bill, requestBill);
                var requestDetails = request.Details;
                BillDetailAction(bill, requestDetails);

                context.SaveChanges();
            }
            return Ok(ApiResponse.Response());
        }

        [Route("checkin")]
        public IHttpActionResult SaveAndCheckin(AddBillModel request)
        {
            DateTime now = DateTime.UtcNow;
            var user = Utility.User();
            var bill = context.Bills.FirstOrDefault(e => e.BillId == request.Bill.BillId);
            var requestBill = request.Bill;
            if (bill != null)
            {
                AssignBillInfo(bill, requestBill);
                Checkin(bill);
                var requestDetails = request.Details;
                BillDetailAction(bill, requestDetails);

                context.SaveChanges();
            }
            return Ok(ApiResponse.Response());
        }

        private void BillDetailAction(Bill bill, List<BillDetail> requestDetails)
        {
            var newDetails = requestDetails.Where(e => e.BillDetailId == 0).ToList();
            newDetails.ForEach(item =>
            {
                item.BillId = bill.BillId;
            });
            context.BillDetails.AddRange(newDetails);

            var detailExists = requestDetails.Where(e => e.BillDetailId != 0)
                                .ToList();
            var detailIdsExists = detailExists.Select(e => e.BillDetailId).ToList();

            var detailFromContext = context.BillDetails.Where(e => e.BillId == bill.BillId).ToList();
            var detailUpdates = detailFromContext.Where(e => detailIdsExists.Contains(e.BillDetailId)).ToList();
            detailUpdates.ForEach(item =>
            {
                updateDetail(item, detailExists);
            });
            var removeDetails = detailFromContext.Where(e => !detailIdsExists.Contains(e.BillDetailId)).ToList();
            context.BillDetails.RemoveRange(removeDetails);
        }

        private static void updateDetail(BillDetail item, List<BillDetail> detailExists)
        {
            var newValue = detailExists.FirstOrDefault(e => e.BillDetailId == item.BillDetailId);
            item.Quantity = newValue.Quantity;
            item.Price = newValue.Price;
        }

        private void Checkin(Bill bill)
        {
            bill.Status = 2;
            bill.CheckinTime = DateTime.UtcNow;
            bill.CheckinUser = Utility.User();
        }

        private static void AssignBillInfo(Bill bill, Bill requestBill)
        {
            bill.GuestName = requestBill.GuestName;
            bill.GuestPhone = requestBill.GuestPhone;
            bill.ArrivalDate = requestBill.ArrivalDate;
            bill.ArrivalTime = requestBill.ArrivalTime;
            bill.ChairId = requestBill.ChairId;
            bill.DepartureTime = requestBill.DepartureTime;
            bill.StaffMain = requestBill.StaffMain;
            bill.StaffSub = requestBill.StaffSub;
            bill.StaffShampoo = requestBill.StaffShampoo;
            bill.Note = requestBill.Note;
        }

        [Route("getbyid/{BillId}"), HttpGet]
        public IHttpActionResult GetByID(decimal BillId)
        {
            var bill = context.Bills.FirstOrDefault(e => e.BillId == BillId);
            var detail = context.BillDetails.Where(e => e.BillId == BillId)
                .Join(context.ItemLists, b => b.ItemId, i => i.ItemId, (b, i) => new
                {
                    b.BillDetailId,
                    b.ItemId,
                    i.ItemName,
                    i.ItemCode,
                    b.Quantity,
                    b.Price,
                }).ToList();
            return Ok(new
            {
                Bill = bill,
                Details = detail
            });
        }

        [Route("add")]
        public IHttpActionResult AddBill(AddBillModel request)
        {
            DateTime now = DateTime.UtcNow;
            var user = Utility.User();
            var bill = request.Bill;
            var salonId = Utility.SalonId();
            bill.SalonId = salonId;
            bill.CreatedTime = now;
            bill.CreatedUser = user;
            if (bill.Status == 2)
            {
                bill.CheckinTime = now;
                bill.CheckinUser = user;
            }
            context.Bills.Add(bill);
            context.SaveChanges();
            var details = request.Details;
            details.ForEach(item =>
            {
                item.BillId = bill.BillId;
            });
            context.BillDetails.AddRange(details);
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }

        [Route("initdata"), HttpPost]
        public IHttpActionResult InitData(SearchBillRequest request)
        {
            var salonId = Utility.SalonId();
            var items = context.ItemLists.Where(e => e.SalonId == salonId).ToList();
            var staffs = context.Staffs.Where(e => e.SalonId == salonId).ToList();
            var bills = Bills(request);
            var chairs = context.Chairs.Where(e => e.SalonId == salonId).ToList();
            return Ok(new
            {
                ItemLists = items,
                Staff = new
                {
                    StaffMains = staffs.Where(e => e.StaffGroupId == 1).ToList(),
                    StaffSubs = staffs.Where(e => e.StaffGroupId == 2).ToList(),
                    StaffShampoos = staffs.Where(e => e.StaffGroupId == 3).ToList(),
                },
                Bills = bills,
                Chairs = chairs
            });
        }

        [Route("search"), HttpPost]
        public IHttpActionResult SearchBill(SearchBillRequest request)
        {
            List<SearchBillResponse> result = Bills(request);
            return Ok(result);
        }

        private List<SearchBillResponse> Bills(SearchBillRequest request)
        {
            var status = new List<int>();
            if (request.Arrival) status.Add(1);
            if (request.CheckedIn) status.Add(2);
            if (request.CheckedOut) status.Add(3);
            var SalonId = Utility.SalonId();
            var bills = context.Bills.Where(e => e.SalonId == SalonId
                             && DbFunctions.TruncateTime(e.CreatedTime) == request.Date.Value
                             && status.Contains(e.Status))
                            .OrderByDescending(e => e.CreatedTime).ToList();

            var billIds = bills.Select(e => e.BillId).ToList();
            var detailsFromContext = context.BillDetails.Where(e => e.BillId.HasValue
                             && billIds.Contains(e.BillId.Value))
                            .ToList();
            var payment = context.Transactions.Where(e => e.BillId.HasValue
                              && billIds.Contains(e.BillId.Value))
                            .ToList();

            var result = bills.Select(e => new SearchBillResponse
            {
                Amount = detailsFromContext.Where(d => d.BillId == e.BillId).Sum(d => d.Quantity * d.Price) ?? 0,
                Payment = payment.Where(p => p.BillId == e.BillId).Sum(p => p.Amount) ?? 0,
                ArrivalDate = e.ArrivalDate,
                ArrivalTime = e.ArrivalTime,
                BillCode = e.BillCode,
                BillId = e.BillId,
                ChairId = e.ChairId,
                CheckinTime = e.CheckinTime,
                CheckinUser = e.CheckinUser,
                CheckoutTime = e.CheckoutTime,
                CheckoutUser = e.CheckoutUser,
                CreatedTime = e.CreatedTime,
                CreatedUser = e.CreatedUser,
                CustomerId = e.CustomerId,
                DepartureTime = e.DepartureTime,
                DscAmount = e.DscAmount,
                DscPcnt = e.DscPcnt,
                GuestName = e.GuestName,
                GuestPhone = e.GuestPhone,
                Note = e.Note,
                SalonId = e.SalonId,
                StaffMain = e.StaffMain,
                StaffShampoo = e.StaffShampoo,
                StaffSub = e.StaffSub,
                Status = e.Status
            }).ToList();
            return result;
        }
    }
}