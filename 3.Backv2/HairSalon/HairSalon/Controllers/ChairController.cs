﻿using HairSalon.Common;
using HairSalon.Models;
using System.Linq;
using System.Web.Http;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/chair")]
    public class ChairController : BaseController
    {
        [Route("active"), HttpPost]
        public IHttpActionResult ActiveStaff(Chair chair)
        {
            var chairOld = context.Chairs.Find(chair.ChairId);
            if (chairOld == null)
            {
                return Ok(ApiResponse.Response(322, "Ghế không tồn tại!"));
            }
            chairOld.IsActive = !chairOld.IsActive;
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }

        [Route("delete"), HttpPost]
        public IHttpActionResult DeleteStaff(Chair chair)
        {
            var chairOld = context.Chairs.Find(chair.ChairId);
            if (chairOld == null)
            {
                return Ok(ApiResponse.Response(322, "Ghế không tồn tại!"));
            }
            if (context.Bills.Any(e => e.ChairId == chair.ChairId))
            {
                return Ok(ApiResponse.Response(322, "Ghế đã được sử dụng không thể xóa!"));
            }
            context.Chairs.Remove(chairOld);
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }

        [Route("edit"), HttpPost]
        public IHttpActionResult EditStaff(Chair chair)
        {
            var chairOld = context.Chairs.Find(chair.ChairId);
            if (chairOld == null)
            {
                return Ok(ApiResponse.Response(322, "Ghế không tồn tại!"));
            }
            chairOld.ChairCode = chair.ChairCode;
            chairOld.ChairName = chair.ChairName;
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }

        [Route("add"), HttpPost]
        public IHttpActionResult AddStaff(Chair chair)
        {
            chair.SalonId = Utility.SalonId();
            chair.IsActive = true;
            context.Chairs.Add(chair);
            context.SaveChanges();
            return Ok(ApiResponse.Response(Data: chair));
        }

        [Route("getall"), HttpGet]
        public IHttpActionResult GetAll()
        {
            var salonId = Utility.SalonId();
            var result = context.Chairs.Where(e => e.SalonId == salonId).ToList();
            return Ok(result);
        }
    }
}