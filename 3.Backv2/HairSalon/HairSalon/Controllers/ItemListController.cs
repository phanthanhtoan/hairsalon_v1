﻿using HairSalon.Common;
using HairSalon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/item")]
    public class ItemListController : BaseController
    {
        [Route("active"), HttpPost]
        public IHttpActionResult ActiveItem(ItemList item)
        {
            var itemOld = context.ItemLists.Find(item.ItemId);
            if (itemOld == null)
            {
                return Ok(ApiResponse.Response(322, "Không tồn tại sản phẩm!"));
            }
            itemOld.IsActive = !(itemOld.IsActive ?? false);
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }


        [Route("delete"), HttpPost]
        public IHttpActionResult DeleteItem(ItemList item)
        {
            var itemOld = context.ItemLists.Find(item.ItemId);
            if (itemOld == null)
            {
                return Ok(ApiResponse.Response(322, "Không tồn tại sản phẩm!"));
            }
            if (context.BillDetails.Any(e => e.ItemId == item.ItemId))
            {
                return Ok(ApiResponse.Response(322, "Sản phẩm đã bán không thể xóa!"));
            }
            context.ItemLists.Remove(itemOld);
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }

        [Route("edit"), HttpPost]
        public IHttpActionResult EditItem(ItemList item)
        {
            var itemOld = context.ItemLists.Find(item.ItemId);
            if (itemOld == null)
            {
                return Ok(ApiResponse.Response(322, "Không tồn tại sản phẩm!"));
            }
            if (context.BillDetails.Any(e => e.ItemId == item.ItemId))
            {
                return Ok(ApiResponse.Response(322, "Sản phẩm đã bán không thể sửa!"));
            }
            itemOld.ItemName = item.ItemName;
            itemOld.Price = item.Price;
            itemOld.OpenPrice = item.OpenPrice;
            itemOld.UnitId = item.UnitId;
            itemOld.ItemGroupId = item.ItemGroupId;
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }

        [Route("add"), HttpPost]
        public IHttpActionResult AddItem(ItemList item)
        {
            item.SalonId = Utility.SalonId();
            item.IsActive = true;
            context.ItemLists.Add(item);
            context.SaveChanges();
            return Ok(ApiResponse.Response(Data: item));
        }

        [Route("picklist"), HttpGet]
        public IHttpActionResult GetPicklist()
        {
            var units = context.IUnits.ToList();
            var groups = context.IGroups.ToList();
            return Ok(new
            {
                Units = units,
                Groups = groups,
            });
        }

        [Route("getall"), HttpGet]
        public IHttpActionResult GetAll()
        {
            var salonId = Utility.SalonId();
            var result = context.ItemLists.Where(e => e.SalonId == salonId)
                .Join(context.IUnits, i => i.UnitId, u => u.UnitId, (i, u) => new
                {
                    item = i,
                    unit = u
                })
                .Join(context.IGroups, i => i.item.ItemGroupId, g => g.IGroupId, (i, g) => new
                {
                    i.item.ItemId,
                    i.item.ItemCode,
                    i.item.IsActive,
                    i.item.ItemName,
                    i.item.OpenPrice,
                    i.item.Price,
                    i.item.UnitId,
                    i.unit.UnitCode,
                    i.unit.UnitName,
                    i.item.ItemGroupId,
                    g.IGroupCode,
                    IGroupName = g.Name
                })
                .ToList();
            return Ok(result);
        }
    }
}