﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using HairSalon.Common;
using HairSalon.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    public class AccountController : BaseController
    {
        private ApplicationUserManager _userManager;

        public AccountController()
        {

        }

        public AccountController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Route("getall")]
        public IHttpActionResult GetAll()
        {
            var accounts = context.AspNetUsers.Select(e => new
            {
                e.UserName,
                e.Email,
                e.LockoutEnabled
            }).ToList();
            return Ok(accounts);
        }

        [AllowAnonymous]
        [Route("register"), HttpPost]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(ApiResponse.Response((int)HttpStatusCode.BadRequest, Data: ModelState));
            }

            var user = new ApplicationUser()
            {
                UserName = model.UserName,
                Email = model.Email
            };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok(ApiResponse.Response());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    return Ok(ApiResponse.Response((int)HttpStatusCode.BadRequest));
                }
                return Ok(ApiResponse.Response((int)HttpStatusCode.BadRequest, Data: ModelState));
            }
            return Ok(ApiResponse.Response(0));
        }
        #endregion
    }
}
