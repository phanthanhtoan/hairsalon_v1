﻿using HairSalon.Common;
using HairSalon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using static HairSalon.Common.ApiResponse;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/email")]
    public class EmailConfigController:BaseController
    {
        [Route("test"), HttpPost]
        public IHttpActionResult TestEmail(TestEmailRequest request)
        {
            try
            {
                ResponseModel send = SendEmail(request.EmailReceive, "Subject - Kiểm tra Email", "Body -  Kiểm tra Email");
                return Ok(send);
            }
            catch (Exception ex)
            {
                return Ok(Response(322, "Exception", ex));
            }
        }

        private ResponseModel SendEmail(string EmailReceive, string Subject, string Body)
        {
            decimal salonId = Utility.SalonId();
            EmailConfig oldConfig = context.EmailConfigs.FirstOrDefault(e => e.SalonId == salonId);
            if (oldConfig == null)
            {
                return Response(322, "Bạn chưa cấu hình email gửi");
            }

            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(oldConfig.EmailName, "Display Name");
                mailMessage.To.Add(EmailReceive);
                mailMessage.Subject = Subject;
                mailMessage.Body = Body;

                SmtpClient smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    Credentials = new NetworkCredential
                    {
                        UserName = oldConfig.EmailName,
                        Password = oldConfig.Password,
                    }
                };
                smtp.Send(mailMessage);
            }
            return Response();
        }

        [Route("config")]
        public IHttpActionResult ConfigEmail(EmailConfig email)
        {
            decimal salonId = Utility.SalonId();
            EmailConfig oldConfig = context.EmailConfigs.FirstOrDefault(e => e.SalonId == salonId);
            if (oldConfig != null)
            {
                oldConfig.EmailName = email.EmailName;
                oldConfig.Password = email.Password;
            }
            else
            {
                var newEmail = new EmailConfig
                {
                    EmailName = email.EmailName,
                    Password = email.Password,
                    SalonId = salonId
                };
                context.EmailConfigs.Add(newEmail);
            }
            context.SaveChanges();
            return Ok(Response(Message : "Cập nhật cấu hình email thành công!"));
        }

        [Route("get"),HttpGet]
        public IHttpActionResult GetEmail()
        {
            decimal salonId = Utility.SalonId();
            EmailConfig result = context.EmailConfigs.FirstOrDefault(e=>e.SalonId==salonId);
            return Ok(result);
        }
    }
}