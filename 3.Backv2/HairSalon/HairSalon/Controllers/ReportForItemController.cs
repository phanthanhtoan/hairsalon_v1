﻿using HairSalon.Common;
using HairSalon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HairSalon.Controllers
{
    [Authorize]
    [RoutePrefix("api/report")]
    public class ReportForItemController : BaseController
    {
        [Route("rpt-item-sale"), HttpPost]
        public IHttpActionResult RptItemSale(BaseRereportRequest request)
        {
            decimal salonId = Utility.SalonId();
            List<Bill> bill = context.Bills.Where(e => e.SalonId == salonId
                                          && e.ArrivalDate >= request.FromDate
                                          && e.ArrivalDate <= request.ToDate).ToList();
            List<decimal> billIds = bill.Select(e => e.BillId).ToList();
            var result = context.BillDetails.Where(e => e.BillId.HasValue
                            && billIds.Contains(e.BillId.Value))
                .Join(context.ItemLists, d => d.ItemId, i => i.ItemId, (d, i) => new
                {
                    detail = d,
                    item = i
                }).GroupBy(e => new
                {
                    e.detail.ItemId,
                    e.detail.Price,
                    e.item.ItemCode,
                    e.item.ItemName,
                }).Select(e => new
                {
                    e.Key.ItemId,
                    e.Key.ItemCode,
                    e.Key.ItemName,
                    e.Key.Price,
                    TotalQuantity = e.Sum(t => t.detail.Quantity)
                })
                .ToList();
            return Ok(result);
        }
    }
}