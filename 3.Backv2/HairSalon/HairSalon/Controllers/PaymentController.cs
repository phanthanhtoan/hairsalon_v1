﻿using HairSalon.Common;
using HairSalon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HairSalon.Controllers
{

    [RoutePrefix("api/payment")]
    public class PaymentController : BaseController
    {
        [Route("get-payment/{billId}"), HttpGet]
        public IHttpActionResult GetPayment(decimal billId)
        {
            decimal balance = GetBalance(billId);
            var transactionCodes = context.TransactionCodes.ToList();
            return Ok(new
            {
                Balance = balance,
                TransactionCodes = transactionCodes
            });
        }

        private decimal GetBalance(decimal billId)
        {
            decimal balance = context.BillDetails.Where(e => e.BillId == billId).Sum(e => e.Price * e.Quantity) ?? 0;
            return balance;
        }

        [Route("add"), HttpPost]
        public IHttpActionResult Add(Transaction transaction)
        {
            var user = Utility.User();
            transaction.SalonId = Utility.SalonId();
            transaction.PostedUser = user;
            transaction.PostedTime = DateTime.UtcNow;
            var bill = context.Bills.Find(transaction.BillId);
            if (bill != null)
            {
                bill.Status = 3;
                bill.CheckoutUser = user;
                bill.CheckoutTime = DateTime.UtcNow;
            }
            context.Transactions.Add(transaction);
            context.SaveChanges();
            return Ok(ApiResponse.Response());
        }
    }
}