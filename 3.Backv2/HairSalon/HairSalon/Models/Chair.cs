//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HairSalon.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Chair
    {
        public decimal ChairId { get; set; }
        public string ChairCode { get; set; }
        public string ChairName { get; set; }
        public bool IsActive { get; set; }
        public int ChairTop { get; set; }
        public int ChairLeft { get; set; }
        public int ChairWidth { get; set; }
        public int ChairHeight { get; set; }
        public Nullable<decimal> SalonId { get; set; }
    }
}
