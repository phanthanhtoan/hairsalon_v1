﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HairSalon.Models
{
    public class BaseRereportRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}