﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HairSalon.Models
{
    public class ItemListModel
    {
        public decimal ItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<bool> OpenPrice { get; set; }
        public Nullable<decimal> UnitId { get; set; }
        public Nullable<decimal> ItemGroupId { get; set; }
        public Nullable<decimal> SalonId { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}