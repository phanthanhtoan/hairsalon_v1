﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HairSalon.Models
{
    public class SearchBillRequest
    {
        public Nullable<DateTime> Date { get; set; }
        public bool Arrival { get; set; }
        public bool CheckedIn { get; set; }
        public bool CheckedOut { get; set; }
    }

    public class SearchBillResponse:Bill
    {
        public decimal Amount { get; set; }
        public decimal Payment { get; set; }
    }
}