﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HairSalon.Models
{
    public class AddBillModel
    {
        public Bill Bill { get; set; }
        public List<BillDetail> Details { get; set; }
    }
}