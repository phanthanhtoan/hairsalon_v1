//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HairSalon.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class IGroup
    {
        public decimal IGroupId { get; set; }
        public string IGroupCode { get; set; }
        public string Name { get; set; }
        public bool IsActice { get; set; }
        public Nullable<decimal> SalonId { get; set; }
    }
}
