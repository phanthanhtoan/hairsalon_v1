﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HairSalon.Models
{
    public class CalculateSalaryRequest
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Nullable<decimal> StaffId { get; set; }
    }

    public class BillByStaffRequest
    {
        public Nullable<decimal> StaffId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}