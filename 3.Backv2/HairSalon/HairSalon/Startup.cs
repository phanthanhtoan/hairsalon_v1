﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(HairSalon.Startup))]
namespace HairSalon
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}