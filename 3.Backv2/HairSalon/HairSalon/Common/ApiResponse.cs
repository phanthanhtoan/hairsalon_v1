﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HairSalon.Common
{
    public class ApiResponse
    {
        public static ResponseModel Response(int Status = 200, string Message = "Thành công", object Data = null)
        {
            return new ResponseModel()
            {
                Status = Status,
                MessageText = Message,
                Data = Data == null ? new { } : Data
            };
        }
        public class ResponseModel
        {
            public int Status { get; set; }
            public string MessageText { get; set; }
            public Object Data { get; set; }
        }
    }
}