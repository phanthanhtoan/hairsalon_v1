﻿using Microsoft.AspNet.Identity.Owin;
using System.IO;
using System.Web;
using Telerik.Reporting;

namespace HairSalon.Common
{
    public class CustomReportResolver : Telerik.Reporting.Services.Engine.IReportResolver
    {
        public Telerik.Reporting.ReportSource Resolve(string reportId)
        {
            //custom method you need to implement to get a report in XML or as Telerik.Reporting.Report object
            //in this example reportId will be 'GetReport1'
            var appPath = HttpContext.Current.Server.MapPath("/");
            var reportsPath = Path.Combine(appPath, @"Reports");

            var reportPackager = new ReportPackager();
            using (var sourceStream = System.IO.File.OpenRead(reportsPath + "/" + reportId))
            {
                var report = (Report)reportPackager.UnpackageDocument(sourceStream);

                //get the information you need and pass it in the report
                var userName = HttpContext.Current.User.Identity.Name;
                var userManager = HttpContext.Current.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = userManager.FindByNameAsync(userName);
                if (user != null)
                {
                    if (report.ReportParameters.Contains("UserName"))
                    {
                        report.ReportParameters["UserName"].Value = userName;
                        report.ReportParameters["UserName"].Visible = false;
                    }
                    if (report.ReportParameters.Contains("UserId"))
                    {
                        report.ReportParameters["UserId"].Value = user.Id;
                        report.ReportParameters["UserId"].Visible = false;
                    }
                }
                var salonId = Utility.SalonId();
                report.ReportParameters["SalonId"].Value = salonId;
                report.ReportParameters["SalonId"].Visible = false;
                //return a valid report source
                return new InstanceReportSource { ReportDocument = report };
            }

        }

    }
}