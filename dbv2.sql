USE [HairSalon]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[BillId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BillCode] [varchar](20) NULL,
	[Arrivaldate] [datetime] NULL,
	[ArrivalTime] [datetime] NULL,
	[DepartureTime] [datetime] NULL,
	[DscPcnt] [numeric](5, 2) NULL,
	[DscAmount] [numeric](18, 0) NULL,
	[Note] [nvarchar](500) NULL,
	[CreatedTime] [datetime] NULL,
	[CreatedUser] [varchar](10) NULL,
	[CheckinTime] [datetime] NULL,
	[CheckinUser] [varchar](10) NULL,
	[CheckoutTime] [datetime] NULL,
	[CheckoutUser] [varchar](10) NULL,
	[Status] [int] NOT NULL,
	[ChairId] [numeric](18, 0) NULL,
	[GuestName] [nvarchar](100) NULL,
	[GuestPhone] [varchar](20) NULL,
	[CustomerId] [numeric](18, 0) NULL,
	[StaffMain] [numeric](18, 0) NULL,
	[StaffShampoo] [numeric](18, 0) NULL,
	[StaffSub] [numeric](18, 0) NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillDetail]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillDetail](
	[BillDetailId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BillId] [numeric](18, 0) NULL,
	[ItemId] [numeric](18, 0) NULL,
	[Price] [numeric](18, 0) NULL,
	[Quantity] [int] NULL,
	[DetailNote] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Chair]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chair](
	[ChairId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ChairCode] [varchar](20) NULL,
	[ChairName] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[ChairTop] [int] NOT NULL,
	[ChairLeft] [int] NOT NULL,
	[ChairWidth] [int] NOT NULL,
	[ChairHeight] [int] NOT NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[ChairId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IGroup]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IGroup](
	[IGroupId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IGroupCode] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActice] [bit] NOT NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[IGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemList]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemList](
	[ItemId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ItemCode] [varchar](20) NULL,
	[ItemName] [nvarchar](100) NULL,
	[Price] [numeric](18, 0) NULL,
	[OpenPrice] [bit] NULL,
	[UnitId] [numeric](18, 0) NULL,
	[ItemGroupId] [numeric](18, 0) NULL,
	[SalonId] [numeric](18, 0) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IUnit]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IUnit](
	[UnitId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UnitCode] [varchar](20) NULL,
	[UnitName] [nvarchar](100) NULL,
	[IsActice] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[LocationId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[LocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Salon]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salon](
	[SalonId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[Address] [nvarchar](100) NULL,
	[Phone] [varchar](20) NULL,
	[Email] [nvarchar](100) NULL,
	[Facebook] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[SalonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Source]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Source](
	[SourceId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[SourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[StaffId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[Address] [nvarchar](100) NULL,
	[Phone] [varchar](20) NULL,
	[Email] [nvarchar](100) NULL,
	[Facebook] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
	[StaffGroupId] [numeric](18, 0) NULL,
	[Basicsalary] [numeric](18, 0) NULL,
	[Commission] [numeric](5, 2) NULL,
	[BeginWork] [datetime] NULL,
	[IsActive] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[StaffId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StaffGroup]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StaffGroup](
	[StaffGroupId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[StaffGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[TransactionId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BillId] [numeric](18, 0) NULL,
	[TransactionCode] [varchar](20) NULL,
	[Amount] [numeric](18, 0) NULL,
	[Note] [nvarchar](200) NULL,
	[PostedUser] [varchar](20) NULL,
	[PostedTime] [datetime] NULL,
	[LastChangeUser] [varchar](20) NULL,
	[LastChangeTime] [datetime] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransactionCode]    Script Date: 5/16/2021 2:20:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionCode](
	[TrnCodeId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[Type] [int] NULL,
	[IsActive] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[TrnCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[IGroup] ON 

INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(1 AS Numeric(18, 0)), N'CAT', N'Cắt', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(2 AS Numeric(18, 0)), N'NHUOM', N'Nhuộm', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(3 AS Numeric(18, 0)), N'UON', N'Uốn', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(4 AS Numeric(18, 0)), N'GOI', N'Gội', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(5 AS Numeric(18, 0)), N'HAP', N'Hấp', 1, CAST(1 AS Numeric(18, 0)))
SET IDENTITY_INSERT [dbo].[IGroup] OFF
GO
SET IDENTITY_INSERT [dbo].[IUnit] ON 

INSERT [dbo].[IUnit] ([UnitId], [UnitCode], [UnitName], [IsActice], [SalonId]) VALUES (CAST(1 AS Numeric(18, 0)), N'Luot', N'Lượt', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IUnit] ([UnitId], [UnitCode], [UnitName], [IsActice], [SalonId]) VALUES (CAST(2 AS Numeric(18, 0)), N'Chai', N'Chai', 1, CAST(1 AS Numeric(18, 0)))
SET IDENTITY_INSERT [dbo].[IUnit] OFF
GO
SET IDENTITY_INSERT [dbo].[Salon] ON 

INSERT [dbo].[Salon] ([SalonId], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website]) VALUES (CAST(1 AS Numeric(18, 0)), N'FAMILY', N'Hair Salon Family', N'Ngô Xá - Cẩm Khê - Phú Thọ', N'0963096426', N'family@gmail.com', N'family.FaceBook.com', N'www.family.com')
SET IDENTITY_INSERT [dbo].[Salon] OFF
GO
