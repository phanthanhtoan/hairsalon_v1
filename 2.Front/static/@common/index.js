const mixin = {
	computed: {
		isLoggedIn() {
			return this.$store.getters['user/isLoggedIn'];
		},
		userName() {
			return this.$store.getters['user/userName'];
		},
	},
	methods: {
		getColorByStatus(status) {
			if (status === 0) {
				return '#495060';
			}
			if (status === 1) {
				return '#2b85e4';
			}
			if (status === 2) {
				return 'green';
			}
			return 'red';
		},
		$renderCheckbox(fieldValue) {
			fieldValue = fieldValue || false;
			let iconClass = 'fa fa-square-o';
			let iconStyle = 'font-size: 18px;';
			if (typeof fieldValue === 'boolean' && fieldValue) {
				iconClass = 'fa fa-check-square';
				iconStyle += 'color: #3c74b0';
			}
			const props = {
				style: iconStyle,
				class: iconClass,
			};
			return (
				<span>
					<i {...props} />
				</span>
			);
		},
		isEmpty(data) {
			return data === null || data === '' || typeof data === 'undefined';
		},
		focusAmountFromInput(input) {
			if (input === 0 || input === null || input === '' || input * 1 === 0) {
				return '';
			}
			while (input.indexOf(',') !== -1) {
				input = input.replace(',', '');
			}
			return input;
		},
		getAmountFromInput(input) {
			if (input !== null && input !== '') {
				input = Number(input.replace(/[^0-9.-]+/g, ''));
				if (isNaN(input)) {
					this.$m('Vui lòng nhập số!');
				} else {
					return input;
				}
			}
			return '';
		},
		$m(message = '', type = MessageType.Success) {
			const self = this;
			const vueIziToastDefaultOptions = {
				position: 'bottomCenter',
			};
			switch (type) {
				case MessageType.Success:
					self.$toast.success(message, 'Success', vueIziToastDefaultOptions);
					break;
				case MessageType.Warning:
					self.$toast.warning(message, 'Warning', vueIziToastDefaultOptions);
					break;
				case MessageType.Error:
					self.$toast.error(message, 'Error', vueIziToastDefaultOptions);
					break;
				default:
					break;
			}
		},
		$deepClone(data) {
			// primitive values
			if (typeof data !== 'object' || data === null) {
				return data;
			}

			// date object
			if (data instanceof Date) {
				const d = new Date(data.getTime());
				return d;
			}
			// Array
			if (Array.isArray(data)) {
				const n = data.length;
				const result = [];
				for (let i = 0; i < n; i += 1) {
					const item = data[i];
					result.push(this.$deepClone(item));
				}
				return result;
			}
			// Object
			if (data instanceof Object) {
				const n = Object.keys(data).length;
				const result = {};
				for (let i = 0; i < n; i += 1) {
					const key = Object.keys(data)[i];
					result[key] = this.$deepClone(data[key]);
				}
				return result;
			}
			return null;
		},
		format_curency(number) {
			if (typeof number !== 'undefined') {
				if (isNaN(number)) {
					return number.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
				}
				return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
			}
			return '';
		},
	},
};

export default mixin;
