const MessageType = {
	Success: 0,
	Warning: 1,
	Error: 2,
};
Object.freeze({
	MessageType,
});
