function initFreshChat(dataConfigChat) {

    window.fcWidget.init({
        token: dataConfigChat.ModuleSecurity,
        host: "https://wchat.freshchat.com",
        locale: convertLanguage(dataConfigChat.Language),
        externalId: dataConfigChat.UserId + '_' + dataConfigChat.HotelId,
        restoreId: dataConfigChat.RestoreId === '0' ? null : dataConfigChat.RestoreId,
        open: false,
        config: {
            headerProperty: {
                direction: 'ltr',
                appName: 'ezFolio',
                appLogo: '',
            },
            cssNames: {
	            widget: "custom_fc_frame",
            },
        }
    });

    window.fcWidget.user.setProperties({
        firstName: dataConfigChat.FullName,
        email: dataConfigChat.UserEmail,
        phone: dataConfigChat.HotelPhone,
        locale: convertLanguage(dataConfigChat.Language),
        HotelId: dataConfigChat.HotelId,
        HotelName: dataConfigChat.HotelName,
        Product: 'ezFolio'
    });

    window.fcWidget.on('user:created', function (resp) {
        var status = resp && resp.status,
            data = resp && resp.data;
        if (status === 200 && data.restoreId) {
            var dataPost = {
                UserId: dataConfigChat.UserId,
                RestoreId: data.restoreId
            };
            $.ajax({
                url: dataConfigChat.apiUrl + "/api/Account/FreshChatSetRestoreId",
                method: "POST",
                crossDomain: true,
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Authorization': 'Bearer ' + dataConfigChat.accessToken,
                },
                xhrFields: {
                    withCredentials: true
                },
                data: JSON.stringify(dataPost),
                success: function (data, textStatus, jQxhr) {

                },
                error: function (jqXhr, textStatus, errorThrown) {

                }
            });
        }
    });
}
function initializeFc(i,t, dataConfigChat) {
    var e;
    i.getElementById(t)?initFreshChat(dataConfigChat):((e=i.createElement("script")).id=t,e.async=!0,e.src="/static/asset/js/fc-widget.js",e.onload=initFreshChat(dataConfigChat),i.head.appendChild(e))
}
function initiateCall(dataConfigChat) {
    initializeFc(document, "freshchat-js-sdk", dataConfigChat)
}
function destroyWidgetChat() {
    window.fcWidget.destroy();
}
function convertLanguage(selectedLanguage) {
    if (selectedLanguage === 'en-gb') return 'en';
    if (selectedLanguage === 'vi-vn') return 'vi';
    return selectedLanguage;
}
function changeWidgetLanguage(selectedLanguage) {
    selectedLanguage = convertLanguage(selectedLanguage);
    window.fcWidget.user.setLocale(selectedLanguage);
}
function showWidgetChat(show, dataConfigChat) {
    if(show) {
        initiateCall(dataConfigChat);
    }
    else {
        destroyWidgetChat();
    }
}
function IsInitializedWidgetChat() {
    return window.fcWidget.isInitialized();
}