// @vue/component
import cpns from './@function';

export default {
	name: 'Bill',
	components: cpns,
	data() {
		return {
			dataBill: [],
			titleNewChangeBill: 'titleNewChangeBill',
		};
	},
	methods: {
		clickNewBill() {
			this.$refs.refNewChangeBill.show();
		},
	},
	computed: {
		colBill() {
			return [
				{
					field: 'test',
					key: '1',
					title: '',
					width: 50,
					align: 'center',
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						console.log(row, column, rowIndex, h);
						return ++rowIndex;
					},
				},
				{
					field: 'name',
					key: '2',
					title: 'Name',
					align: 'left',
					width: '20%',
				},
				{
					field: 'email',
					key: '3',
					title: 'email',
					align: 'left',
				},
			];
		},
	},
};
