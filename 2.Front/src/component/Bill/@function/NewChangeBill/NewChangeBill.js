// @vue/component
export default {
  name: 'NewChangeBill',
  data() {
    return {
      bill: {
        ArrivalDate: new Date(),
      },
      search_data: [],
      base: [
        {
          id: 1,
          Code: 'AF',
          Name: 'Afghanistan',
        },
        {
          id: 2,
          Code: 'AL',
          Name: 'Albania',
        },
        {
          id: 3,
          Code: 'toanpt',
          Name: 'a',
        },
      ],
      colDataSearch: [
        {
          field: 'test',
          key: '1',
          title: '',
          width: 50,
          align: 'center',
          renderBodyCell: ({ row, column, rowIndex }, h) => {
            return ++rowIndex;
          },
        },
        {
          field: 'Code',
          key: '2',
          title: 'Code',
          align: 'left',
          width: '20%',
        },
        {
          field: 'Name',
          key: '3',
          title: 'Name',
          align: 'left',
        },
      ],
    };
  },
  methods: {
    getData() {
      console.log(this.bill.GuestName, this.search_data);
      const a = this.base.filter(
        (item) =>
          this.bill.GuestName.toString().toLowerCase().includes(item.Name.toLowerCase()) ||
          item.Name.toLowerCase().includes(this.bill.GuestName.toString().toLowerCase()),
      );
      this.search_data = a;
    },
    getName(e) {
      console.log(e);
    },
  },
};
