import { UserMutationTypes } from '../../store/module/user';

const menu = [
	{
		menu_name: 'Timeline',
		menu_link: '/timeline',
		menu_permission: ['ALL'],
		// sub_menus: [{ menu_name: 'Danh sách', menu_link: '/billinformation', menu_permission: ['ALL'] }],
	},
	{
		menu_name: 'Hóa đơn',
		menu_link: '/billinformation',
		menu_permission: ['ALL'],
		// sub_menus: [{ menu_name: 'Danh sách', menu_link: '/billinformation', menu_permission: ['ALL'] }],
	},
	{
		menu_name: 'Cấu hình',
		menu_link: '/setting',
		menu_permission: ['ALL'],
		sub_menus: [
			{ menu_name: 'Nhân viên', menu_link: '/staff', menu_permission: ['ALL'] },
			{ menu_name: 'Sản phẩm', menu_link: '/itemlist', menu_permission: ['ALL'] },
			{ menu_name: 'Ghế', menu_link: '/chair', menu_permission: ['ALL'] },
			{ menu_name: 'Người dùng', menu_link: '/account', menu_permission: ['ALL'] },
			{ menu_name: 'Email', menu_link: '/email', menu_permission: ['ALL'] },
		],
	},
	{
		menu_name: 'Báo cáo',
		menu_link: '/report',
		menu_permission: ['ALL'],
		sub_menus: [
			{ menu_name: 'Hóa đơn', menu_link: '/rptbill', menu_permission: ['ALL'] },
			{ menu_name: 'Dịch vụ', menu_link: '/rptitemsale', menu_permission: ['ALL'] },
			{ menu_name: 'Thanh toán', menu_link: '/rpttransaction', menu_permission: ['ALL'] },
			{ menu_name: 'Hoa hồng', menu_link: '/salary', menu_permission: ['ALL'] },
			{ menu_name: 'Doanh thu', menu_link: '/rptrevenue', menu_permission: ['ALL'] },
		],
	},
];
function pad(num, size) {
	let s = `${num}`;
	while (s.length < size) s = `0${s}`;
	return s;
}
for (let i = 0; i < menu.length; i += 1) {
	const idx = pad(i, 3);
	if (menu[i].sub_menus) {
		for (let j = 0; j < menu[i].sub_menus.length; j += 1) {
			const subidx = pad(j, 3);
			menu[i].sub_menus[j].id = `${idx}-${subidx}`;
		}
	}
	menu[i].id = idx;
}
export default {
	name: 'Navigation',
	data() {
		return {
			menu: this.calculateMenu(),
			breadCrumb: this.getBreadCrumb(this.$store.getters['user/menu']),
		};
	},
	created() {
		this.$eventHub.$on('path-changed', (path) => {
			this.breadCrumb = this.getBreadCrumb(this.$store.getters['user/menu'], path);
		});
		this.$eventHub.$on('role-changed', () => {
			this.menu = this.calculateMenu();
		});
	},
	methods: {
		getBreadCrumb(m, path) {
			if (path == null) {
				path = this.$router.history.current.path;
			}
			if (!m || path !== m.menu_link) {
				//
				const pm = this.calculateMenu();
				for (let idx = 0; idx < pm.length; idx += 1) {
					if (pm[idx].menu_link === path) return this.getBreadCrumb(pm[idx], path);
					if (pm[idx].sub_menus) {
						for (let idx2 = 0; idx2 < pm[idx].sub_menus.length; idx2 += 1) {
							if (pm[idx].sub_menus[idx2].menu_link === path) return this.getBreadCrumb(pm[idx].sub_menus[idx2], path);
						}
					}
				}
				return [{ name: 'home', href: '/' }];
			}
			if (!m.id) return [{ name: 'home', href: '/' }];
			const menuIds = m.id.split('-');
			const mm = menu.filter((item) => item.id === menuIds[0]);
			if (mm.length > 0) {
				if (mm[0].sub_menus && menuIds.length === 2) {
					const smm = mm[0].sub_menus.filter((item) => item.id === menuIds[1]);
					if (smm.length > 0)
						return [
							{ name: 'home', href: '/' },
							{ name: mm[0].menu_name, href: mm[0].menu_link },
							{ name: smm[0].menu_name, href: smm[0].menu_link },
						];
				}

				return [
					{ name: 'home', href: '/' },
					{ name: mm[0].menu_name, href: mm[0].menu_link },
				];
			}
			return [{ name: 'home', href: '/' }];
		},
		menuClick(m) {
			this.$store.commit(UserMutationTypes.SET_CURRENT_MENU, m);
			this.$router.push(m.menu_link);
		},
		calculateMenu() {
			const permissionMenu = [];
			for (let idx = 0; idx < menu.length; idx += 1) {
				const m = menu[idx];
				if (m.sub_menus) {
					const mm = {};
					mm.id = m.id;
					mm.menu_name = m.menu_name;
					mm.menu_link = m.menu_link;
					mm.sub_menus = [];
					for (let idx2 = 0; idx2 < m.sub_menus.length; idx2 += 1) {
						if (m.sub_menus[idx2].menu_permission.filter((item) => item === 'ALL')) {
							mm.sub_menus.push(m.sub_menus[idx2]);
						}
					}
					if (mm.sub_menus.length > 0) permissionMenu.push(mm);
				} else if (m.menu_permission.filter((item) => item === 'ALL').length > 0) {
					permissionMenu.push(m);
				}
			}
			return permissionMenu;
		},
		signOut() {
			this.$store.commit(UserMutationTypes.CLEAR_USER_INFO);
			this.$router.push('/login');
		},
	},
};
