// @vue/component
export default {
	name: 'EmailConfig',
	data() {
		return {
			email: {},
		};
	},
	created() {
		this.initData();
	},
	methods: {
		async testEmail() {
			const { value: email } = await Swal.fire({
				title: 'Nhập địa chỉ email nhận',
				input: 'email',
				inputPlaceholder: 'Enter your email address',
				showCancelButton: true,
				validationMessage: 'Địa chỉ email không hợp lệ!',
			});
			if (email) {
				const url = `email/test`;
				this.$vueLoading.startLoading('loading');
				const body = {
					EmailReceive: email,
				};
				this.$gateway
					.post(url, body)
					.then((res) => {
						if (res.data.Status === 200) {
							this.$m(res.data.MessageText);
						} else {
							this.$m(res.data.MessageText, MessageType.Error);
						}
					})
					.finally(() => {
						this.$vueLoading.endLoading('loading');
					});
			}
		},
		configEmail() {
			const url = `email/config`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.email)
				.then((res) => {
					this.$m(res.data.MessageText);
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		initData() {
			const url = `email/get`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					if (typeof res.data !== 'undefined') {
						this.email = res.data || {};
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
};
