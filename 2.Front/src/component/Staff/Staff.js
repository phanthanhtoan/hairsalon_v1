/* eslint-disable no-undef */
import func from './@function';

const ADD_STAFF_ACTION = 'add';
const EDIT_STAFF_ACTION = 'edit';

export default {
	name: 'Staff',
	components: func,
	data() {
		return {
			staffs: [],
			eventCustomOption: {
				bodyRowEvents: ({ row, rowIndex }) => {
					return {
						click: (event) => {
							this.click(row, rowIndex, event);
						},
						dblclick: (event) => {
							this.dblclick(row, rowIndex, event);
						},
					};
				},
			},
			titleStaff: 'Thêm nhân viên',
			typeAction: ADD_STAFF_ACTION,
			staffSelected: {},
			staffGroup: 0,
		};
	},
	created() {
		this.initData();
	},
	methods: {
		inActive() {
			if (typeof this.staffSelected === 'undefined' || this.isEmpty(this.staffSelected.Code)) {
				this.$m('Vui lòng chọn nhân viên để thao tác!', MessageType.Warning);
				return;
			}
			const url = `staff/active`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.staffSelected)
				.then((res) => {
					if (res.data.Status === 200) {
						const mess = this.staffSelected.IsActive ? `InActive nhân viên ${this.staffSelected.Code} thành công!` : `Active nhân viên ${this.staffSelected.Code} thành công!`;
						this.$m(mess);
						this.initData();
					} else {
						this.$m(res.data.MessageText);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		deleteStaff() {
			if (typeof this.staffSelected === 'undefined' || this.isEmpty(this.staffSelected.Code)) {
				this.$m('Vui lòng chọn nhân viên để thao tác!', MessageType.Warning);
				return;
			}
			const url = `staff/delete`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.staffSelected)
				.then((res) => {
					if (res.data.Status === 200) {
						this.$m(`Xóa nhân viên ${this.staffSelected.Code} thành công!`);
						this.staffSelected = {};
						this.typeAction = this.ADD_STAFF_ACTION;
						this.titleStaff = 'Thêm nhân viên';
						this.initData();
					} else {
						this.$m(res.data.MessageText);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		editStaff() {
			if (typeof this.staffSelected === 'undefined' || this.isEmpty(this.staffSelected.Code)) {
				this.$m('Vui lòng chọn nhân viên để thao tác!', MessageType.Warning);
				return;
			}
			this.titleStaff = 'Sửa nhân viên';
			this.typeAction = EDIT_STAFF_ACTION;
			this.$refs.refNewChangeStaff.show();
		},
		cancelAddStaff() {
			this.$refs.refNewChangeStaff.hide();
		},
		addStaffSuccess(staff) {
			const mess = this.typeAction === ADD_STAFF_ACTION ? `Thêm nhân viên ${staff.Code} thành công!` : `Sửa nhân viên ${this.staffSelected.Code} thành công!`;
			this.$m(mess);
			this.$refs.refNewChangeStaff.hide();
			this.initData();
		},
		addStaff() {
			this.titleStaff = 'Thêm nhân viên';
			this.typeAction = ADD_STAFF_ACTION;
			this.$refs.refNewChangeStaff.show();
		},
		dblclick(row, rowIndex, event) {
			this.staffSelected = row;
			this.editStaff();
		},
		click(row, rowIndex, event) {
			this.staffSelected = row;
		},
		initData() {
			const url = `staff/getall`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					this.staffs = res.data;
					if (typeof this.staffSelected !== 'undefined' && !this.isEmpty(this.staffSelected.StaffId)) {
						const itemAfterUpdate = this.staffs.find((e) => e.StaffId === this.staffSelected.StaffId);
						this.staffSelected = this.$deepClone(itemAfterUpdate);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
	computed: {
		colStaff() {
			const self = this;
			return [
				{
					field: 'No',
					key: '1',
					title: 'No.',
					width: 40,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return ++rowIndex;
					},
				},
				{
					field: 'Code',
					key: '2',
					title: 'Mã nhân viên',
					align: 'left',
					width: 80,
				},
				{
					field: 'Name',
					key: '3',
					title: 'Tên nhân viên',
					align: 'left',
					width: 100,
				},
				{
					field: 'BeginWork',
					key: '4',
					title: 'Ngày vào',
					width: 30,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.$moment(row.BeginWork).format('DD/MM/YYYY');
					},
				},
				{
					field: 'GroupName',
					key: '5',
					title: 'Vị trí',
					align: 'left',
					width: 60,
				},
				{
					field: 'Basicsalary',
					key: '6',
					title: 'Lương cứng',
					align: 'right',
					width: 75,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Basicsalary);
					},
				},
				{
					field: 'Commission',
					key: '7',
					title: 'Hoa hồng',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Commission);
					},
				},
				{
					field: 'Address',
					key: '8',
					title: 'Địa chỉ',
					align: 'left',
					width: 120,
				},
				{
					field: 'Phone',
					key: '9',
					title: 'Số điện thoại',
					align: 'left',
					width: 100,
				},
				{
					field: 'Facebook',
					key: '10',
					title: 'Facebook',
					align: 'left',
					width: 120,
				},
				{
					field: 'Email',
					key: '11',
					title: 'Email',
					align: 'left',
					width: 120,
				},
				{
					field: 'IsActive',
					key: '12',
					title: 'Đã nghỉ',
					width: 50,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.$renderCheckbox(!row.IsActive);
					},
				},
			];
		},
	},
};
