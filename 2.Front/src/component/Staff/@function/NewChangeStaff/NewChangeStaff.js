const ADD_STAFF_ACTION = 'add';
const EDIT_STAFF_ACTION = 'edit';

export default {
	name: 'NewChangeStaff',
	props: {
		staffGroup: {
			type: Number,
			default: 0,
		},
		typeAction: {
			type: String,
			default: ADD_STAFF_ACTION,
		},
		staffChange: {
			type: Object,
			default() {
				return {};
			},
		},
	},
	data() {
		return {
			staff: {
				Basicsalary: 0,
				BeginWork: new Date(),
			},
		};
	},
	computed: {
		isEditStaff() {
			return this.typeAction === EDIT_STAFF_ACTION;
		},
		staffGroups() {
			return [
				{
					StaffGroupId: 1,
					StaffGroupName: 'Thợ chính',
				},
				{
					StaffGroupId: 2,
					StaffGroupName: 'Thợ phụ',
				},
				{
					StaffGroupId: 3,
					StaffGroupName: 'Khác',
				},
			];
		},
	},
	created() {
		this.initData();
	},
	methods: {
		changeAmountValue(e) {
			const newValue = this.getAmountFromInput(e.target.value);
			this.staff.Basicsalary = this.format_curency(newValue);
		},
		focusInputAmount(e) {
			const price = this.focusAmountFromInput(e.target.value);
			this.staff.Basicsalary = price;
		},
		okAddStaff() {
			const body = this.$deepClone(this.staff);
			body.BeginWork = this.$moment(body.BeginWork).format('YYYY/MM/DD');
			body.Basicsalary = this.focusAmountFromInput(body.Basicsalary);
			if (!this.isValid(body)) {
				return;
			}
			const url = this.typeAction === ADD_STAFF_ACTION ? `staff/add` : `staff/edit`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then((res) => {
					this.$emit('add-staff-success', res.data.Data);
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		isValid(body) {
			if (this.isEmpty(body.Code)) {
				this.$m('Vui lòng mã nhân viên!', MessageType.Warning);
				return false;
			}
			if (this.isEmpty(body.Name)) {
				this.$m('Vui lòng nhập tên nhân viên!', MessageType.Warning);
				return false;
			}
			if (body.StaffGroupId === 0 || body.StaffGroupId === null) {
				this.$m('Vui lòng chọn vị trí!', MessageType.Warning);
				return false;
			}
			if (body.Basicsalary < 0) {
				this.$m('Lương cứng phải là số lớn hơn hoặc bằng 0!', MessageType.Warning);
				return false;
			}
			const regex = new RegExp('^0*(?:[1-9][0-9]?|100)$');
			if (!regex.test(body.Commission)) {
				this.$m('Hoa hồng là số từ 0-100!', MessageType.Warning);
				return false;
			}
			return true;
		},
		cancelAddStaff() {
			this.$emit('cancel-add-staff');
		},
		initData() {
			if (this.typeAction === EDIT_STAFF_ACTION) {
				this.staff = this.$deepClone(this.staffChange);
				this.staff.BeginWork = new Date(this.staff.BeginWork);
			} else {
				this.staff.StaffGroupId = this.staffGroup;
			}
			this.staff.Basicsalary = this.format_curency(this.staff.Basicsalary);
		},
	},
};
