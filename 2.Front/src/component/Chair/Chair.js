/* eslint-disable no-undef */
import func from './@function';

const ADD_CHAIR_ACTION = 'add';
const EDIT_CHAIR_ACTION = 'edit';

export default {
	name: 'Chair',
	components: func,
	data() {
		return {
			chairs: [],
			eventCustomOption: {
				bodyRowEvents: ({ row, rowIndex }) => {
					return {
						click: (event) => {
							this.click(row, rowIndex, event);
						},
						dblclick: (event) => {
							this.dblclick(row, rowIndex, event);
						},
					};
				},
			},
			titleChair: 'Thêm ghế',
			typeAction: ADD_CHAIR_ACTION,
			chairSelected: {},
		};
	},
	created() {
		this.initData();
	},
	methods: {
		inActive() {
			if (typeof this.chairSelected === 'undefined' || this.isEmpty(this.chairSelected.ChairCode)) {
				this.$m('Vui lòng chọn ghế để thao tác!', MessageType.Warning);
				return;
			}
			const url = `chair/active`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.chairSelected)
				.then((res) => {
					if (res.data.Status === 200) {
						const mess = this.chairSelected.IsActive ? `Ngừng sử dụng ghế ${this.chairSelected.ChairCode} thành công!` : `Bật sử dụng ghế ${this.chairSelected.ChairCode} thành công!`;
						this.$m(mess);
						this.initData();
					} else {
						this.$m(res.data.MessageText);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		deleteChair() {
			if (typeof this.chairSelected === 'undefined' || this.isEmpty(this.chairSelected.ChairCode)) {
				this.$m('Vui lòng chọn ghế để thao tác!', MessageType.Warning);
				return;
			}
			const url = `chair/delete`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.chairSelected)
				.then((res) => {
					if (res.data.Status === 200) {
						this.$m(`Xóa ghế ${this.chairSelected.ChairCode} thành công!`);
						this.chairSelected = {};
						this.typeAction = this.ADD_CHAIR_ACTION;
						this.titleChair = 'Thêm ghế';
						this.initData();
					} else {
						this.$m(res.data.MessageText);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		editChair() {
			if (typeof this.chairSelected === 'undefined' || this.isEmpty(this.chairSelected.ChairCode)) {
				this.$m('Vui lòng chọn ghế để thao tác!', MessageType.Warning);
				return;
			}
			this.titleChair = 'Sửa ghế';
			this.typeAction = EDIT_CHAIR_ACTION;
			this.$refs.refNewChangeChair.show();
		},
		cancelAddChair() {
			this.$refs.refNewChangeChair.hide();
		},
		addChairSuccess(item) {
			const mess = this.typeAction === ADD_CHAIR_ACTION ? `Thêm ghế ${item.ChairCode} thành công!` : `Sửa ghế ${this.chairSelected.ChairCode} thành công!`;
			this.$m(mess);
			this.$refs.refNewChangeChair.hide();
			this.initData();
		},
		addChair() {
			this.titleChair = 'Thêm ghế';
			this.typeAction = ADD_CHAIR_ACTION;
			this.$refs.refNewChangeChair.show();
		},
		dblclick(row, rowIndex, event) {
			this.chairSelected = row;
			this.editChair();
		},
		click(row, rowIndex, event) {
			this.chairSelected = row;
		},
		initData() {
			const url = `chair/getall`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					this.chairs = res.data;
					if (typeof this.chairSelected !== 'undefined' && !this.isEmpty(this.chairSelected.ChairId)) {
						const chairAfterUpdate = this.chairs.find((e) => e.ChairId === this.chairSelected.ChairId);
						this.chairSelected = this.$deepClone(chairAfterUpdate);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
	computed: {
		colChair() {
			const self = this;
			return [
				{
					field: 'No',
					key: '1',
					title: 'No.',
					width: 50,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return ++rowIndex;
					},
				},
				{
					field: 'ChairCode',
					key: '2',
					title: 'Mã ghế',
					align: 'left',
					width: 100,
				},
				{
					field: 'ChairName',
					key: '3',
					title: 'Tên ghế',
					align: 'left',
					width: 200,
				},
				{
					field: 'IsActive',
					key: '7',
					title: 'Sử dụng',
					width: 50,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.$renderCheckbox(row.IsActive);
					},
				},
			];
		},
	},
};
