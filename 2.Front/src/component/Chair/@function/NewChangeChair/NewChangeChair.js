const ADD_CHAIR_ACTION = 'add';
const EDIT_CHAIR_ACTION = 'edit';

export default {
	name: 'NewChangeChair',
	props: {
		typeAction: {
			type: String,
			default: ADD_CHAIR_ACTION,
		},
		chairChange: {
			type: Object,
			default() {
				return {};
			},
		},
	},
	data() {
		return {
			chair: {},
		};
	},
	computed: {
		isEdit() {
			return this.typeAction === EDIT_CHAIR_ACTION;
		},
	},
	created() {
		this.initData();
	},
	methods: {
		okAddChair() {
			if (!this.isValid(this.chair)) {
				return;
			}
			const url = this.typeAction === ADD_CHAIR_ACTION ? `chair/add` : `chair/edit`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.chair)
				.then((res) => {
					this.$emit('add-chair-success', res.data.Data);
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		isValid(chair) {
			if (this.isEmpty(chair.ChairCode)) {
				this.$m('Vui lòng nhập mã ghế!', MessageType.Warning);
				return false;
			}
			if (this.isEmpty(chair.ChairName)) {
				this.$m('Vui lòng nhập tên ghế!', MessageType.Warning);
				return false;
			}
			return true;
		},
		cancelAddChair() {
			this.$emit('cancel-add-chair');
		},
		initData() {
			if (this.typeAction === EDIT_CHAIR_ACTION) {
				this.chair = this.$deepClone(this.chairChange);
			}
		},
	},
};
