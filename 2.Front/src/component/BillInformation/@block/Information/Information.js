import DeleteProduct from '../../@function/DeleteProduct';

export default {
	name: 'Information',
	components: { DeleteProduct },
	props: {
		bill: {
			type: Object,
			default() {
				return {};
			},
		},
		productSelected: {
			type: Array,
			default() {
				return [];
			},
		},
		staff: {
			type: Object,
			default() {
				return {};
			},
		},
		chairs: {
			type: Array,
			default() {
				return [];
			},
		},
	},
	data() {
		return {
			showAutoComplete: false,
			search_data: [],
			base: [],
			eventCustomOption: {
				bodyCellEvents: ({ row, column, rowIndex }) => {
					return {
						click: (event) => {
							this.clickEvent(row, column, rowIndex, event);
						},
					};
				},
			},
			staffGroup: 0,
		};
	},
	methods: {
		moreChair() {
			this.$emit('more-chair');
		},
		moreStaffShampoo() {
			this.staffGroup = 3;
			this.emitData();
		},
		moreStaffSub() {
			this.staffGroup = 2;
			this.emitData();
		},
		moreStaffMain() {
			this.staffGroup = 1;
			this.emitData();
		},
		emitData() {
			this.$emit('more-staff', this.staffGroup);
		},
		closeAutoComplete() {
			this.showAutoComplete = false;
		},
		clickEvent(row, column, rowIndex, event) {
			if (column.field === 'Delete') {
				this.$emit('delete-product', rowIndex);
			}
		},
		getData() {
			this.showAutoComplete = false;
			const a = this.base.filter(
				(item) => this.bill.GuestName.toString().toLowerCase().includes(item.Name.toLowerCase()) || item.Name.toLowerCase().includes(this.bill.GuestName.toString().toLowerCase()),
			);
			this.search_data = a;
		},
		getName(e) {
			console.log(e);
		},
	},
	computed: {
		colDataSearch() {
			return [
				{
					field: 'test',
					key: '1',
					title: '',
					width: 50,
					align: 'center',
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						console.log(row, column, rowIndex, h);
						return ++rowIndex;
					},
				},
				{
					field: 'Code',
					key: '2',
					title: 'Code',
					align: 'left',
					width: '20%',
				},
				{
					field: 'Name',
					key: '3',
					title: 'Name',
					align: 'left',
				},
			];
		},
		totalAmount() {
			let totalAmount = 0;
			this.productSelected.forEach((item) => {
				totalAmount += item.Price * item.Quantity;
			});
			return this.format_curency(totalAmount);
		},
		colProductSelected() {
			const self = this;
			return [
				{
					field: 'ItemName',
					key: '1',
					title: 'Tên dịch vụ',
					align: 'left',
					width: 200,
				},
				{
					field: 'Quantity',
					key: '2',
					title: 'Q.ty',
					align: 'center',
					width: 60,
				},
				{
					field: 'Price',
					key: '3',
					title: 'Giá',
					align: 'right',
					width: 100,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Price);
					},
				},
				{
					field: 'Delete',
					key: '4',
					title: 'Xóa',
					align: 'center',
					width: 30,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return h(DeleteProduct);
					},
				},
			];
		},
	},
	directives: {
		'click-outside': {
			priority: 700,
			bind(el, binding, vnode) {
				// eslint-disable-next-line func-names
				el.event = function (event) {
					if (!(el === event.target || el.contains(event.target))) {
						vnode.context[binding.expression](event);
					}
				};
				document.body.addEventListener('click', el.event);
			},
			unbind(el) {
				document.body.removeEventListener('click', el.event);
			},
		},
	},
};
