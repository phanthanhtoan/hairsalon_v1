// @vue/component
export default {
	name: 'BillList',
	props: {
		bills: {
			type: Array,
			default() {
				return [];
			},
		},
		billSelected: {
			type: Object,
			default() {
				return {};
			},
		},
	},
	methods: {
		chooseBill(bill) {
			this.$emit('choose-bill', bill);
		},
		newBill() {
			this.$emit('new-bill');
		},
		getBillStatus(bill) {
			const COLOR_BACK_GROUND = this.getColorByStatus(bill.Status);
			return `background-color: ${COLOR_BACK_GROUND}`;
		},
	},
};
