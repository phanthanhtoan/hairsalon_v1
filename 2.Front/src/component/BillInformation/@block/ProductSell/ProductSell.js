export default {
	name: 'ProductSell',
	props: {
		itemLists: {
			type: Array,
			default() {
				return [];
			},
		},
	},
	methods: {
		chooseProduct(product) {
			this.$emit('choose-product', product);
		},
	},
};
