import ProductSell from './ProductSell';
import Information from './Information';
import BillList from './BillList';

const block = {
	ProductSell,
	Information,
	BillList,
};

export default block;
