/* eslint-disable no-undef */
import block from './@block';
import func from './@function';
import NewChangeChair from '../Chair/@function/NewChangeChair';

const END_HOUR_OF_DAY = 23;
const END_MIN_OF_DAY = 59;
export default {
	name: 'BillInformation',
	props: {
		objProp: {
			type: Object,
			default() {
				return {};
			},
		},
	},
	components: {
		...block,
		...func,
		NewChangeChair,
	},
	data() {
		return {
			itemLists: [],
			dataTable: [],
			productSelected: [],
			baseBillInfo: {
				GuestName: '',
				GuestPhone: '',
				ArrivalDate: new Date(),
				ArrivalTime: '',
				DepartureTime: '',
				StaffMain: '',
				StaffSub: '',
				StaffShampoo: '',
				Note: '',
				ChairId: '',
			},
			billInfo: {},
			staffGroup: 0,
			staff: {},
			chairs: [],
			bills: [],
			billSelected: {},
			transactionCodes: [],
			balance: 0,
			baseBtnAction: {
				New: true,
				QuickCheckin: true,
				Save: false,
				Checkin: false,
				Payment: false,
			},
			btnAction: {},
			search: {
				Date: new Date(),
				Arrival: true,
				CheckedIn: true,
				CheckedOut: false,
			},
		};
	},
	created() {
		this.initData();
	},
	methods: {
		close() {
			this.$emit('event-popup-close');
		},
		searchBill() {
			if (!this.props.isProp) {
				const documnetActive = document.activeElement;
				documnetActive.blur();
				documnetActive.focus();
				const body = this.$deepClone(this.search);
				body.Date = this.$moment(body.Date).format('YYYY/MM/DD');
				const url = `bill-information/search`;
				this.$vueLoading.startLoading('loading');
				this.$gateway
					.post(url, body)
					.then((res) => {
						this.bills = res.data;
						let selectedExists = {};
						if (typeof this.bills !== 'undefined' && this.bills.length > 0) {
							selectedExists = this.bills.find((e) => e.BillId === this.billSelected.BillId);
							if ((this.billSelected.BillId || 0) === 0 || typeof selectedExists === 'undefined') {
								selectedExists = this.$deepClone(this.bills[0]);
							}
						} else if ((this.billInfo.BillId || 0) === 0) {
							this.$m('Không tìm thấy hóa đơn', MessageType.Warning);
						}
						this.getBillInformation(selectedExists);
					})
					.finally(() => {
						this.$vueLoading.endLoading('loading');
					});
			}
		},
		addChairSuccess(chair) {
			this.$refs.refNewChangeChair.hide();
			this.chairs.push(chair);
		},
		cancelAddChair() {
			this.$refs.refNewChangeChair.hide();
		},
		moreChair() {
			this.$refs.refNewChangeChair.show();
		},
		addPaymentSuccess(data) {
			this.searchBill();
			this.$refs.refPayment.hide();
		},
		cancelPayment() {
			this.$refs.refPayment.hide();
		},
		payment() {
			const url = `bill-information/save`;
			const body = this.getBody();
			if (!this.isValid(this.billInfo)) {
				return;
			}
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then((res) => {
						this.$refs.refPayment.show();
					},
					() => {
						this.$m('Có lỗi xảy ra!', MessageType.Error);
					},
				)
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		chooseBill(bill) {
			if (bill.BillId !== this.billInfo.BillId) {
				this.getBillInformation(bill);
			}
		},
		getBillInformation(bill) {
			const billId = typeof bill !== 'undefined' ? bill.BillId || 0 : 0;
			const url = `bill-information/getbyid/${billId}`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					const billRes = res.data.Bill || {};
					this.billInfo = this.assignBillInfo(billRes);
					this.billSelected = this.$deepClone(billRes);
					this.productSelected = res.data.Details || [];
					this.updateBtnAction(this.billInfo);
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		updateBtnAction(bill) {
			const tempBtnAction = {
				New: false,
				QuickCheckin: false,
				Save: false,
				Checkin: false,
				Payment: false,
			};
			if (bill.Status === 1) {
				tempBtnAction.Save = true;
				tempBtnAction.Checkin = true;
			} else if (bill.Status === 2) {
				tempBtnAction.Save = true;
				tempBtnAction.Payment = true;
			}
			this.btnAction = tempBtnAction;
		},
		newBill() {
			this.btnAction = this.$deepClone(this.baseBtnAction);
			this.billInfo = this.$deepClone(this.baseBillInfo);
			this.billSelected = {};
			this.productSelected = [];
		},
		addStaffSuccess(staff) {
			if (staff.StaffGroupId === 1) {
				this.staff.StaffMains.push(staff);
			} else if (staff.StaffGroupId === 2) {
				this.staff.StaffSubs.push(staff);
			} else if (staff.StaffGroupId === 3) {
				this.staff.StaffShampoos.push(staff);
			}
			this.$root.$emit('bv::hide::modal', 'new-staff');
		},
		cancelAddStaff() {
			this.$root.$emit('bv::hide::modal', 'new-staff');
		},
		moreStaff(staffGroup) {
			this.staffGroup = staffGroup;
			this.$refs.refNewChangeStaff.show();
		},
		addItemSuccess(item) {
			this.itemLists.push(item);
			this.$refs.refNewChangeItem.hide();
		},
		cancelAddItem() {
			this.$refs.refNewChangeItem.hide();
		},
		addItem() {
			this.$refs.refNewChangeItem.show();
		},
		checkin() {
			const url = `bill-information/checkin`;
			this.sendData(url);
		},
		saveBill() {
			const url = `bill-information/save`;
			this.sendData(url);
		},
		quickCheckin() {
			this.billInfo.Status = 2;
			const url = `bill-information/add`;
			this.sendData(url);
		},
		addBill() {
			this.billInfo.Status = 1;
			const url = `bill-information/add`;
			this.sendData(url);
		},
		sendData(url) {
			const body = this.getBody();
			if (!this.isValid(this.billInfo)) {
				return;
			}
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then(
					(res) => {
						if (this.objProp.isProp) {
							this.$emit('event-popup-done');
						} else {
							this.searchBill();
							this.$m('Thành công!');
						}
					},
					() => {
						this.$m('Có lỗi xảy ra!', MessageType.Error);
					},
				)
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		assignBillInfo(bill) {
			const result = this.$deepClone(bill);
			result.ArrivalDate = new Date(result.ArrivalDate);
			result.ArrivalTime = this.$moment(result.ArrivalTime).format('HH:mm');
			result.DepartureTime = this.$moment(result.DepartureTime).format('HH:mm');
			return result;
		},
		getBody() {
			const result = {
				Bill: this.$deepClone(this.billInfo),
				Details: this.productSelected,
			};
			result.Bill.ArrivalDate = this.$moment(result.Bill.ArrivalDate).format('YYYY/MM/DD');
			result.Bill.ArrivalTime = `${result.Bill.ArrivalDate} ${result.Bill.ArrivalTime}`;
			result.Bill.DepartureTime = `${result.Bill.ArrivalDate} ${result.Bill.DepartureTime}`;
			return result;
		},
		isValid(bill) {
			if (this.isEmpty(bill.GuestName)) {
				this.$m('Vui lòng nhập tên khách!', MessageType.Warning);
				return false;
			}
			if (this.isEmpty(bill.GuestPhone)) {
				this.$m('Vui lòng nhập số điện thoại', MessageType.Warning);
				return false;
			}
			if (bill.Status === 2 && (bill.ChairId === 0 || bill.ChairId === null || bill.ChairId === '')) {
				this.$m('Vui lòng chọn ghế', MessageType.Warning);
				return false;
			}
			return true;
		},
		initData() {
			this.getHoursDefault();
			this.btnAction = this.$deepClone(this.baseBtnAction);
			this.billInfo = this.$deepClone(this.baseBillInfo);
			const body = this.$deepClone(this.search);
			body.Date = this.$moment(body.Date).format('YYYY/MM/DD');
			const url = `bill-information/initdata`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then((res) => {
					this.itemLists = res.data.ItemLists;
					this.staff = res.data.Staff;
					this.bills = res.data.Bills;
					this.chairs = res.data.Chairs;
					this.getDataProp();
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		getDataProp() {
			if (typeof this.objProp.bill !== 'undefined' && (this.objProp.bill.BillId || 0) !== 0) {
				this.getBillInformation(this.objProp.bill);
			}
			if (typeof this.objProp.bill !== 'undefined' && (this.objProp.bill.BillId || 0) === 0 && (this.objProp.bill.StaffId || 0) !== 0) {
				this.billInfo.StaffMain = this.objProp.bill.StaffId;
				this.billInfo.ArrivalDate = new Date(this.objProp.bill.ArrivalDate);
				this.billInfo.ArrivalTime = this.$moment(this.objProp.bill.ArrivalTime).format('HH:mm');
				const dayDep = new Date(this.objProp.bill.DepartureTime);
				const IS_END_DAY = this.billInfo.ArrivalDate.getDate() < dayDep.getDate();
				const depHour = IS_END_DAY ? END_HOUR_OF_DAY : hour;
				const depMin = IS_END_DAY ? END_MIN_OF_DAY : min;
				this.billInfo.DepartureTime = `${depHour}:${depMin}`;
			}
		},
		getHoursDefault() {
			const day = new Date();
			const hour = day.getHours();
			const arrHour = this.addZero(hour);
			const arrMin = this.addZero(day.getMinutes());
			const nextHour = hour + 1;
			const depHour = nextHour > END_HOUR_OF_DAY ? END_HOUR_OF_DAY : nextHour;
			const depMin = nextHour > END_HOUR_OF_DAY ? END_MIN_OF_DAY : arrMin;
			this.baseBillInfo.ArrivalTime = `${arrHour}:${arrMin}`;
			this.baseBillInfo.DepartureTime = `${depHour}:${depMin}`;
		},
		addZero(i) {
			if (i < 10) {
				i = `0${i}`;
			}
			return i;
		},
		deleteProduct(index) {
			this.productSelected.splice(index, 1);
		},
		chooseProduct(item) {
			const product = this.$deepClone(item);
			const productExists = this.productSelected.find((e) => e.ItemId === product.ItemId);
			if (typeof productExists !== 'undefined') {
				productExists.Quantity += 1;
			} else {
				product.Quantity = 1;
				this.productSelected.push(product);
			}
		},
	},
};
