// @vue/component
export default {
	name: 'Payment',
	props: {
		billId: {
			type: Number,
			default: 0,
		},
	},
	data() {
		return {
			transactionCodes: [],
			balance: 0,
			payment: {
				PaymentAmount: 0,
				Refund: 0,
			},
			amountInput: 0,
		};
	},
	computed: {
		refund() {
			const amount = this.focusAmountFromInput(this.payment.PaymentAmount);
			if (amount > this.balance && this.balance > 0) {
				return this.format_curency(amount - this.balance);
			}
			return '';
		},
	},
	created() {
		this.initData();
	},
	methods: {
		okPayment() {
			this.payment.BillId = this.billId;
			this.payment.Amount = this.focusAmountFromInput(this.payment.PaymentAmount);
			if (this.payment.Amount === 0 || this.payment.Amount === '') {
				this.$m('Vui lòng nhập số tiền!', MessageType.Warning);
				return;
			}
			const url = `payment/add`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.payment)
				.then((res) => {
					if (res.data.Status === 200) {
						this.$m('Thanh toán thành công!');
						this.$emit('add-payment-success', res.data.Data);
					} else {
						this.$m(res.data.MessageText);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		changeAmountValue(e) {
			const newValue = this.getAmountFromInput(e.target.value);
			this.payment.PaymentAmount = this.format_curency(newValue);
		},
		focusInputAmount(e) {
			const price = this.focusAmountFromInput(e.target.value);
			this.payment.PaymentAmount = price;
		},
		cancelPayment() {
			this.$emit('cancel-payment');
		},
		suggest(amount) {
			this.payment.PaymentAmount = this.format_curency(amount);
		},
		initData() {
			const url = `payment/get-payment/${this.billId}`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					this.transactionCodes = res.data.TransactionCodes;
					this.payment.TransactionCode = this.transactionCodes[0].Code;
					this.balance = res.data.Balance;
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		}
	},
};
