import NewChangeItem from '../../ItemList/@function/NewChangeItem';
import NewChangeStaff from '../../Staff/@function/NewChangeStaff';
import Payment from './Payment';

const func = {
	NewChangeItem,
	NewChangeStaff,
	Payment,
};

export default func;
