// @vue/component
export default {
	name: 'BillByStaff',
	props: {
		fromDate: {
			type: String,
			default: '',
		},
		toDate: {
			type: String,
			default: '',
		},
		staffId: {
			type: Number,
			default: 0,
		},
	},
	data() {
		return {
			bills: [],
		};
	},
	computed: {
		colBill() {
			const self = this;
			return [
				{
					field: 'No',
					key: '1',
					title: 'No.',
					width: 20,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return ++rowIndex;
					},
				},
				{
					field: 'ArrivalDate',
					key: '2',
					title: 'Ngày',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.$moment(row.ArrivalDate).format('DD/MM/YYYY');
					},
				},
				{
					field: 'BillId',
					key: '3',
					title: 'Mã hóa đơn',
					align: 'left',
					width: 60,
				},
				{
					field: 'GuestName',
					key: '4',
					title: 'Tên khách',
					align: 'left',
					width: 100,
				},
				{
					field: 'StaffMainName',
					key: '5',
					title: 'Thợ chính',
					align: 'left',
					width: 100,
				},
				{
					field: 'StaffSubName',
					key: '6',
					title: 'Thợ phụ',
					align: 'left',
					width: 100,
				},
				{
					field: 'StaffShampooName',
					key: '7',
					title: 'Thợ phụ',
					align: 'left',
					width: 100,
				},
				{
					field: 'BillAmount',
					key: '8',
					title: 'Tiền hóa đơn',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.BillAmount);
					},
				},
			];
		},
	},
	created() {
		this.initData();
	},
	methods: {
		initData() {
			const body = {
				FromDate: this.fromDate,
				ToDate: this.toDate,
				StaffId: this.staffId,
			};
			const url = `salary/bill-by-staff`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then((res) => {
					this.bills = res.data;
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
};
