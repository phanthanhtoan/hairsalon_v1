// @vue/component
export default {
	name: 'RtpRevenue',
	data() {
		return {
			revenues: [],
			search: {
				FromDate: new Date(),
				ToDate: new Date(),
			},
			footerData: [],
			cellSpanOption: {
				footerCellSpan: this.footerCellSpan,
			},
			cellStyleOption: {
				footerCellClass: ({ row, column, rowIndex }) => {
					if (column.field === 'No') {
						return 'table-footer-cell-class';
					}
					return 'table-footer-cell-class text-right';
				},
			},
		};
	},
	computed: {
		colRevenue() {
			const self = this;
			return [
				{
					field: 'No',
					key: '1',
					title: 'No.',
					width: 20,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return ++rowIndex;
					},
				},
				{
					field: 'BillId',
					key: '2',
					title: 'Số HĐ',
					align: 'left',
					width: 20,
				},
				{
					field: 'ArrivalDate',
					key: '3',
					title: 'Ngày',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.$moment(row.ArrivalDate).format('DD/MM/YYYY');
					},
				},
				{
					field: 'BillAmout',
					key: '4',
					title: 'Tiền HĐ',
					align: 'right',
					width: 40,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.BillAmout);
					},
				},
				{
					field: 'MainName',
					key: '5',
					title: 'Thợ chính',
					align: 'left',
					width: 90,
				},
				{
					field: 'MainCom',
					key: '6',
					title: '%HH',
					align: 'right',
					width: 20,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.MainCom);
					},
				},
				{
					field: 'MainComAmount',
					key: '7',
					title: 'HH thợ chính',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.MainComAmount);
					},
				},
				{
					field: 'SubName',
					key: '8',
					title: 'Thợ phụ',
					align: 'left',
					width: 90,
				},
				{
					field: 'SubCom',
					key: '9',
					title: '%HH',
					align: 'right',
					width: 20,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.SubCom);
					},
				},
				{
					field: 'SubComAmount',
					key: '10',
					title: 'HH thợ phụ',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.SubComAmount);
					},
				},
				{
					field: 'ShampooName',
					key: '11',
					title: 'Thợ khác',
					align: 'left',
					width: 90,
				},
				{
					field: 'ShampooCom',
					key: '12',
					title: '%HH',
					align: 'right',
					width: 20,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.ShampooCom);
					},
				},
				{
					field: 'ShampooComAmount',
					key: '13',
					title: 'HH thợ khác',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.ShampooComAmount);
					},
				},
				{
					field: 'RemainAmount',
					key: '14',
					title: 'Còn lại',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.BillAmout - row.MainComAmount - row.SubComAmount - row.ShampooComAmount);
					},
				},
			];
		},
	},
	methods: {
		footerCellSpan({ row, column, rowIndex }) {
			if (column.field === 'BillId' || column.field === 'MainName' || column.field === 'SubName' || column.field === 'ShampooName') {
				return {
					rowspan: 1,
					colspan: 3,
				};
			}
			if (
				column.field === 'ArrivalDate' ||
				column.field === 'BillAmout' ||
				column.field === 'MainCom' ||
				column.field === 'MainComAmount' ||
				column.field === 'SubCom' ||
				column.field === 'SubComAmount' ||
				column.field === 'ShampooCom' ||
				column.field === 'ShampooComAmount'
			) {
				return {
					rowspan: 0,
					colspan: 0,
				};
			}
		},
		initFooterData() {
			const totalBill = this.revenues.length;
			let totalBillAmount = 0;
			let totalMainAmount = 0;
			let totalSubAmount = 0;
			let totalShampooAmount = 0;
			let totalRemainAmount = 0;
			this.revenues.forEach((item) => {
				totalBillAmount += item.BillAmout;
				totalMainAmount += item.MainComAmount;
				totalSubAmount += item.SubComAmount;
				totalShampooAmount += item.ShampooComAmount;
				totalRemainAmount += item.BillAmout - item.MainComAmount - item.SubComAmount - item.ShampooComAmount;
			});
			this.footerData = [
				{
					No: totalBill,
					BillId: this.format_curency(totalBillAmount),
					MainName: this.format_curency(totalMainAmount),
					SubName: this.format_curency(totalSubAmount),
					ShampooName: this.format_curency(totalShampooAmount),
					RemainAmount: this.format_curency(totalRemainAmount),
				},
			];
		},
		searchRevenue() {
			const body = this.$deepClone(this.search);
			body.FromDate = this.$moment(body.FromDate).format('YYYY/MM/DD');
			body.ToDate = this.$moment(body.ToDate).format('YYYY/MM/DD');
			const url = `report/bill-commission`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then((res) => {
					this.revenues = res.data;
					this.initFooterData();
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
};
