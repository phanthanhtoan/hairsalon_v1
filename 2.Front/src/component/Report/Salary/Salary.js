// @vue/component
import BillByStaff from '../BillByStaff';

export default {
	name: 'Salary',
	data() {
		return {
			search: {
				FromDate: new Date(),
				ToDate: new Date(),
			},
			staffs: [],
			staffOption: [],
			expandOption: {
				expandable: ({ row, column, rowIndex }) => {
					if (row.BillQuantity <= 0) {
						return false;
					}
				},
				expandedRowKeys: [],
				render: ({ row, column, rowIndex }, h) => {
					const body = this.$deepClone(this.search);
					body.FromDate = this.$moment(body.FromDate).format('YYYY/MM/DD');
					body.ToDate = this.$moment(body.ToDate).format('YYYY/MM/DD');
					return <BillByStaff fromDate={body.FromDate} toDate={body.ToDate} staffId={row.StaffId} />;
				},
				afterExpandRowChange: ({ afterExpandedRowKeys, row, rowIndex }) => {
					this.changeExpandedRowKeys(afterExpandedRowKeys);
				},
			},
		};
	},
	computed: {
		colStaff() {
			const self = this;
			return [
				{
					field: '',
					key: '1',
					type: 'expand',
					title: '',
					width: 20,
					align: 'center',
				},
				{
					field: 'Code',
					key: '2',
					title: 'Mã nhân viên',
					align: 'left',
					width: 80,
				},
				{
					field: 'Name',
					key: '3',
					title: 'Tên nhân viên',
					align: 'left',
					width: 100,
				},
				{
					field: 'GroupName',
					key: '5',
					title: 'Vị trí',
					align: 'left',
					width: 60,
				},
				{
					field: 'BillQuantity',
					key: '6',
					title: 'Số lượng HĐ',
					align: 'right',
					width: 50,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.BillQuantity);
					},
				},
				{
					field: 'BillAmount',
					key: '7',
					title: 'Tổng tiền HĐ',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.BillAmount);
					},
				},
				{
					field: 'Commission',
					key: '8',
					title: 'Hoa hồng',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Commission);
					},
				},
				{
					field: 'CommissionAmount',
					key: '9',
					title: 'Tiền hoa hồng',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.CommissionAmount);
					},
				},
			];
		},
	},
	created() {
		this.initData();
	},
	methods: {
		changeExpandedRowKeys(keys) {
			this.expandOption.expandedRowKeys = keys;
		},
		expandSwitch(key) {
			const rowKeyIndex = this.expandOption.expandedRowKeys.indexOf(key);

			if (rowKeyIndex > -1) {
				this.expandOption.expandedRowKeys.splice(rowKeyIndex, 1);
			} else {
				this.expandOption.expandedRowKeys.push(key);
			}
		},
		calCommission() {
			const body = this.$deepClone(this.search);
			body.FromDate = this.$moment(body.FromDate).format('YYYY/MM/DD');
			body.ToDate = this.$moment(body.ToDate).format('YYYY/MM/DD');
			const url = `salary/calculate-commission`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then((res) => {
					this.staffs = res.data;
					this.expandOption.expandedRowKeys = [];
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		initData() {
			const url = `salary/initdata`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					this.staffOption = res.data.staffs;
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
};
