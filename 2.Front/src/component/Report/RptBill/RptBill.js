import RptBillDetail from '../RptBillDetail';

export default {
	name: 'RptBill',
	data() {
		return {
			bills: [],
			search: {
				FromDate: new Date(),
				ToDate: new Date(),
			},
			expandOption: {
				expandedRowKeys: [],
				render: ({ row, column, rowIndex }, h) => {
					return <RptBillDetail billId={row.BillId} />;
				},
				afterExpandRowChange: ({ afterExpandedRowKeys, row, rowIndex }) => {
					this.changeExpandedRowKeys(afterExpandedRowKeys);
				},
			},
		};
	},
	computed: {
		isShowTotal() {
			return this.bills.length > 0;
		},
		totalAmount() {
			let totalAmount = 0;
			this.bills.forEach((item) => {
				totalAmount += item.BillAmount;
			});
			return this.format_curency(totalAmount);
		},
		colBill() {
			const self = this;
			return [
				{
					field: '',
					key: '1',
					type: 'expand',
					title: '',
					width: 20,
					align: 'center',
				},
				{
					field: 'ArrivalDate',
					key: '2',
					title: 'Ngày',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.$moment(row.ArrivalDate).format('DD/MM/YYYY');
					},
				},
				{
					field: 'BillId',
					key: '3',
					title: 'Mã hóa đơn',
					align: 'left',
					width: 60,
				},
				{
					field: 'GuestName',
					key: '4',
					title: 'Tên khách',
					align: 'left',
					width: 100,
				},
				{
					field: 'StaffMainName',
					key: '5',
					title: 'Thợ chính',
					align: 'left',
					width: 100,
				},
				{
					field: 'StaffSubName',
					key: '6',
					title: 'Thợ phụ',
					align: 'left',
					width: 100,
				},
				{
					field: 'StaffShampooName',
					key: '7',
					title: 'Thợ phụ',
					align: 'left',
					width: 100,
				},
				{
					field: 'BillAmount',
					key: '8',
					title: 'Tiền hóa đơn',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.BillAmount);
					},
				},
			];
		},
	},
	methods: {
		changeExpandedRowKeys(keys) {
			this.expandOption.expandedRowKeys = keys;
		},
		expandSwitch(key) {
			const rowKeyIndex = this.expandOption.expandedRowKeys.indexOf(key);

			if (rowKeyIndex > -1) {
				this.expandOption.expandedRowKeys.splice(rowKeyIndex, 1);
			} else {
				this.expandOption.expandedRowKeys.push(key);
			}
		},
		searchBill() {
			const body = this.$deepClone(this.search);
			body.FromDate = this.$moment(body.FromDate).format('YYYY/MM/DD');
			body.ToDate = this.$moment(body.ToDate).format('YYYY/MM/DD');
			const url = `report/rptbill`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then((res) => {
					this.bills = res.data;
					this.expandOption.expandedRowKeys = [];
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
};
