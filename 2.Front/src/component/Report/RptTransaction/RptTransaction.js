// @vue/component
export default {
	name: 'RptTransaction',
	data() {
		return {
			search: {
				FromDate: new Date(),
				ToDate: new Date(),
			},
			transactions: [],
			sumTransactions: [],
		};
	},
	computed: {
		colSumTrn() {
			const self = this;
			return [
				{
					field: 'Name',
					key: '1',
					title: 'Hình thức',
					align: 'left',
					width: 120,
				},
				{
					field: 'TotalAmount',
					key: '2',
					title: 'Số tiền',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.TotalAmount);
					},
				},
			];
		},
		colTransaction() {
			const self = this;
			return [
				{
					field: 'No',
					key: '1',
					title: 'No.',
					width: 20,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return ++rowIndex;
					},
				},
				{
					field: 'BillId',
					key: '2',
					title: 'Số HĐ',
					align: 'left',
					width: 40,
				},
				{
					field: 'TransactionCode',
					key: '3',
					title: 'Mã',
					align: 'left',
					width: 30,
				},
				{
					field: 'Name',
					key: '4',
					title: 'Hình thức',
					align: 'left',
					width: 70,
				},
				{
					field: 'PostedTime',
					key: '5',
					title: 'Thời gian',
					width: 90,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.$moment(row.PostedTime).format('DD/MM/YYYY HH:mm:ss');
					},
				},
				{
					field: 'PostedUser',
					key: '6',
					title: 'Tài khoản',
					align: 'left',
					width: 40,
				},
				{
					field: 'Amount',
					key: '7',
					title: 'Số tiền',
					align: 'right',
					width: 40,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Amount);
					},
				},
				{
					field: 'Note',
					key: '8',
					title: 'Ghi chú',
					align: 'left',
					width: 160,
				},
			];
		},
	},
	methods: {
		searchTransaction() {
			const body = this.$deepClone(this.search);
			body.FromDate = this.$moment(body.FromDate).format('YYYY/MM/DD');
			body.ToDate = this.$moment(body.ToDate).format('YYYY/MM/DD');
			const url = `report/rpt-transaction`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then((res) => {
					this.transactions = res.data.transactions;
					this.sumTransactions = res.data.sumTransactions;
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
};
