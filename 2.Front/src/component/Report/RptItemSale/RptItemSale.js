// @vue/component
export default {
	name: 'RptItemSale',
	data() {
		return {
			itemSales: [],
			search: {
				FromDate: new Date(),
				ToDate: new Date(),
			},
		};
	},
	computed: {
		isShowTotal() {
			return this.itemSales.length > 0;
		},
		totalAmount() {
			let totalAmount = 0;
			this.itemSales.forEach((item) => {
				totalAmount += item.TotalQuantity * item.Price;
			});
			return this.format_curency(totalAmount);
		},
		colItem() {
			const self = this;
			return [
				{
					field: 'No',
					key: '1',
					title: 'No.',
					width: 20,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return ++rowIndex;
					},
				},
				{
					field: 'ItemCode',
					key: '2',
					title: 'Mã sản phẩm',
					align: 'left',
					width: 60,
				},
				{
					field: 'ItemName',
					key: '3',
					title: 'Tên sản phẩm',
					align: 'left',
					width: 200,
				},
				{
					field: 'TotalQuantity',
					key: '4',
					title: 'Số lượng',
					align: 'right',
					width: 40,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.TotalQuantity);
					},
				},
				{
					field: 'Price',
					key: '5',
					title: 'Đơn giá',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Price);
					},
				},
				{
					field: 'TotalAmount',
					key: '6',
					title: 'Thành tiền',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.TotalQuantity * row.Price);
					},
				},
			];
		},
	},
	methods: {
		searchItemSale() {
			const body = this.$deepClone(this.search);
			body.FromDate = this.$moment(body.FromDate).format('YYYY/MM/DD');
			body.ToDate = this.$moment(body.ToDate).format('YYYY/MM/DD');
			const url = `report/rpt-item-sale`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, body)
				.then((res) => {
					this.itemSales = res.data;
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
};
