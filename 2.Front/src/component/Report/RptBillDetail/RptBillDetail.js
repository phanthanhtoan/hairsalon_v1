// @vue/component
export default {
	name: 'RptBillDetail',
	props: {
		billId: {
			type: Number,
			default: 0,
		},
	},
	data() {
		return {
			billDetails: [],
		};
	},
	computed: {
		colBillDetail() {
			const self = this;
			return [
				{
					field: 'No',
					key: '1',
					title: 'No.',
					width: 20,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return ++rowIndex;
					},
				},
				{
					field: 'ItemCode',
					key: '2',
					title: 'Mã sản phẩm',
					width: 60,
				},
				{
					field: 'ItemName',
					key: '3',
					title: 'Tên sản phẩm',
					align: 'left',
					width: 300,
				},
				{
					field: 'Quantity',
					key: '4',
					title: 'Số lượng',
					align: 'right',
					width: 40,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Quantity);
					},
				},
				{
					field: 'Price',
					key: '5',
					title: 'Đơn giá',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Price);
					},
				},
				{
					field: 'Amount',
					key: '6',
					title: 'Thành tiền',
					align: 'right',
					width: 60,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Quantity * row.Price);
					},
				},
			];
		},
	},
	created() {
		this.initData();
	},
	methods: {
		initData() {
			const url = `report/bill-detail/${this.billId}`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					this.billDetails = res.data;
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
};
