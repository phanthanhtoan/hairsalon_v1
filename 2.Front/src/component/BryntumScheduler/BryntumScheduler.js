/* eslint-disable no-unused-expressions */
/**
 * @author Phùng Quốc Phòng
 * @description Component BryntumScheduler sử dụng Vẽ RoomPlan
 */
export default {
	name: 'BryntumScheduler',
	props: {
		// Configs
		allowOverlap: { type: Boolean, default: false },
		animateRemovingRows: { type: Boolean, default: undefined },
		autoAdjustTimeAxis: { type: Boolean, default: undefined },
		autoHeight: { type: Boolean, default: true },
		barMargin: { type: Number, default: 2 },
		columnLines: { type: Boolean, default: undefined },
		columns: Array,
		contextMenuTriggerEvent: String,
		createEventOnDblClick: { type: Boolean, default: false },
		defaultResourceImageName: String,
		disableGridRowModelWarning: { type: Boolean, default: undefined },
		displayDateFormat: String,
		emptyText: String,
		enableDeleteKey: { type: Boolean, default: false },
		enableEventAnimations: { type: Boolean, default: undefined },
		enableRecurringEvents: { type: Boolean, default: undefined },
		enableTextSelection: { type: Boolean, default: undefined },
		endDate: Date,
		endParamName: String,
		eventBarTextField: { type: String, default: 'name' },
		eventBodyTemplate: Function,
		eventColor: { type: String, default: 'red' },
		/**
		 * Các giá trị:
		 * - stack
		 * - stackmixed
		 * - pack
		 * - overlap
		 */
		eventLayout: String,
		eventRenderer: Function,
		eventSelectionDisabled: { type: Boolean, default: undefined },
		/**
		 * Các gia trị:
		 * - mixed
		 * - colored
		 * - plain
		 * - line
		 * - dashed
		 * - minimal
		 * - border
		 * - rounded
		 * - hollow
		 */
		eventStyle: { type: String, default: 'plain' },
		fillLastColumn: { type: Boolean, default: undefined },
		flex: String,
		fillTicks: Boolean,
		forceFit: {
			default: false,
			type: Boolean,
		},
		height: Number,
		hideHeaders: Boolean,
		horizontalEventSorterFn: Function,
		id: String,
		listeners: Object,
		loadMask: { type: String, default: 'Loading...' },
		longPressTime: { type: Number, default: 400 },
		maintainSelectionOnDatasetChange: { type: Boolean, default: undefined },
		managedEventSizing: { type: Boolean, default: undefined },
		maxHeight: [Number, String],
		maxWidth: [Number, String],
		maxZoomLevel: Number,
		milestoneAlign: String,
		milestoneCharWidth: Number,
		milestoneLayoutMode: String,
		minHeight: [Number, String],
		minWidth: [Number, String],
		minZoomLevel: Number,
		mode: String,
		multiEventSelect: Boolean,
		partner: [Object, String],
		passStartEndParameters: { type: Boolean, default: undefined },
		presets: [Object, Array],
		readOnly: Boolean,
		removeUnassignedEvent: { type: Boolean, default: undefined },
		resizeToFitIncludesHeader: { type: Boolean, default: false },
		resourceColumns: Object,
		resourceImagePath: String,
		resourceMargin: Number,
		resourceTimeRanges: [Object, Array],
		responsiveLevels: { type: Object, default: undefined },
		rowHeight: Number,
		scrollLeft: Number,
		scrollTop: Number,
		selectedEvents: Array,
		selectionMode: Object,
		showDirty: { type: Boolean, default: undefined },
		snap: { type: Boolean, default: undefined },
		snapRelativeToEventStartDate: { type: Boolean, default: undefined },
		showRemoveEventInContextMenu: Boolean,
		showRemoveRowInContextMenu: Boolean,
		showUnassignEventInContextMenu: Boolean,
		startDate: Date,
		startParamName: String,
		subGridConfigs: Object,
		tickWidth: Number,
		timeResolution: Object,
		triggerSelectionChangeOnRemove: { type: Boolean, default: undefined },
		/**
		 * Các chuyển động:
		 * - fade-in
		 * - slide-from-left
		 * - slide-from-top
		 * - zoom-in
		 */
		useInitialAnimation: { type: [Boolean, String], default: false },
		viewportCenterDate: Date,
		viewPreset: { type: [String, Object], default: 'dayAndWeek' },
		weekStartDay: Number,
		width: [Number, String],
		workingTime: Object,
		zoomLevel: Number,
		zoomOnMouseWheel: { type: Boolean, default: false },
		zoomOnTimeAxisDoubleClick: { type: Boolean, default: false },
		tbar: { type: [Object, Array], default: undefined },
		bbar: { type: [Object, Array], default: undefined },

		// Stores
		assignmentStore: Object,
		dependencyStore: Object,
		eventStore: Object,
		resourceStore: Object,

		crudManager: Object,

		// Data
		assignments: Array,
		dependencies: Array,
		events: Array,
		resources: Array,
		timeRanges: Array,

		config: Object,

		// Features, only used for initialization
		cellEditFeature: { type: [Boolean, Object], default: false },
		cellMenuFeature: { type: [Boolean, Object], default: undefined },
		cellTooltipFeature: { type: [Boolean, Object], default: undefined },
		columnDragToolbarFeature: { type: [Boolean, Object], default: undefined },
		columnLinesFeature: { type: Boolean, default: true },
		columnPickerFeature: { type: [Boolean, Object], default: undefined },
		columnReorderFeature: { type: [Boolean, Object], default: undefined },
		columnResizeFeature: { type: [Boolean, Object], default: undefined },
		contextMenuFeature: [Boolean, Object],
		ddependenciesFeature: { type: [Boolean, Object], default: undefined },
		dependencyEditFeature: { type: [Boolean, Object], default: undefined },
		eventContextMenuFeature: { type: [Boolean, Object], default: true },
		eventDragFeature: { type: [Boolean, Object], default: true },
		eventDragCreateFeature: { type: [Boolean, Object], default: true },
		eventDragSelectFeature: { type: Boolean, default: undefined },
		eventEditFeature: { type: [Boolean, Object], default: true },
		eventFilterFeature: { type: [Boolean, Object], default: true },
		eventResizeFeature: { type: [Boolean, Object], default: true },
		eventTooltipFeature: { type: [Boolean, Object], default: false },
		filterBarFeature: { type: [Boolean, Object], default: undefined },
		filterFeature: { type: [Boolean, Object], default: undefined },
		groupFeature: { type: [Boolean, Object, String], default: undefined },
		groupSummaryFeature: { type: [Boolean, Object], default: undefined },
		headerContextMenuFeature: { type: [Boolean, Object], default: true },
		headerMenuFeature: { type: [Boolean, Object], default: false },
		headerZoomFeature: { type: Boolean, default: undefined },
		labelsFeature: { type: [Boolean, Object], default: undefined },
		nonWorkingTimeFeature: { type: [Boolean, Object], default: undefined },
		panFeature: { type: [Boolean, Object], default: undefined },
		pdfExportFeature: { type: [Boolean, Object], default: undefined },
		quickFindFeature: { type: [Boolean, Object], default: undefined },
		regionResizeFeature: { type: Boolean, default: true },
		resourceTimeRangesFeature: { type: [Boolean, Object], default: undefined },
		rowReorderFeature: { type: Boolean, default: undefined },
		scheduleMenuFeature: { type: [Boolean, Object], default: undefined },
		scheduleTooltipFeature: { type: [Boolean, Object], default: true },
		searchFeature: { type: [Boolean, Object], default: undefined },
		sortFeature: { type: [Boolean, Object, String, Array], default: true },
		simpleEventEdit: { type: [Boolean, Object], default: false },
		stripeFeature: { type: Boolean, default: undefined },
		summaryFeature: { type: [Boolean, Object], default: undefined },
		timeAxisHeaderMenuFeature: { type: [Boolean, Object], default: undefined },
		timeRangesFeature: { type: [Boolean, Object], default: true },
		treeFeature: { type: [Boolean, Object], default: undefined },
	},
	mounted() {
		const propKeys = Object.keys(this.$props);

		const config = {
			// Render grid to components element
			appendTo: this.$el,
			// Listeners, will relay events using $emit
			listeners: {
				catchAll(event) {
					// Uncomment this line to log events being emitted to console
					this.$emit(event.type, event);
				},
				thisObj: this,
			},
			features: {},
		};

		// Apply all props to grid config
		propKeys.forEach((prop) => {
			let match;
			// eslint-disable-next-line no-cond-assign
			if ((match = prop.match(/(.*)Feature/)) && this[prop] !== undefined) {
				// Prop which ends with Feature is a feature config
				config.features[match[1]] = this[prop];
			} else if (prop === 'config') {
				// Prop is a config object
				Object.assign(config, this[prop]);
			} else {
				// Prop is a config
				if (this[prop] !== undefined) {
					config[prop] = this[prop];
				}

				// Set up a watcher
				this.$watch(prop, (newValue) => {
					this.schedulerEngine[prop] = Array.isArray(newValue) ? newValue.slice() : newValue;
				});
			}
		}, this);

		// Create a Bryntum Grid with props as configs
		// eslint-disable-next-line no-undef
		this.schedulerEngine = new bryntum.scheduler.Scheduler(config);
		const engine = this.schedulerEngine;
		engine.eventStore && engine.eventStore.relayAll(engine, 'events');
		engine.resourceStore && engine.resourceStore.relayAll(engine, 'resources');
		engine.assignmentStore && engine.assignmentStore.relayAll(engine, 'assignments');
		engine.dependencyStore && engine.dependencyStore.relayAll(engine, 'dependencies');
	},

	beforeDestroy() {
		// Make sure Bryntum Grid is destroyed when vue component is
		this.schedulerEngine.destroy();
	},
};
