/* eslint-disable no-undef */
import func from './@function';

const ADD_ITEM_ACTION = 'add';
const EDIT_ITEM_ACTION = 'edit';

export default {
	name: 'ItemList',
	components: func,
	data() {
		return {
			products: [],
			eventCustomOption: {
				bodyRowEvents: ({ row, rowIndex }) => {
					return {
						click: (event) => {
							this.click(row, rowIndex, event);
						},
						dblclick: (event) => {
							this.dblclick(row, rowIndex, event);
						},
					};
				},
			},
			titleItem: 'Thêm sản phẩm',
			typeAction: ADD_ITEM_ACTION,
			itemSelected: {},
		};
	},
	created() {
		this.initData();
	},
	methods: {
		inActive() {
			if (typeof this.itemSelected === 'undefined' || this.isEmpty(this.itemSelected.ItemCode)) {
				this.$m('Vui lòng chọn sản phẩm để thao tác!', MessageType.Warning);
				return;
			}
			const url = `item/active`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.itemSelected)
				.then((res) => {
					if (res.data.Status === 200) {
						const mess = this.itemSelected.IsActive ? `Ngừng sử dụng sản phẩm ${this.itemSelected.ItemCode} thành công!` : `Bật sử dụng sản phẩm ${this.itemSelected.ItemCode} thành công!`;
						this.$m(mess);
						this.initData();
					} else {
						this.$m(res.data.MessageText);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		deleteItem() {
			if (typeof this.itemSelected === 'undefined' || this.isEmpty(this.itemSelected.ItemCode)) {
				this.$m('Vui lòng chọn sản phẩm để thao tác!', MessageType.Warning);
				return;
			}
			const url = `item/delete`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.itemSelected)
				.then((res) => {
					if (res.data.Status === 200) {
						this.$m(`Xóa sản phẩm ${this.itemSelected.ItemCode} thành công!`);
						this.itemSelected = {};
						this.typeAction = this.ADD_ITEM_ACTION;
						this.titleItem = 'Thêm sản phẩm';
						this.initData();
					} else {
						this.$m(res.data.MessageText);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		editItem() {
			if (typeof this.itemSelected === 'undefined' || this.isEmpty(this.itemSelected.ItemCode)) {
				this.$m('Vui lòng chọn sản phẩm để thao tác!', MessageType.Warning);
				return;
			}
			this.titleItem = 'Sửa sản phẩm';
			this.typeAction = EDIT_ITEM_ACTION;
			this.$refs.refNewChangeItem.show();
		},
		cancelAddItem() {
			this.$refs.refNewChangeItem.hide();
		},
		addItemSuccess(item) {
			const mess = this.typeAction === ADD_ITEM_ACTION ? `Thêm sản phẩm ${item.ItemCode} thành công!` : `Sửa sản phẩm ${this.itemSelected.ItemCode} thành công!`;
			this.$m(mess);
			this.$refs.refNewChangeItem.hide();
			this.initData();
		},
		addItem() {
			this.titleItem = 'Thêm sản phẩm';
			this.typeAction = ADD_ITEM_ACTION;
			this.$refs.refNewChangeItem.show();
		},
		dblclick(row, rowIndex, event) {
			this.itemSelected = row;
			this.editItem();
		},
		click(row, rowIndex, event) {
			this.itemSelected = row;
		},
		initData() {
			const url = `item/getall`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					this.products = res.data;
					if (typeof this.itemSelected !== 'undefined' && !this.isEmpty(this.itemSelected.ItemId)) {
						const itemAfterUpdate = this.products.find((e) => e.ItemId === this.itemSelected.ItemId);
						this.itemSelected = this.$deepClone(itemAfterUpdate);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
	computed: {
		colProduct() {
			const self = this;
			return [
				{
					field: 'No',
					key: '1',
					title: 'No.',
					width: 50,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return ++rowIndex;
					},
				},
				{
					field: 'ItemCode',
					key: '2',
					title: 'Mã dịch vụ',
					align: 'left',
					width: 100,
				},
				{
					field: 'ItemName',
					key: '3',
					title: 'Tên dịch vụ',
					align: 'left',
					width: 200,
				},
				{
					field: 'Price',
					key: '4',
					title: 'Giá',
					align: 'right',
					width: 80,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.format_curency(row.Price);
					},
				},
				{
					field: 'UnitName',
					key: '5',
					title: 'Đơn vị tính',
					align: 'left',
					width: 100,
				},
				{
					field: 'IGroupName',
					key: '6',
					title: 'Danh mục',
					align: 'left',
					width: 100,
				},
				{
					field: 'IsActive',
					key: '7',
					title: 'Sử dụng',
					width: 50,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.$renderCheckbox(row.IsActive);
					},
				},
			];
		},
	},
};
