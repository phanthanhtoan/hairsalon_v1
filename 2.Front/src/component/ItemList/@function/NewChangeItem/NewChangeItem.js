const ADD_ITEM_ACTION = 'add';
const EDIT_ITEM_ACTION = 'edit';
export default {
	name: 'NewChangeItem',
	props: {
		typeAction: {
			type: String,
			default: ADD_ITEM_ACTION,
		},
		itemChange: {
			type: Object,
			default() {
				return {};
			},
		},
	},
	data() {
		return {
			item: {
				Price: 0,
			},
			IUnits: [],
			IGroup: [],
			priceDisplay: 0,
		};
	},
	computed: {
		isEdit() {
			return this.typeAction === EDIT_ITEM_ACTION;
		},
	},
	created() {
		this.initData();
	},
	methods: {
		changeAmountValue(e) {
			const newValue = this.getAmountFromInput(e.target.value);
			this.priceDisplay = this.format_curency(newValue);
		},
		focusInputAmount(e) {
			const price = this.focusAmountFromInput(e.target.value);
			this.priceDisplay = price;
		},
		cancelAddItem() {
			this.$emit('cancel-add-item');
		},
		okAddItem() {
			this.item.Price = this.focusAmountFromInput(this.priceDisplay);
			if (!this.isValid(this.item)) {
				return;
			}
			const url = this.typeAction === ADD_ITEM_ACTION ? `item/add` : `item/edit`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.item)
				.then((res) => {
					if (res.data.Status === 200) {
						this.$emit('add-item-success', res.data.Data);
					} else {
						this.$m(res.data.MessageText);
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		isValid(item) {
			if (this.isEmpty(item.ItemCode)) {
				this.$m('Vui lòng mã sản phẩm!', MessageType.Warning);
				return false;
			}
			if (this.isEmpty(item.ItemName)) {
				this.$m('Vui lòng nhập tên sản phẩm!', MessageType.Warning);
				return false;
			}
			if (item.UnitId === 0 || item.UnitId === null) {
				this.$m('Vui lòng chọn đơn vị!', MessageType.Warning);
				return false;
			}
			if (item.ItemGroupId === 0 || item.ItemGroupId === null) {
				this.$m('Vui lòng chọn danh mục!', MessageType.Warning);
				return false;
			}
			if (item.Price <= 0) {
				this.$m('Giá sản phẩm phải là số lớn hơn 0!', MessageType.Warning);
				return false;
			}
			return true;
		},
		initData() {
			const url = `item/picklist`;
			this.$gateway.get(url).then((res) => {
				this.IUnits = res.data.Units;
				this.IGroup = res.data.Groups;
				if (this.typeAction === EDIT_ITEM_ACTION) {
					this.item = this.$deepClone(this.itemChange);
				}
				this.priceDisplay = this.format_curency(this.item.Price);
			});
		},
	},
};
