// @vue/component
import func from './@function';

export default {
	name: 'Account',
	components: func,
	data() {
		return {
			accounts: [],
		};
	},
	created() {
		this.getAccount();
	},
	methods: {
		addAccountSuccess() {
			this.$refs.refNewAccount.hide();
			this.getAccount();
		},
		cancelNewAccount() {
			this.$refs.refNewAccount.hide();
		},
		newAccount() {
			this.$refs.refNewAccount.show();
		},
		getAccount() {
			const url = `account/getall`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					this.accounts = res.data;
					// if (typeof this.itemSelected !== 'undefined' && !this.isEmpty(this.itemSelected.ItemId)) {
					// 	const itemAfterUpdate = this.products.find((e) => e.ItemId === this.itemSelected.ItemId);
					// 	this.itemSelected = this.$deepClone(itemAfterUpdate);
					// }
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
	computed: {
		colAccount() {
			const self = this;
			return [
				{
					field: 'No',
					key: '1',
					title: 'No.',
					width: 20,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return ++rowIndex;
					},
				},
				{
					field: 'UserName',
					key: '2',
					title: 'Tên đăng nhập',
					align: 'left',
					width: 200,
				},
				{
					field: 'Email',
					key: '3',
					title: 'Email',
					align: 'left',
					width: 200,
				},
				{
					field: 'LockoutEnabled',
					key: '4',
					title: 'Bị khóa',
					width: 50,
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						return self.$renderCheckbox(row.LockoutEnabled);
					},
				},
			];
		},
	},
};
