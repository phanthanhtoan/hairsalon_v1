// @vue/component
export default {
	name: 'NewAccount',
	data() {
		return {
			accountInfo: {},
			visibility: 'password',
		};
	},
	methods: {
		showHidePassword() {
			if (this.visibility === 'password') {
				this.visibility = '';
			} else {
				this.visibility = 'password';
			}
		},
		okNewAccount() {
			const url = `account/register`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url, this.accountInfo)
				.then((res) => {
					this.$emit('add-account-success', res.data.Data);
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		cancelNewAccount() {
			this.$emit('cancel-new-account');
		},
	},
};
