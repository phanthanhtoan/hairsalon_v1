// @vue/component
import VueDraggableResizable from 'vue-draggable-resizable';

export default {
	name: 'HomePage',
	components: { VueDraggableResizable },
	data() {
		return {
			customer: [],
			columns: [
				{
					field: 'test',
					key: '1',
					title: '',
					width: 50,
					align: 'center',
					renderBodyCell: ({ row, column, rowIndex }, h) => {
						console.log(row, column, rowIndex, h);
						return ++rowIndex;
					},
				},
				{
					field: 'name',
					key: '2',
					title: 'Name',
					align: 'left',
					width: '20%',
				},
				{
					field: 'email',
					key: '3',
					title: 'email',
					align: 'left',
				},
			],
			listRmMapFilter: [
				{
					RoomWidth: 100,
					RoomHeight: 100,
					RoomLeft: 100,
					RoomTop: 100,
				},
				{
					RoomWidth: 100,
					RoomHeight: 100,
					RoomLeft: 300,
					RoomTop: 0,
				},
				{
					RoomWidth: 100,
					RoomHeight: 100,
					RoomLeft: 400,
					RoomTop: 0,
				},
			],
		};
	},
	created() {
		this.testApi();
	},
	methods: {
		showFunctionForRoom(e) {
			console.log(e)
		},
		testApi() {
			const url = 'customer-management';
			this.$gateway.get(url).then((res) => {
				this.customer = res.data;
			});
		},
	},
};
