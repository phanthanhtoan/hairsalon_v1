import { UserMutationTypes } from '../../store/module/user';

export default {
	name: 'Login',
	data() {
		return {
			loginForm: {},
		};
	},
	created() {
		if (this.isLoggedIn) {
			this.redirectAfterLogin();
		}
	},
	methods: {
		loginAction() {
			this.$vueLoading.startLoading('loading');
			const BODY_REQUEST = this.bodyPostToken(this.loginForm.UserName, this.loginForm.Password);
			this.$http
				.post('/token', BODY_REQUEST)
				.then((response) => {
					if (response.status === 200) {
						if (response.data.access_token) {
							this.$store.commit(UserMutationTypes.SET_USER_INFO, response.data);
							this.redirectAfterLogin();
						}
					}
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		redirectAfterLogin() {
			this.$router.push('/');
		},
		bodyPostToken(userName, password) {
			return `username=${userName}&password=${password}&grant_type=password`;
		},
	},
};
