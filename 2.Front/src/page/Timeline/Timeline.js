/* eslint-disable no-undef */
import BryntumScheduler from '../../component/BryntumScheduler';
import BillInformation from '../../component/BillInformation';
import Payment from '../../component/BillInformation/@function/Payment';

const DATE_FORMAT_HELPER = {
	HOUR_24H: 'HH:mm',
	HOUR_12H: 'hh:mm',
	DATE_MDY: 'MM/DD/YYYY',
	DATE_DMY: 'DD/MM/YYYY',
	DATE_YMD: 'YYYY/MM/DD',
	DATE_YDM: 'YYYY/DD/MM',
	DATETIME_DMY: 'DD/MM/YYYY HH:mm',
	DATETIME_MDY: 'MM/DD/YYYY HH:mm',
	DATETIME_YMD: 'YYYY/MM/DD HH:mm',
	DATETIME_YDM: 'YYYY/DD/MM HH:mm',
};

const presetOptions = {
	ViewByDays: {
		base: 'dayAndWeek',
		headers: [
			{
				unit: 'day',
				dateFormat: 'DD/MM ddd',
				renderer: (start, end, headerConfig) => {
					return bryntum.scheduler.DateHelper.format(start, 'DD/MM ddd');
				},
			},
		],
		tickWidth: 75,
	},
	ViewByHours: {
		base: 'hourAndDay',
		timeResolution: {
			unit: 'minute',
			increment: 5,
		},
		headers: [
			{
				unit: 'day',
				dateFormat: 'DD/MM ddd',
			},
			{
				unit: 'hour',
				dateFormat: DATE_FORMAT_HELPER.HOUR_24H,
			},
		],
		tickWidth: 50,
	},
};
const MIN_HOUR_IN_DAY = '00:00';
const MAX_HOUR_IN_DAY = '23:59';

export default {
	name: 'Timeline',
	components: {
		BryntumScheduler,
		BillInformation,
		Payment,
	},
	data() {
		return {
			events: [],
			resources: [],
			timeRanges: [],
			startDate: new Date(),
			endDate: new Date(),
			viewPresetConfig: presetOptions.ViewByHours,
			scheduleContextMenu: {
				items: {
					addEvent: false,
				},
			},
			timeRangesFeatureConfig: {
				showCurrentTimeLine: true,
			},
			eventEditConfig: false,
			eventDragFeatureConfig: {
				constrainDragToTimeSlot: true,
			},
			barMargin: 2,
			rowHeightEvent: 35,
			bryntumColumns: [{ text: 'Name', field: 'name', width: 140, locked: true }],
			contextCreateProxyElement: null,
			objProp: {
				isProp: true,
				bill: {},
			},
			billIdSelected: 0,
		};
	},
	computed: {
		eventContextMenuConfig() {
			const self = this;
			return {
				items: {
					deleteEvent: false,
					addEvent: false,
				},
				processItems({ eventRecord, items }) {
					const MENU_ITEMS = {
						CANCEL_BILL: {
							icon: 'fa fa-trash-o',
							text: 'Hủy hóa đơn',
							onItem() {
								self.cancelBill(eventRecord);
							},
						},
						EDIT_BILL: {
							icon: 'fa fa-pencil',
							text: 'Sửa hóa đơn',
							onItem() {
								self.editBill(eventRecord);
							},
						},
						CHECK_IN: {
							icon: 'fa fa-sign-in',
							text: 'Check-in',
							onItem() {
								self.checkinBill(eventRecord);
							},
						},
						PAYMENT: {
							icon: 'fa fa-dollar',
							text: 'Thanh toán',
							onItem() {
								self.paymentBill(eventRecord);
							},
						},
					};
					items.cancelBill = { ...MENU_ITEMS.CANCEL_BILL };
					items.editBill = { ...MENU_ITEMS.EDIT_BILL };
					items.checkBill = { ...MENU_ITEMS.CHECK_IN };
					items.paymentBill = { ...MENU_ITEMS.PAYMENT };
				},
			};
		},
		bryntumBaseConfig() {
			const self = this;
			return {
				listeners: {
					catchAll(eventData) {
						if (eventData.type === 'eventdblclick') {
							const eventRecordDetail = eventData.eventRecord;
							const billDblClick = {
								BillId: eventRecordDetail.billId,
							};
							self.dblClickBill(billDblClick);
						}
						// else if (eventData.type === 'eventclick') {
						// 	this.$m('Bạn vừa click vào bill');
						// }
					},
					beforeDragCreateFinalize: async ({ context, proxyElement }) => {
						self.contextCreateProxyElement = { ...context };
						const resourceDetail = context.resourceRecord;
						const newBillEvent = {
							ArrivalDate: self.$moment(context.startDate).format('MM/DD/YYYY'),
							ArrivalTime: self.$moment(context.startDate).format('MM/DD/YYYY HH:mm'),
							DepartureTime: self.$moment(context.endDate).format('MM/DD/YYYY HH:mm'),
							StaffId: resourceDetail.id,
						};
						context.async = true;
						const menu = new bryntum.scheduler.Menu({
							forElement: proxyElement,
							items: [
								{
									icon: 'b-icon b-icon-add',
									text: 'Đặt trước',
									onItem() {
										self.addBill(newBillEvent);
										context.finalize();
									},
								},
								{
									icon: 'fa fa-sign-in',
									text: 'Quick Check-in',
									onItem() {
										self.quickCheckin(newBillEvent);
										context.finalize();
									},
								},
							],
						});
						self.contextFinalize(context);
					},
					beforeEventDropFinalize: async ({ context }) => {
						context.async = true;
						const originBill = { ...context.eventRecords[0].data };
						const NEW_RESOURCE = context.newResource;
						const newBillEvent = {
							ArrivalDate: self.$moment(context.startDate).format('MM/DD/YYYY'),
							ArrivalTime: self.$moment(context.startDate).format('MM/DD/YYYY HH:mm'),
							DepartureTime: self.$moment(context.endDate).format('MM/DD/YYYY HH:mm'),
							StaffId: NEW_RESOURCE.id,
							BillId: originBill.billId,
						};
						await self.moveAndResizeBill(context, newBillEvent);
					},
					beforeEventResizeFinalize: async ({ context }) => {
						context.async = true;
						const resourceRecordDetail = context.resourceRecord;
						const eventRecordDetail = context.eventRecord;
						const newBillEvent = {
							ArrivalDate: self.$moment(context.startDate).format('MM/DD/YYYY'),
							ArrivalTime: self.$moment(context.startDate).format('MM/DD/YYYY HH:mm'),
							DepartureTime: self.$moment(context.endDate).format('MM/DD/YYYY HH:mm'),
							StaffId: resourceRecordDetail.id,
							BillId: eventRecordDetail.billId,
						};
						await self.moveAndResizeBill(context, newBillEvent);
					},
				},
			};
		},
	},
	created() {
		this.initData();
	},
	methods: {
		eventBillInfoClose() {
			this.$refs.refBillInformation.hide();
			this.getBill();
		},
		addPaymentSuccess(data) {
			this.$refs.refPaymentTimeline.hide();
			this.getBill();
		},
		cancelPayment() {
			this.$refs.refPaymentTimeline.hide();
		},
		paymentBill(bill) {
			this.billIdSelected = bill.billId;
			this.$refs.refPaymentTimeline.show();
		},
		checkinBill(bill) {
			const url = `timeline/checkin/${bill.billId}`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url)
				.then(
					(res) => {
						this.$m(res.data.MessageText);
						this.getBill();
					},
					() => {
						this.$m('Có lỗi xảy ra!', MessageType.Error);
					},
				)
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		editBill(bill) {
			const billId = {
				BillId: bill.billId,
			};
			this.dblClickBill(billId);
		},
		cancelBill(bill) {
			const url = `timeline/cancel/${bill.billId}`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.post(url)
				.then(
					(res) => {
						this.$m(res.data.MessageText);
						this.getBill();
					},
					() => {
						this.$m('Có lỗi xảy ra!', MessageType.Error);
					},
				)
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		dblClickBill(bill) {
			this.objProp.bill = bill;
			this.$refs.refBillInformation.show();
		},
		async moveAndResizeBill(context, bill) {
			const confirmResult = await Swal.fire({
				icon: 'warning',
				title: 'Bạn có muốn thay đổi hóa đơn?',
				showCancelButton: true,
				confirmButtonText: `Đồng ý`,
				cancelButtonText: `Hủy`,
			});
			if (confirmResult.isConfirmed) {
				await this.updateBill(context, bill);
			} else {
				context.finalize();
			}
		},
		async updateBill(context, bill) {
			this.$vueLoading.startLoading('loading');
			try {
				const updateBillResult = await this.$gateway.post('timeline/updatebill', bill);
				this.$m(updateBillResult.data.MessageText);
				context.finalize(updateBillResult.data.Status === 200);
			} catch (exception) {
				this.$m('Có lỗi xảy ra!');
				context.finalize();
			}
			this.$vueLoading.endLoading('loading');
		},
		eventBillInfoDone() {
			this.objProp.bill = {};
			this.$refs.refBillInformation.hide();
			this.getBill();
		},
		quickCheckin(bill) {
			this.objProp.bill = bill;
			this.$refs.refBillInformation.show();
		},
		addBill(bill) {
			this.objProp.bill = bill;
			this.$refs.refBillInformation.show();
		},
		contextFinalize(context) {
			setTimeout(() => {
				// async code don't forget to call finalize
				if (context !== undefined || context !== null) {
					context.finalize();
				}
			}, 5000);
		},
		initData() {
			this.startDate = this.createNewDateByDateAndTime(this.startDate, MIN_HOUR_IN_DAY);
			this.endDate = this.createNewDateByDateAndTime(this.endDate, MAX_HOUR_IN_DAY);
			this.getResources();
			this.getBill();
		},
		createNewDateByDateAndTime(dateValue, strTime) {
			const dateMDYFormat = this.$moment(dateValue).format(DATE_FORMAT_HELPER.DATE_MDY);
			return new Date(`${dateMDYFormat} ${strTime}`);
		},
		getBill() {
			const url = `timeline/bills`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					const bills = Array.isArray(res.data) ? [...res.data] : [];
					const bryntumEvents = [];
					bills.forEach((item) => {
						const BOOKING_COLOR_CODE = this.getColorByStatus(item.Status);
						const eventStyleValue = `background-color : ${BOOKING_COLOR_CODE}; border-color: ${BOOKING_COLOR_CODE}`;
						const bill = {
							resourceId: item.StaffMain || 0,
							startDate: this.bryntumDateFormat(item.ArrivalTime),
							endDate: this.bryntumDateFormat(item.DepartureTime),
							name: item.GuestName,
							billId: item.BillId,
							style: eventStyleValue,
						};
						bryntumEvents.push(bill);
					});
					this.events = [...bryntumEvents];
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
		bryntumDateFormat(date) {
			return this.$moment(date).format('YYYY-MM-DD HH:mm');
		},
		getResources() {
			const url = `timeline/staffs`;
			this.$vueLoading.startLoading('loading');
			this.$gateway
				.get(url)
				.then((res) => {
					const staffs = Array.isArray(res.data) ? [...res.data] : [];
					const bryntumResource = [];
					staffs.forEach((item) => {
						const staff = {
							id: item.StaffId,
							name: item.Name,
						};
						bryntumResource.push(staff);
					});
					const noStaff = {
						id: 0,
						name: 'Chưa chọn Stylist',
					};
					bryntumResource.push(noStaff);
					this.resources = [...bryntumResource];
				})
				.finally(() => {
					this.$vueLoading.endLoading('loading');
				});
		},
	},
};
