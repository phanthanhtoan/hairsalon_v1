export const SET_USER_INFO = 'setUserInfo';
export const CLEAR_USER_INFO = 'clearUserInfo';
export const SET_CURRENT_MENU = 'setCurrentMenu';
export default {
	namespaced: 'user',
	state: {
		accessToken: '',
		currentMenuInformation: {},
	},
	getters: {
		accessToken: (state) => state.accessToken,
		isLoggedIn: (state) => state.accessToken,
		userName: (state) => state.userName,
		currentMenuInformation: (state) => state.currentMenuInformation,
	},
	mutations: {
		[SET_USER_INFO](state, payload) {
			state.accessToken = typeof payload.access_token !== 'undefined' ? payload.access_token : '';
			state.userName = payload.userName;
		},
		[CLEAR_USER_INFO]: (state) => {
			state.accessToken = '';
		},
		[SET_CURRENT_MENU]: (state, payload) => {
			state.currentMenuInformation = payload;
		},
	},
	actions: {},
};
