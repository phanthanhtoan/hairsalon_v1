import user, { SET_USER_INFO, CLEAR_USER_INFO, SET_CURRENT_MENU } from './user';

export const UserNamespace = 'user';

export const UserMutationTypes = {
	SET_USER_INFO: `${UserNamespace}/${SET_USER_INFO}`,
	CLEAR_USER_INFO: `${UserNamespace}/${CLEAR_USER_INFO}`,
	SET_CURRENT_MENU: `${UserNamespace}/${SET_CURRENT_MENU}`,
};
export default user;
