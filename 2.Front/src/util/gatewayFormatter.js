import getStore from '../store';
import getRouter from '../router';

import { UserMutationTypes } from '../store/module/user';

export const clearLocalStorage = () => {
	getStore().commit(UserMutationTypes.CLEAR_USER_INFO);
};

export const errorFormatter = (error) => {
	if (error && error.response && error.response.data && error.response.data.error) {
		const response = { config: error.config, ...error.response, ...error.response.data };
		// delete data to avoid confusion
		delete response.data;
		return response;
	}
	return error;
};

export const responseFormatter = (response) => {
	if (response.status === 401 || response.status === 403) {
		clearLocalStorage();
		getRouter().push('/login');
		return response;
	}
	if (response && response.data && typeof response.data.data !== 'undefined') {
		return { ...response, ...response.data };
	}
	return response;
};
