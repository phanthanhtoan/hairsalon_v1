import Vue from 'vue';
import VueI18nManager from 'vue-i18n-manager';
import { sync } from 'vuex-router-sync';

import './asset/style/screen.scss';

import './settings/settings';
import { VeTable, VePagination, VeIcon, VeLoading } from 'vue-easytable'; // import library
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

import iview from 'iview';
import locale from 'iview/dist/locale/en-US';
import VueTheMask from 'vue-the-mask';
import VueLoading from 'vuex-loading';
import Swal from 'sweetalert2';
import VueNotifications from 'vue-notifications';
import VueIziToast from 'vue-izitoast';
import iziToast from 'izitoast';
import VueMoment from 'vue-moment';
import Spinner from 'vue-simple-spinner';
import VueShortKey from 'vue-shortkey';
import directive from './directive/directive';
import component from './component/component';
import getRouter from './router';
import getStore from './store';
import startUp from './control/startUp';
import getLocaleConfig from './config/localeConfig';
import setupInjects from './util/setupInjects';
import localeLoader from './util/localeLoader';
import App from './App';
import filter from './filter/filter';
import mixin from '../static/@common';
import '../node_modules/vue-easytable/libs/theme-default/index.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/bootstrap-vue/dist/bootstrap-vue.css';
import '../node_modules/izitoast/dist/css/iziToast.css';
import 'iview/dist/styles/iview.css';

function toast({ title, message, type, timeout }) {
	return iziToast[type]({ title, message, timeout });
}
window.Swal = Swal;
Vue.use(VueMoment);
Vue.mixin(mixin);
Vue.use(VueShortKey);
Vue.use(VueTheMask);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueLoading);
Vue.use(iview, { locale });
Vue.use(Spinner);
Vue.use(VueLoading);
Vue.use(VeTable);
Vue.use(VePagination);
Vue.use(VeIcon);
Vue.use(VeLoading);
Vue.use(VueIziToast);
Vue.use(VueNotifications, {
	success: toast,
	error: toast,
	info: toast,
	warning: toast,
});
// register filters globally
Object.keys(filter).forEach((key) => Vue.filter(key, filter[key]));

// register directives globally
Object.keys(directive).forEach((key) => Vue.directive(key, directive[key]));

// register components globally
Object.keys(component).forEach((key) => Vue.component(key, component[key]));

setupInjects();

if (window.webpackPublicPath) {
	// eslint-disable-next-line
  __webpack_public_path__ = window.webpackPublicPath;
}

const router = getRouter();
const store = getStore();
const localeConfig = getLocaleConfig();

if (localeConfig.localeEnabled) {
	Vue.use(VueI18nManager, {
		store,
		router: localeConfig.localeRoutingEnabled ? router : null,
		config: localeConfig.config,
		proxy: localeLoader,
	});
	Vue.initI18nManager();
}

// sync router data to store
sync(store, router);

// Init new vue app
const app = new Vue({
	router,
	store,
	render: (createElement) => createElement(App),
	vueLoading: new VueLoading({ registerComponents: false }),
});

// Mount the app after startUp
startUp(store).then(() => {
	app.$mount('#app');
});
