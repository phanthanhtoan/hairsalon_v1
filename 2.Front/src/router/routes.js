import HomePage from '../page/HomePage';
import Login from '../page/Login';
import Bill from '../component/Bill';
import BillInformation from '../component/BillInformation';
import Account from '../component/Account';
import ItemList from '../component/ItemList';
import Staff from '../component/Staff';
import Chair from '../component/Chair';
import TimeLine from '../page/Timeline';
import EmailConfig from '../component/EmailConfig';
// Report
import Salary from '../component/Report/Salary';
import RptBill from '../component/Report/RptBill';
import RptTransaction from '../component/Report/RptTransaction';
import RptItemSale from '../component/Report/RptItemSale';
import RtpRevenue from '../component/Report/RtpRevenue';

export const RouteNames = {
	HOME: 'home',
	BILL: 'bill',
	BILL_INFORMATION: 'billinformation',
	ACCOUNT: 'account',
	LOGIN: 'login',
	ITEM_LIST: 'itemlist',
	STAFF: 'staff',
	CHAIR: 'chair',
	// Report
	SALARY: 'salary',
	RPT_BILL: 'rptbill',
	RPT_TRANSACTION: 'rpttransaction',
	RPT_ITEM_SALE: 'rptitemsale',
	RPT_REVENUE: 'rptrevenue',
	TIME_LINE: 'timeline',
	EMAIL_CONFIG: 'email',
};

export default [
	{
		path: '/home',
		component: HomePage,
		name: RouteNames.HOME,
	},
	{
		path: '/login',
		component: Login,
		name: RouteNames.LOGIN,
	},
	{
		path: '/bill',
		component: Bill,
		name: RouteNames.BILL,
	},
	{
		path: '/billinformation',
		component: BillInformation,
		name: RouteNames.BILL,
	},
	{
		path: '/account',
		component: Account,
		name: RouteNames.ACCOUNT,
	},
	{
		path: '/itemlist',
		component: ItemList,
		name: RouteNames.ITEM_LIST,
	},
	{
		path: '/staff',
		component: Staff,
		name: RouteNames.STAFF,
	},
	{
		path: '/chair',
		component: Chair,
		name: RouteNames.CHAIR,
	},
	{
		path: '/timeline',
		component: TimeLine,
		name: RouteNames.TIME_LINE,
	},
	{
		path: '/email',
		component: EmailConfig,
		name: RouteNames.EMAIL_CONFIG,
	},
	// Report
	{
		path: '/salary',
		component: Salary,
		name: RouteNames.SALARY,
	},
	{
		path: '/rptbill',
		component: RptBill,
		name: RouteNames.RPT_BILL,
	},
	{
		path: '/rpttransaction',
		component: RptTransaction,
		name: RouteNames.RPT_TRANSACTION,
	},
	{
		path: '/rptitemsale',
		component: RptItemSale,
		name: RouteNames.RPT_ITEM_SALE,
	},
	{
		path: '/rptrevenue',
		component: RtpRevenue,
		name: RouteNames.RPT_REVENUE,
	},
];
