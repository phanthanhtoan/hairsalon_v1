

USE [HairSalon]
GO

/****** Object:  Table [dbo].[Bill]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[Id] [uniqueidentifier] NOT NULL,
	[BillCode] [nvarchar](100) NULL,
	[Arrivaldate] [datetime2](7) NOT NULL,
	[ArrivalTime] [datetime2](7) NOT NULL,
	[DepartureTime] [datetime2](7) NOT NULL,
	[DscPcnt] [decimal](18, 2) NOT NULL,
	[DscAmount] [decimal](18, 2) NOT NULL,
	[Note] [nvarchar](100) NULL,
	[CreatedTime] [datetime2](7) NOT NULL,
	[CreatedUser] [nvarchar](100) NULL,
	[CheckinTime] [datetime2](7) NOT NULL,
	[CheckinUser] [nvarchar](100) NULL,
	[CheckoutTime] [datetime2](7) NOT NULL,
	[CheckoutUser] [nvarchar](100) NULL,
	[Status] [int] NOT NULL,
	[ChairId] [uniqueidentifier] NOT NULL,
	[GuestName] [nvarchar](100) NULL,
	[GuestPhone] [nvarchar](100) NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
	[StaffMain] [uniqueidentifier] NOT NULL,
	[StaffShampoo] [uniqueidentifier] NOT NULL,
	[StaffSub] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillDetail]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillDetail](
	[Id] [uniqueidentifier] NOT NULL,
	[BillId] [uniqueidentifier] NOT NULL,
	[ItemId] [uniqueidentifier] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[DetailNote] [nvarchar](100) NULL,
 CONSTRAINT [PK_BillDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillHairDresser]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillHairDresser](
	[Id] [uniqueidentifier] NOT NULL,
	[BillId] [uniqueidentifier] NOT NULL,
	[StaffId] [uniqueidentifier] NOT NULL,
	[Note] [nvarchar](100) NULL,
 CONSTRAINT [PK_BillHairDresser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Chair]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chair](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[ChairTop] [int] NOT NULL,
	[ChairLeft] [int] NOT NULL,
	[ChairWidth] [int] NOT NULL,
	[ChairHeight] [int] NOT NULL,
 CONSTRAINT [PK_Chair] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[TaxNumber] [nvarchar](100) NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Phone] [nvarchar](100) NULL,
	[Email] [varchar](100) NOT NULL,
	[BirthDate] [datetime2](7) NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IGroup]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IGroup](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActice] [bit] NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_IGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemList]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemList](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[OpenPrice] [bit] NOT NULL,
	[UnitId] [uniqueidentifier] NOT NULL,
	[ItemGroupId] [uniqueidentifier] NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ItemList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IUnit]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IUnit](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActice] [bit] NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_IUnit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Salon]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salon](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[Address] [nvarchar](100) NULL,
	[Phone] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[Facebook] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
 CONSTRAINT [PK_Salon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Source]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Source](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Source] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[Address] [nvarchar](100) NULL,
	[Phone] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[Facebook] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
	[StaffGroupId] [uniqueidentifier] NOT NULL,
	[Basicsalary] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NOT NULL,
	[BeginWork] [datetime2](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StaffGroup]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StaffGroup](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_StaffGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [uniqueidentifier] NOT NULL,
	[BillId] [uniqueidentifier] NOT NULL,
	[TransactionCode] [nvarchar](100) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Note] [nvarchar](100) NULL,
	[Posteduser] [nvarchar](100) NULL,
	[PostedTime] [datetime2](7) NOT NULL,
	[LastChangeUser] [nvarchar](100) NULL,
	[LastChangeTime] [datetime2](7) NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransactionCode]    Script Date: 5/16/2021 10:44:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionCode](
	[Id] [uniqueidentifier] NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[Type] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[SalonId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_TransactionCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ItemList] ([Id], [Code], [Name], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (N'2b9db771-6736-4f07-9525-27c61783dd19', N'SP005', N'Nhuộm tóc loại 2', CAST(1000000.00 AS Decimal(18, 2)), 0, N'8c7bb95c-77b1-41be-b8ae-47a96b8878d3', N'85711877-2677-4b35-841a-39b765ce1ac1', N'f6e9f4ad-9d27-402c-80e1-fe208b38ccee', 1)
INSERT [dbo].[ItemList] ([Id], [Code], [Name], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (N'3a64073f-11fa-4147-b369-2b750714bf2f', N'SP006', N'Nhuộm tóc loại 3', CAST(1000000.00 AS Decimal(18, 2)), 0, N'f843d97e-e17a-48e6-82b9-90966feb0a43', N'e7ba55aa-dcfd-4b5f-b8b8-28b011af8812', N'633e2728-c9c7-4f89-8d72-4ef00e7a7384', 1)
INSERT [dbo].[ItemList] ([Id], [Code], [Name], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (N'5931282a-60ae-45e9-ad85-2fcb818d3944', N'SP008', N'Uốn tạo kiểu 500k', CAST(1000000.00 AS Decimal(18, 2)), 0, N'0aaf69fb-a98b-450f-a463-ffe9fe48c35c', N'0c6970f4-8ee2-4cb2-a408-98c077780e2d', N'7e966add-9ae9-4ad6-8cd1-7d6d801e463b', 1)
INSERT [dbo].[ItemList] ([Id], [Code], [Name], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (N'23eab819-1aa4-4313-a63c-507b780c3b71', N'SP007', N'Uốn tạo kiểu 100k', CAST(1000000.00 AS Decimal(18, 2)), 0, N'fb5734fb-6e05-4b05-a22f-34c72539ce8d', N'3f2f16f8-3d8f-4fd2-b70a-e4ead30265c5', N'7d1a7d85-b65b-40e5-b840-fd42a773c41d', 1)
INSERT [dbo].[ItemList] ([Id], [Code], [Name], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (N'e3b829f0-fe39-4bde-afc1-a2c2f16d42a6', N'SP003', N'Căt tóc loại 3', CAST(1000000.00 AS Decimal(18, 2)), 0, N'16eaaa8a-4de1-4691-b9f2-e2c3f85ed1bd', N'80ebd4cd-4411-41ed-bb0e-972f73184bdd', N'3d50d3e2-6264-4199-9e93-48ae4f36337e', 1)
INSERT [dbo].[ItemList] ([Id], [Code], [Name], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (N'1706f060-3bce-46aa-b9b8-ae1862bd613e', N'SP002', N'Căt tóc loại 2', CAST(1000000.00 AS Decimal(18, 2)), 0, N'61d6ad27-52aa-47af-92fa-736a6b6c22e8', N'534b34f5-78cd-41f2-be5f-9b647dac2fcc', N'93e89dea-0979-405c-80c2-ce7c1bd69ee8', 1)
INSERT [dbo].[ItemList] ([Id], [Code], [Name], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (N'3ad59f3d-2779-4c5e-a12d-dfc4f5d494ca', N'SP004', N'Nhuộm tóc loại 1', CAST(1000000.00 AS Decimal(18, 2)), 0, N'529e5ca2-de43-46aa-ab33-f703c6743919', N'6a51556b-9a26-4440-8d40-b17818b2a58f', N'b8df7699-bbe0-4ff2-be07-5b9e121b945c', 1)
INSERT [dbo].[ItemList] ([Id], [Code], [Name], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (N'2450dfa3-f6ad-466b-9522-fb27e46ea9a5', N'SP001', N'Căt tóc', CAST(1000000.00 AS Decimal(18, 2)), 0, N'6dc67261-61b8-46c3-84e2-4a07429e2eb7', N'4bd9ebd6-301f-41d7-aaf0-48c6d59c9a6a', N'83fa7bd1-b57a-4467-bf85-feec43fcaefe', 1)
GO
INSERT [dbo].[Salon] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website]) VALUES (N'd17c671d-bedc-43fe-a6b8-977c6e93f738', N'SL01', N'Hair salon Family', N'Xóm Tri - Ngô Xá - Cẩm Khê - Phú Thọ', N'0963096426', N'toanpt@gmail.com', N'toanpt.facebook', N'www.familysalon.com')
GO
INSERT [dbo].[Staff] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (N'113ac42d-c0ee-4bee-a493-1d803fc2366b', N'NV01', N'Nhân viên 1', N'', N'', N'', N'', N'', N'b9c6df86-821c-441b-9d89-5efc85d8d377', CAST(1000000.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(N'2021-05-08T16:56:50.0588088' AS DateTime2), 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[Staff] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (N'1da197f6-69ad-4a75-995e-288e159a6c7b', N'NV07', N'Trương Văn Sỹ', N'', N'', N'', N'', N'', N'dd388671-8634-4a57-a161-bcbfd4398a3f', CAST(2000000.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(N'2021-05-08T16:59:33.4445790' AS DateTime2), 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[Staff] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (N'87ba70cb-bf5f-4e97-b53e-2a18063afaa1', N'NV03', N'Nguyễn Văn Dũng', N'', N'', N'', N'', N'', N'b9c6df86-821c-441b-9d89-5efc85d8d377', CAST(1500000.00 AS Decimal(18, 2)), CAST(22.00 AS Decimal(18, 2)), CAST(N'2021-05-08T16:57:27.9409938' AS DateTime2), 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[Staff] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (N'4d1131c1-64f0-45c1-b54c-572b32601f97', N'NV04', N'Trần Thị Bình', N'', N'', N'', N'', N'', N'353a626f-a946-41ee-a0be-3691b748d6ad', CAST(0.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), CAST(N'2021-05-08T16:58:15.3215746' AS DateTime2), 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[Staff] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (N'b7cc7073-7bc4-453a-ba73-6b79c2899069', N'NV02', N'Nhân viên 1', N'', N'', N'', N'', N'', N'b9c6df86-821c-441b-9d89-5efc85d8d377', CAST(2000000.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), CAST(N'2021-05-08T16:57:01.2488972' AS DateTime2), 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[Staff] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (N'ac2c875f-2a64-4c15-82f5-a99519064d04', N'NV05', N'Phạm Thế Vĩnh', N'', N'', N'', N'', N'', N'353a626f-a946-41ee-a0be-3691b748d6ad', CAST(0.00 AS Decimal(18, 2)), CAST(17.00 AS Decimal(18, 2)), CAST(N'2021-05-08T16:58:33.7765026' AS DateTime2), 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[Staff] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (N'e8a94ebd-6dd6-44d7-b366-aaa8fdff84f1', N'NV09', N'Trần Trí Dũng', N'', N'', N'', N'', N'', N'dd388671-8634-4a57-a161-bcbfd4398a3f', CAST(0.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), CAST(N'2021-05-08T17:00:16.1427754' AS DateTime2), 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[Staff] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (N'6c286f11-8376-4b0e-b04c-c71bfabb7fee', N'NV07', N'Phạm Thanh Hà', N'', N'', N'', N'', N'', N'dd388671-8634-4a57-a161-bcbfd4398a3f', CAST(1700000.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(N'2021-05-08T16:59:57.8264055' AS DateTime2), 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[Staff] ([Id], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (N'a8633cd6-234e-4192-bafa-d7132f9ebc81', N'NV06', N'Châu Gia Kiệt', N'', N'', N'', N'', N'', N'353a626f-a946-41ee-a0be-3691b748d6ad', CAST(0.00 AS Decimal(18, 2)), CAST(14.00 AS Decimal(18, 2)), CAST(N'2021-05-08T16:59:00.6853435' AS DateTime2), 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
GO
INSERT [dbo].[StaffGroup] ([Id], [Code], [Name], [IsActive], [SalonId]) VALUES (N'353a626f-a946-41ee-a0be-3691b748d6ad', N'TP', N'Thợ phụ', 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[StaffGroup] ([Id], [Code], [Name], [IsActive], [SalonId]) VALUES (N'b9c6df86-821c-441b-9d89-5efc85d8d377', N'TC', N'Thợ chính', 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
INSERT [dbo].[StaffGroup] ([Id], [Code], [Name], [IsActive], [SalonId]) VALUES (N'dd388671-8634-4a57-a161-bcbfd4398a3f', N'TG', N'Nhân viên gội', 1, N'd17c671d-bedc-43fe-a6b8-977c6e93f738')
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [StaffMain]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [StaffShampoo]
GO
ALTER TABLE [dbo].[Bill] ADD  DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [StaffSub]
GO
ALTER TABLE [dbo].[ItemList] ADD  DEFAULT (CONVERT([bit],(0))) FOR [IsActive]
GO
