﻿using EzInvoice.Infra.ThirdParty.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EzInvoice.Infra.ThirdParty.Heldper
{
    public class HelpResponse
    {
        public static ResponseModel ResponseCommon(string status = "ERR:322", string message = "Lỗi không xác định!", object data = null)
        {
            return new ResponseModel()
            {
                status = status,
                message = message,
                data = data == null ? new { } : data
            };
        }
        public static ResponseModel Success(object data = null, string message = "Thành công")
        {
            return new ResponseModel()
            {
                status = "OK",
                message = message,
                data = data == null ? new { } : data
            };
        }
    }
}
