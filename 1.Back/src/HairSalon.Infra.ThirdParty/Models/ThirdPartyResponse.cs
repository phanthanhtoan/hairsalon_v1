﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EzInvoice.Infra.ThirdParty.Models
{
    public class ThirdPartyResponse
    {
        public string ThirdPartyInvoiceNumber { get; set; }
        public string ThirdPartyInvoiceRef { get; set; }
        public string InvoicePDF { get; set; }
        public string ConvertForStore { get; set; }
    }
}
