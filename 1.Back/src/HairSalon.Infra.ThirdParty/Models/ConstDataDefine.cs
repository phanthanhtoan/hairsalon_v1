﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EzInvoice.Infra.ThirdParty.Models
{
    public class ConstDataDefine
    {
        public const string RESPONSE_SUCCESS = "OK";
        public const string RESPONSE_ERROR = "ERR";
        public const string EXCEPTION_ERROR_CODE = "ERR:322";
        public const string CREATE_INVOICE = "CreateInvoice";
        public const string SIGN_INVOICE = "SignInvoice";
        public const string CREATE_AND_SIGN_INVOICE = "CreateAndSignInvoice";
        public const string ADJUST_INVOICE = "AdjustInvoice";
        public const string REPLACE_INVOICE = "ReplaceInvoice";
        public const string DOWNLOAD_PDF = "DownloadPDF";
        public const string CONVERT_FOR_STORE = "ConvertForStore";
    }
}
