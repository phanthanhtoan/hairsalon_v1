﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EzInvoice.Infra.ThirdParty.Models
{
    public  class ResponseModel
    {
        public string status { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
}
