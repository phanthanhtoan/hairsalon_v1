﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EzInvoice.Infra.ThirdParty.Models.VnptModels
{
    public class Product
    {
        public string Code { get; set; }
        public string ProdName { get; set; }
        public string ProdUnit { get; set; }
        public string ProdQuantity { get; set; }
        public string ProdPrice { get; set; }
        public string Amount { get; set; }
        public string Remark { get; set; }
        public string Total { get; set; }
        public string VATRate { get; set; }
        public string VATAmount { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public string Discount { get; set; }
        public string DiscountAmount { get; set; }
        public string IsSum { get; set; }
    }

    //public class VnptExtraItem
    //{
    //    public string Extra_Name { get; set; }
    //    public string Extra_Value { get; set; }
    //}

    //public class VnptExtra
    //{
    //    public VnptExtraItem Extra_item { get; set; }
    //}
    public class VnptInvoice
    {
        public string CusCode { get; set; }
        public string CusName { get; set; }
        public string CusAddress { get; set; }
        public string CusPhone { get; set; }
        public string CusTaxCode { get; set; }
        public string PaymentMethod { get; set; }
        public string CusBankName { get; set; }
        public string KindOfService { get; set; }
        public string CusBankNo { get; set; }
        public List<Product> Products { get; set; }
        public string Total { get; set; }
        public string DiscountAmount { get; set; }
        public string VATRate { get; set; }
        public string VATAmount { get; set; }
        public string Amount { get; set; }
        public string AmountInWords { get; set; }
        public string Extra { get; set; }
        // public List<VnptExtra> Extras { get; set; }
        public string ArisingDate { get; set; }
        public string PaymentStatus { get; set; }
        public string EmailDeliver { get; set; }
        public string ComName { get; set; }
        public string ComAddress { get; set; }
        public string ComTaxCode { get; set; }
        public string ComFax { get; set; }
        public string ResourceCode { get; set; }
        public string GrossValue { get; set; }
        public string GrossValue0 { get; set; }
        public string VatAmount0 { get; set; }
        public string GrossValue5 { get; set; }
        public string VatAmount5 { get; set; }
        public string GrossValue10 { get; set; }
        public string VatAmount10 { get; set; }
        public string Buyer { get; set; }
        public string Name { get; set; }
        public string ComPhone { get; set; }
        public string ComBankName { get; set; }
        public string ComBankNo { get; set; }
        public string CreateDate { get; set; }
        public string DiscountRate { get; set; }
        public string CusSignStatus { get; set; }
        public string CreateBy { get; set; }
        public string PublishBy { get; set; }
        public string Note { get; set; }
        public string ProcessInvNote { get; set; }
        public string Fkey { get; set; }
        public string GrossValue_NonTax { get; set; }
        public string CurrencyUnit { get; set; }
        public string ExchangeRate { get; set; }
        public string ConvertedAmount { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public string SMSDeliver { get; set; }
        public string Type { get; set; }
    }
    public class VnptInv
    {
        public string key { get; set; }
        public VnptInvoice Invoice { get; set; }
    }

    public class Invoices
    {
        public VnptInv Inv { get; set; }
    }

    public class AdjustInv : VnptInvoice
    {
        public string key { get; set; }
    }

    public class ReplaceInv : VnptInvoice
    {
        public string key { get; set; }
    }
}

