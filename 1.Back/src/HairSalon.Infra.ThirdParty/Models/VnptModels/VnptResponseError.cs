﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EzInvoice.Infra.ThirdParty.Models.VnptModels
{
    public class VnptResponseError
    {
        public Dictionary<string, Dictionary<string, string>> ListResponseError = new Dictionary<string, Dictionary<string, string>>
        {
            {
                ConstDataDefine.CREATE_INVOICE, new Dictionary<string, string>
                {
                    {"ERR:1", "Tài khoản đăng nhập sai hoặc không có quyền" },
                    {"ERR:3", "Dữ liệu xml đầu vào không đúng quy định" },
                    {"ERR:5", "Không phát hành được hóa đơn" },
                    {"ERR:6", "Không đủ số lượng hóa đơn cho lô thêm mới" },
                    {"ERR:7", "User name không phù hợp, không tìm thấy company tương ứng cho user" },
                    {"ERR:10", "Lô có số hóa đơn vượt quá max cho phép" },
                    {"ERR:13", "Danh sách hóa đơn tồn tại hóa đơn trùng Fkey" },
                    {"ERR:20", "Pattern và serial không phù hợp, hoặc không tồn tại hóa đơn đã đăng kí có sử dụng Pattern và serial truyền vào" },
                    {"ERR:22", "Trùng số hóa đơn" }
                }
            },
            {
                ConstDataDefine.SIGN_INVOICE, new Dictionary<string, string>
                {
                    {"ERR:1", "Tài khoản đăng nhập sai hoặc không có quyền" },
                    {"ERR:5", "Không phát hành được hóa đơn" },
                    {"ERR:6", "Danh sách Fkey không tồn tại" },
                    {"ERR:10", "Danh sách Fkey truyền vào vượt quá 200 fkey" },
                    {"ERR:15", "Danh sách Fkey đã phát hành" },
                    {"ERR:20", "Pattern và serial không phù hợp, hoặc không tồn tại hóa đơn đã đăng kí có sử dụng Pattern và serial truyền vào" },
                    {"ERR:29", "Lỗi chứng thư hết hạn" },
                    {"ERR:30", "Danh sách hóa đơn tồn tại ngày hóa đơn nhỏ hơn ngày hóa đơn đã phát hành" },
                }
            },
            {
                ConstDataDefine.CREATE_AND_SIGN_INVOICE, new Dictionary<string, string>
                {
                    {"ERR:1", "Tài khoản đăng nhập sai hoặc không có quyền" },
                    {"ERR:3", "Dữ liệu xml đầu vào không đúng quy định" },
                    {"ERR:5", "Không phát hành được hóa đơn" },
                    {"ERR:6", "Không đủ số hóa đơn cho lô phát hành" },
                    {"ERR:7", "User name không phù hợp, không tìm thấy company tương ứng cho user" },
                    {"ERR:10", "Lô có số hóa đơn vượt quá max cho phép" },
                    {"ERR:13", "Lỗi trùng fkey" },
                    {"ERR:20", "Pattern và serial không phù hợp, hoặc không tồn tại hóa đơn đã đăng kí có sử dụng Pattern và serial truyền vào" },
                    {"ERR:21", "Lỗi trùng số hóa đơn" },
                    {"ERR:29", "Lỗi chứng thư hết hạn"},
                    {"ERR:30", "Danh sách hóa đơn tồn tại ngày hóa đơn nhỏ hơn ngày hóa đơn đã phát hành" }
                }
            },
            {
                ConstDataDefine.ADJUST_INVOICE, new Dictionary<string, string>
                {
                    {"ERR:1", "Tài khoản đăng nhập sai hoặc không có quyền" },
                    {"ERR:2", "Hóa đơn cần điều chỉnh không tồn tại" },
                    {"ERR:3", "Dữ liệu xml đầu vào không đúng quy định" },
                    {"ERR:5", "Không phát hành được hóa đơn" },
                    {"ERR:6", "Dải hóa đơn cũ đã hết" },
                    {"ERR:7", "User name không phù hợp, không tìm thấy company tương ứng cho user" },
                    {"ERR:8", "Hóa đơn cần điều chỉnh đã bị thay thế. Không thể điều chỉnh được nữa" },
                    {"ERR:9", "Trạng thái hóa đơn không được điều chỉnh" },
                    {"ERR:13", "Lỗi trùng fkey" },
                    {"ERR:14", "Lỗi trong quá trình thực hiện cấp số hóa đơn" },
                    {"ERR:15", "Lỗi khi thực hiện Deserialize chuỗi hóa đơn đầu vào" },
                    {"ERR:19", "Pattern truyền vào không giống với hóa đơn cần điều chỉnh" },
                    {"ERR:20", "Dải hóa đơn hết, User/Account không có quyền với Serial/Pattern và serial không phù hợp" },
                    {"ERR:29", "Lỗi chứng thư hết hạn" },
                    {"ERR:30", "Danh sách hóa đơn tồn tại ngày hóa đơn nhỏ hơn ngày hóa đơn đã phát hành" },
                }
            },
            {
                ConstDataDefine.REPLACE_INVOICE, new Dictionary<string, string>
                {
                    {"ERR:1", "Tài khoản đăng nhập sai hoặc không có quyền" },
                    {"ERR:2", "Không tồn tại hóa đơn cần thay thế" },
                    {"ERR:3", "Dữ liệu xml đầu vào không đúng quy định" },
                    {"ERR:5", "Có lỗi trong quá trình thay thế hóa đơn" },
                    {"ERR:6", "Dải hóa đơn cũ đã hết" },
                    {"ERR:7", "User name không phù hợp, không tìm thấy company tương ứng cho user" },
                    {"ERR:8", "Hóa đơn đã được thay thế rồi. Không thể thay thế nữa" },
                    {"ERR:13", "Lỗi trùng fkey" },
                    {"ERR:14", "Lỗi trong quá trình thực hiện cấp số hóa đơn" },
                    {"ERR:15", "Lỗi khi thực hiện Deserialize chuỗi hóa đơn đầu vào" },
                    {"ERR:19", "Trạng thái hóa đơn ko được thay thế" },
                    {"ERR:20", "Pattern và serial không phù hợp" },
                    {"ERR:29", "Lỗi chứng thư hết hạn" },
                    {"ERR:30", "Danh sách hóa đơn tồn tại ngày hóa đơn nhỏ hơn ngày hóa đơn đã phát hành" },
                }
            },
            {
                ConstDataDefine.DOWNLOAD_PDF, new Dictionary<string, string>
                {
                    {"ERR:1", "Tài khoản đăng nhập sai hoặc không có quyền" },
                    {"ERR:2", "Chuỗi token không chính xác" },
                    {"ERR:4", "Không tìm thấy mẫu số tương ứng" },
                    {"ERR:5", "Có lỗi xảy ra" },
                    {"ERR:6", "Không tìm thấy hóa đơn (Chưa được phân quyền Serial hoặc hóa đơn không tồn tại)" },
                    {"ERR:7", "Không tìm thấy công ty" },
                    {"ERR:11", "Chuỗi fkey đúng định dạng nhưng không tồn tại, hoặc là của hóa đơn đã bị hủy, bị thay thế, hoặc hóa đơn chưa thanh toán" },
                }
            },
             {
                ConstDataDefine.CONVERT_FOR_STORE, new Dictionary<string, string>
                {
                    {"ERR:1", "Tài khoản đăng nhập sai hoặc không có quyền" },
                    {"ERR:2", "Chuỗi token không chính xác" },
                    {"ERR:5", "Có lỗi xảy ra" },
                    {"ERR:6", "Không tìm thấy hóa đơn (Chưa được phân quyền Serial hoặc hóa đơn không tồn tại)" },
                    {"ERR:7", "Không tìm thấy công ty" },
                    {"ERR:8", "Hóa đơn đã chuyển đổi" },
                    {"ERR:11", "Chuỗi fkey đúng định dạng nhưng không tồn tại, hoặc là của hóa đơn đã bị hủy, bị thay thế, hoặc hóa đơn chưa thanh toán" },
                }
            },
        };
    }
}
