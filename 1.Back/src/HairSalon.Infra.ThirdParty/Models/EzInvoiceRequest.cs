﻿using EzInvoice.Infra.ThirdParty.Models.VnptModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EzInvoice.Infra.ThirdParty.Models
{
    public class ResquestModel
    {
        public int Pertner { get; set; }
        public int Action { get; set; }
        public object Data { get; set; }
    }
    public class EzInvoiceRequest
    {
        public string Account { get; set; }
        public string ACpass { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public VnptInv invoice { get; set; }
        public List<Product> details { get; set; }
    }
}
