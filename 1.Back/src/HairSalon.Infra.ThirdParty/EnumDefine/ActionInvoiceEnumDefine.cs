﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EzInvoice.Infra.ThirdParty.EnumDefine
{
    /// <summary>
    /// Danh sách service được dùng của vnpt
    /// </summary>
    public enum INVOICE_ACTION
    {
        /// <summary>
        /// Phát hành hóa đơn nháp, chưa ký
        /// </summary>
        CREATED = 1,
        /// <summary>
        /// Sửa hóa đơn
        /// </summary>
        CHANGE = 2,
        /// <summary>
        /// Kí hóa đơn
        /// </summary>
        SIGN = 3,
        /// <summary>
        /// Phát hành hóa đơn và kí luôn
        /// </summary>
        CREATED_AND_SIGN = 4,
        /// <summary>
        /// Điều chỉnh tăng
        /// </summary>
        ADJUST_INCREASE = 5,
        /// <summary>
        /// Điều chỉnh giảm
        /// </summary>
        ADJUST_DECREASE = 6,
        /// <summary>
        /// Điều chỉnh thông tin
        /// </summary>
        ADJUST_INFOR = 7,
        /// <summary>
        /// Thay thế
        /// </summary>
        REPLACE = 8,
        /// <summary>
        /// Tải file PDF
        /// </summary>
        DOWNLOAD_PDF = 9,
        /// <summary>
        /// Lấy thông tin mẫu hóa đơn, ký hiệu
        /// </summary>
        FORM_SERIAL = 10,
        /// <summary>
        /// In chuyển đổi
        /// </summary>
        CONVERT_FOR_STORE = 11,
    }
}
