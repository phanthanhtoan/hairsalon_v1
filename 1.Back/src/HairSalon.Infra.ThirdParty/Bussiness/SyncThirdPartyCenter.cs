﻿using EzInvoice.Infra.ThirdParty.Bussiness.VnptBussiness;
using EzInvoice.Infra.ThirdParty.EnumDefine;
using EzInvoice.Infra.ThirdParty.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EzInvoice.Infra.ThirdParty.Bussiness
{
    public class SyncThirdPartyCenter : ISyncThirdPartyCenter
    {
        public async Task<ResponseModel> SendToThirdParty(ResquestModel data)
        {
            ResponseModel thirdPartyResult = new ResponseModel();
            switch (data.Pertner)
            {
                case (int)Partner.VNPT:
                    VnptSyncData _vnptSyncData = new VnptSyncData();
                    data.Data = new EzInvoiceRequest
                    {
                        invoice = new Models.VnptModels.VnptInv
                        {
                            key = ""
                        }
                    };
                    thirdPartyResult = await _vnptSyncData.SyncDataToVnpt(data);
                    break;
                default:
                    break;
            }
            return thirdPartyResult;
        }
    }
}
