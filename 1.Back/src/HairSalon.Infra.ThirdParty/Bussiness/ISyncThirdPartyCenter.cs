﻿using EzInvoice.Infra.ThirdParty.Models;
using System.Threading.Tasks;

namespace EzInvoice.Infra.ThirdParty.Bussiness
{
    public interface ISyncThirdPartyCenter
    {
        Task<ResponseModel> SendToThirdParty(ResquestModel ezInvoiceRequest);
    }
}