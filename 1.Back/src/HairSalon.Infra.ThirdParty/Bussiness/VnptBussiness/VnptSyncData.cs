﻿using EzInvoice.Infra.ThirdParty.EnumDefine;
using EzInvoice.Infra.ThirdParty.Heldper;
using EzInvoice.Infra.ThirdParty.Models;
using EzInvoice.Infra.ThirdParty.Models.VnptModels;
using Serilog;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Net.WebSockets;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using VnptBusinessService;
using VnptPortalService;
using VnptPublishService;
using static EzInvoice.Infra.ThirdParty.Heldper.HelpResponse;

namespace EzInvoice.Infra.ThirdParty.Bussiness.VnptBussiness
{
    public class VnptSyncData
    {
        private readonly PublishServiceSoap _publishService;
        private readonly BusinessServiceSoap _businessService;
        private readonly PortalServiceSoap _portalService;
        private readonly XmlHelper _serializeXml;
        private readonly VnptConvertRequest _convertDataRequest;
        private readonly VnptConvertResponse _convertDataResponse;
        public VnptSyncData()
        {
            _serializeXml = new XmlHelper();
            _convertDataRequest = new VnptConvertRequest(_serializeXml);
            _convertDataResponse = new VnptConvertResponse();
            _publishService = new PublishServiceSoapClient(PublishServiceSoapClient.EndpointConfiguration.PublishServiceSoap);
            _businessService = new BusinessServiceSoapClient(BusinessServiceSoapClient.EndpointConfiguration.BusinessServiceSoap);
            _portalService = new PortalServiceSoapClient(PortalServiceSoapClient.EndpointConfiguration.PortalServiceSoap);
        }
        public async Task<ResponseModel> SyncDataToVnpt(ResquestModel model)
        {
            ResponseModel response = new ResponseModel();
            switch (model.Action)
            {
                case (int)INVOICE_ACTION.CREATED:
                    response = await CreateInvoice(model.Data as EzInvoiceRequest);
                    break;
                case (int)INVOICE_ACTION.SIGN:
                    response = await SignInvoice(model.Data as EzInvoiceRequest);
                    break;
                case (int)INVOICE_ACTION.CREATED_AND_SIGN:
                    response = await CreateAndSignInvoice(model.Data as EzInvoiceRequest);
                    break;
                case (int)INVOICE_ACTION.ADJUST_INCREASE:
                    response = await AdjustInvoice(model.Data as EzInvoiceRequest, model.Action);
                    break;
                case (int)INVOICE_ACTION.ADJUST_DECREASE:
                    response = await AdjustInvoice(model.Data as EzInvoiceRequest, model.Action);
                    break;
                case (int)INVOICE_ACTION.ADJUST_INFOR:
                    response = await AdjustInvoice(model.Data as EzInvoiceRequest, model.Action);
                    break;
                case (int)INVOICE_ACTION.REPLACE:
                    response = await ReplaceInvoice(model.Data as EzInvoiceRequest);
                    break;
                case (int)INVOICE_ACTION.DOWNLOAD_PDF:
                    response = await DownloadPDF(model.Data as EzInvoiceRequest);
                    SaveFilePDF(response.data as ThirdPartyResponse);
                    break;
                case (int)INVOICE_ACTION.CONVERT_FOR_STORE:
                    response = await ConvertForStore(model.Data as EzInvoiceRequest);
                    SaveFileHtml(response.data as ThirdPartyResponse);
                    break;
            }
            return response;
        }

        private void SaveFileHtml(ThirdPartyResponse model)
        {
            string content = model.ConvertForStore;
            var filePath = String.Format(@"D:\file{0}.html", model.ThirdPartyInvoiceRef);
            File.WriteAllText(filePath, content);
            model.ConvertForStore = filePath;
        }

        private void SaveFilePDF(ThirdPartyResponse model)
        {
            var filePath = String.Format(@"D:\file{0}.pdf", model.ThirdPartyInvoiceRef);
            byte[] bytes = Convert.FromBase64String(model.InvoicePDF ?? model.ConvertForStore);
            System.IO.FileStream stream =
            new FileStream(filePath, FileMode.CreateNew);
            System.IO.BinaryWriter writer =
                new BinaryWriter(stream);
            writer.Write(bytes, 0, bytes.Length);
            writer.Close();
            model.InvoicePDF = filePath;
        }

        /// <summary>
        /// In chuyển đổi
        /// </summary>
        /// <param name="model"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private async Task<ResponseModel> ConvertForStore(EzInvoiceRequest model)
        {
            convertForStoreFkeyRequest request = _convertDataRequest.ConvertModelConvertForStoreRequest(model);
            try
            {
                var vnptResult = await _portalService.convertForStoreFkeyAsync(request);
                var a = vnptResult.Body.convertForStoreFkeyResult;
                ResponseModel result = _convertDataResponse.ConvertVnptResponse(model, a, ConstDataDefine.CONVERT_FOR_STORE);

                Log.Information("DownloadPDF :" + request.Body.fkey);
                Log.Information("DownloadPDF Result " + result.message + " " +
               (result.status == ConstDataDefine.RESPONSE_SUCCESS ? (result.data as ThirdPartyResponse).ConvertForStore : ""));
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("DownloadPDF: Message {0} : xmlInvoiceData {1}", ex.Message, request.Body.fkey));
                return ResponseCommon(ConstDataDefine.EXCEPTION_ERROR_CODE, ex.Message, ex);
            }
        }

        /// <summary>
        /// Tải dữ liệu về lưu giữ PDF
        /// </summary>
        /// <param name="model"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private async Task<ResponseModel> DownloadPDF(EzInvoiceRequest model)
        {
            downloadInvPDFFkeyRequest request = _convertDataRequest.ConvertDownloadPDFRequest(model);
            try
            {
                var vnptResult = await _portalService.downloadInvPDFFkeyAsync(request);
                var a = vnptResult.Body.downloadInvPDFFkeyResult;
                ResponseModel result = _convertDataResponse.ConvertVnptResponse(model, a, ConstDataDefine.DOWNLOAD_PDF);

                Log.Information("DownloadPDF :" + request.Body.fkey);
                Log.Information("DownloadPDF Result " + result.message + " " +
               (result.status == ConstDataDefine.RESPONSE_SUCCESS ? (result.data as ThirdPartyResponse).InvoicePDF : ""));
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("DownloadPDF: Message {0} : xmlInvoiceData {1}", ex.Message, request.Body.fkey));
                return ResponseCommon(ConstDataDefine.EXCEPTION_ERROR_CODE, ex.Message, ex);
            }
        }

        /// <summary>
        /// Thay thế hóa đơn
        /// </summary>
        /// <param name="model"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private async Task<ResponseModel> ReplaceInvoice(EzInvoiceRequest model)
        {
            ReplaceInvoiceActionRequest request = _convertDataRequest.ConvertReplaceInvoiceRequest(model);
            try
            {
                var vnptResult = await _businessService.ReplaceInvoiceActionAsync(request);
                var a = vnptResult.Body.ReplaceInvoiceActionResult;
                ResponseModel result = _convertDataResponse.ConvertVnptResponse(model, a, ConstDataDefine.REPLACE_INVOICE);

                Log.Information("ReplaceInvoice :" + request.Body.xmlInvData);
                Log.Information("ReplaceInvoice Result " + result.message + " " +
               (result.status == ConstDataDefine.RESPONSE_SUCCESS ? (result.data as ThirdPartyResponse).ThirdPartyInvoiceNumber : ""));
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("ReplaceInvoice: Message {0} : xmlInvoiceData {1}", ex.Message, request.Body.xmlInvData));
                return ResponseCommon(ConstDataDefine.EXCEPTION_ERROR_CODE, ex.Message, ex);
            }
        }

        /// <summary>
        /// Điều chỉnh hóa đơn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<ResponseModel> AdjustInvoice(EzInvoiceRequest model, int action)
        {
            AdjustInvoiceActionRequest request = _convertDataRequest.ConvertAdjustInvoiceRequest(model, action);
            try
            {
                var vnptResult = await _businessService.AdjustInvoiceActionAsync(request);
                var a = vnptResult.Body.AdjustInvoiceActionResult;
                ResponseModel result = _convertDataResponse.ConvertVnptResponse(model, a, ConstDataDefine.ADJUST_INVOICE);

                Log.Information("AdjustInvoice :" + request.Body.xmlInvData);
                Log.Information("AdjustInvoice Result " + result.message + " " +
               (result.status == ConstDataDefine.RESPONSE_SUCCESS ? (result.data as ThirdPartyResponse).ThirdPartyInvoiceNumber : ""));
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("AdjustInvoice: Message {0} : xmlInvoiceData {1}", ex.Message, request.Body.xmlInvData));
                return ResponseCommon(ConstDataDefine.EXCEPTION_ERROR_CODE, ex.Message, ex);
            }
        }

        /// <summary>
        /// Phát hành hóa đơn và ký luôn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<ResponseModel> CreateAndSignInvoice(EzInvoiceRequest model)
        {
            ImportAndPublishInvRequest request = _convertDataRequest.ConvertCreateAndSignInvoiceRequest(model);
            try
            {
                var vnptResult = await _publishService.ImportAndPublishInvAsync(request);
                var a = vnptResult.Body.ImportAndPublishInvResult;
                ResponseModel result = _convertDataResponse.ConvertVnptResponse(model, a, ConstDataDefine.CREATE_AND_SIGN_INVOICE);

                Log.Information("CreateAndSignInvoice :" + request.Body.xmlInvData);
                Log.Information("CreateAndSignInvoice Result " + result.message + " " +
               (result.status == ConstDataDefine.RESPONSE_SUCCESS ? (result.data as ThirdPartyResponse).ThirdPartyInvoiceNumber : ""));
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("CreateAndSignInvoice: Message {0} : xmlInvoiceData {1}", ex.Message, request.Body.xmlInvData));
                return ResponseCommon(ConstDataDefine.EXCEPTION_ERROR_CODE, ex.Message, ex);
            }
        }

        /// <summary>
        /// Ký hóa đơn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<ResponseModel> SignInvoice(EzInvoiceRequest model)
        {
            PublishInvFkeyRequest request = _convertDataRequest.ConvertSignRequest(model);
            try
            {
                var vnptResult = await _publishService.PublishInvFkeyAsync(request);
                var a = vnptResult.Body.PublishInvFkeyResult;
                ResponseModel result = _convertDataResponse.ConvertVnptResponse(model, a, ConstDataDefine.SIGN_INVOICE);
                Log.Information("SignInvoice :" + request.Body.lsFkey);
                Log.Information("SignInvoice Result " + result.message + " " +
               (result.status == ConstDataDefine.RESPONSE_SUCCESS ? (result.data as ThirdPartyResponse).ThirdPartyInvoiceNumber : ""));
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("SignInvoice: Message {0} : xmlInvoiceData {1}", ex.Message, request.Body.lsFkey));
                return ResponseCommon(ConstDataDefine.EXCEPTION_ERROR_CODE, ex.Message, ex);
            }
        }

        /// <summary>
        /// Phát hành hóa đơn chưa ký
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<ResponseModel> CreateInvoice(EzInvoiceRequest model)
        {
            ImportInvByPatternRequest request = _convertDataRequest.ConvertCreateInvoiceRequest(model);
            try
            {
                var vnptResult = await _publishService.ImportInvByPatternAsync(request);
                var a = vnptResult.Body.ImportInvByPatternResult;
                ResponseModel result = _convertDataResponse.ConvertVnptResponse(model, a, ConstDataDefine.CREATE_INVOICE);
                
                Log.Information("CreateInvoice :" + request.Body.xmlInvData);
                Log.Information("CreateInvoice Result " + result.message + " " +
               (result.status == ConstDataDefine.RESPONSE_SUCCESS ? (result.data as ThirdPartyResponse).ThirdPartyInvoiceNumber : ""));
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("CreateInvoice: Message {0} : xmlInvoiceData {1}", ex.Message, request.Body.xmlInvData));
                return ResponseCommon(ConstDataDefine.EXCEPTION_ERROR_CODE, ex.Message, ex);
            }
        }
    }
}
