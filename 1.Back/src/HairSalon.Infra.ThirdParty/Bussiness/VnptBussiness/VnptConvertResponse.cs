﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VnptPublishService;
using static EzInvoice.Infra.ThirdParty.Heldper.HelpResponse;

namespace EzInvoice.Infra.ThirdParty.Models.VnptModels
{
    public class VnptConvertResponse
    {
        private readonly VnptResponseError thirdPartyResponseError = new VnptResponseError();
        /// <summary>
        /// Xử lý output của vnpt sau khi gọi hàm
        /// </summary>
        /// <param name="vnptResult"></param>
        /// <returns></returns>
        public ResponseModel ConvertVnptResponse(EzInvoiceRequest model, string vnptResult, string action = "")
        {
            if (vnptResult.Contains(ConstDataDefine.RESPONSE_ERROR))
            {
                var errorCreateAndSign = thirdPartyResponseError.ListResponseError.FirstOrDefault(e => e.Key == action);
                if (errorCreateAndSign.Value != null)
                {
                    Log.Information("RESPONSE_ERROR");
                    var errorDetail = errorCreateAndSign.Value.FirstOrDefault(e => e.Key == vnptResult).Value;
                    return ResponseCommon(vnptResult, errorDetail, vnptResult);
                }
            }
            if (action == ConstDataDefine.DOWNLOAD_PDF)
            {
                return Success(new ThirdPartyResponse
                {
                    InvoicePDF = vnptResult,
                    ThirdPartyInvoiceRef = model.invoice.key,
                });
            }
            if (action == ConstDataDefine.CONVERT_FOR_STORE)
            {
                return Success(new ThirdPartyResponse
                {
                    ConvertForStore = vnptResult,
                    ThirdPartyInvoiceRef = model.invoice.key,
                });
            }
            if (vnptResult.Contains(ConstDataDefine.RESPONSE_SUCCESS))
            {
                ThirdPartyResponse data = new ThirdPartyResponse();
                const string SEPARATOR_SUCCESS = "_";
                if (vnptResult.Contains(SEPARATOR_SUCCESS))
                {
                    var splitBySeparator = vnptResult.Split(SEPARATOR_SUCCESS.ToCharArray());
                    if (splitBySeparator.Length > 1)
                    {
                        data.ThirdPartyInvoiceNumber = splitBySeparator[1].ToString();
                    }
                }
                data.ThirdPartyInvoiceRef = model.invoice.key;
                return Success(data, vnptResult);
            }
            return ResponseCommon(ConstDataDefine.RESPONSE_ERROR, vnptResult, vnptResult);
        }
    }
}
