﻿using EzInvoice.Infra.ThirdParty.EnumDefine;
using EzInvoice.Infra.ThirdParty.Heldper;
using EzInvoice.Infra.ThirdParty.Models;
using EzInvoice.Infra.ThirdParty.Models.VnptModels;
using System.Collections.Generic;
using VnptBusinessService;
using VnptPortalService;
using VnptPublishService;

namespace EzInvoice.Infra.ThirdParty.Bussiness.VnptBussiness
{
    public  class VnptConvertRequest
    {
        private const int NOT_CONVERT_TCVN3_TO_UNICODE = 0;
        private const string KEY = "202010100105";
        private const string Newkey = "202010100103";
        private const string InvoiceNo = "37";

        private readonly XmlHelper _serializeXml;
        public VnptConvertRequest(XmlHelper serializeXml)
        {
            this._serializeXml = serializeXml;
        }

        /// <summary>
        ///  Request phát hành hóa đơn chưa ký
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ImportInvByPatternRequest ConvertCreateInvoiceRequest(EzInvoiceRequest model)
        {
            //List<Product> itemProducts = details.ForEach(item => new Product()
            //{
            //    ProdName = item.ItemName,
            //    ProdUnit = item.UnitCode,
            //    ProdQuantity = item.Quantity.ToString(),
            //    ProdPrice = item.UnitPrice.ToString(),
            //    Total = item.TotalAmountWithoutVat.ToString(),
            //    Discount = item.DiscountPercent.ToString(),
            //    DiscountAmount = item.DiscountAmount.ToString(),
            //    VATRate = item.VatPercentage.ToString(),
            //    VATAmount = item.VatAmount.ToString(),
            //    Amount = item.TotalAmount.ToString(),
            //    Extra1 = "",
            //    Extra2 = "",
            //    IsSum = "0",
            //    Remark = "Ghi chú sản phẩm",
            //}).ToList();
            List<Product> itemProducts = GetListProuct();
            VnptInvoice vnptInvoice = Invoice(itemProducts);
            VnptInvoice invoice = vnptInvoice;

            //List<VnptExtra> extras = new List<VnptExtra>();
            //VnptExtra extra = new VnptExtra();
            //VnptExtraItem extraItem = new VnptExtraItem();
            //extra.Extra_item = extraItem;
            //extras.Add(extra);
            //invoice.Extras = extras;

            VnptInv inv = new VnptInv
            {
                Invoice = invoice,
                key = KEY,
            };

            Invoices invoices = new Invoices
            {
                Inv = inv
            };

            var vnptBody = new List<Invoices>();
            vnptBody.Add(invoices);

            var xml = _serializeXml.Serialize(invoices);

            var result = new ImportInvByPatternRequestBody()
            {
                Account = "ezcloudadmin",
                ACpass = "Einv@oi@vn#pt20",
                convert = NOT_CONVERT_TCVN3_TO_UNICODE,
                username = "ezcloudws",
                password = "123456aA@",
                pattern = "01GTKT0/001",
                serial = "AA/20E",
                xmlInvData = xml
            };
            ImportInvByPatternRequest importAndPublishInvRequest = new ImportInvByPatternRequest();
            importAndPublishInvRequest.Body = result;
            model.invoice.key = KEY;
            return importAndPublishInvRequest;
        }

        /// <summary>
        /// Request ký hóa đơn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public PublishInvFkeyRequest ConvertSignRequest(EzInvoiceRequest model)
        {
            PublishInvFkeyRequest result = new PublishInvFkeyRequest
            {
                Body = new PublishInvFkeyRequestBody
                {
                    Account = "ezcloudadmin",
                    ACpass = "Einv@oi@vn#pt20",
                    lsFkey = KEY,
                    username = "ezcloudws",
                    password = "123456aA@",
                    pattern = "01GTKT0/001",
                    serial = "AA/20E",
                }
            };
            return result;
        }

        /// <summary>
        /// Request phát hành hóa đơn ký luôn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ImportAndPublishInvRequest ConvertCreateAndSignInvoiceRequest(EzInvoiceRequest model)
        {
            //List<Product> itemProducts = details.ForEach(item => new Product()
            //{
            //    ProdName = item.ItemName,
            //    ProdUnit = item.UnitCode,
            //    ProdQuantity = item.Quantity.ToString(),
            //    ProdPrice = item.UnitPrice.ToString(),
            //    Total = item.TotalAmountWithoutVat.ToString(),
            //    Discount = item.DiscountPercent.ToString(),
            //    DiscountAmount = item.DiscountAmount.ToString(),
            //    VATRate = item.VatPercentage.ToString(),
            //    VATAmount = item.VatAmount.ToString(),
            //    Amount = item.TotalAmount.ToString(),
            //    Extra1 = "",
            //    Extra2 = "",
            //    IsSum = "0",
            //    Remark = "Ghi chú sản phẩm",
            //}).ToList();
            List<Product> itemProducts = GetListProuct();
            VnptInvoice vnptInvoice = Invoice(itemProducts);
            VnptInvoice invoice = vnptInvoice;

            //List<VnptExtra> extras = new List<VnptExtra>();
            //VnptExtra extra = new VnptExtra();
            //VnptExtraItem extraItem = new VnptExtraItem();
            //extra.Extra_item = extraItem;
            //extras.Add(extra);
            //invoice.Extras = extras;

            VnptInv inv = new VnptInv
            {
                Invoice = invoice,
                key = KEY,
            };

            Invoices invoices = new Invoices
            {
                Inv = inv
            };

            var vnptBody = new List<Invoices>();
            vnptBody.Add(invoices);

            var xml = _serializeXml.Serialize(invoices);

            ImportAndPublishInvRequest importAndPublishInvRequest = new ImportAndPublishInvRequest
            {
                Body = new ImportAndPublishInvRequestBody
                {
                    Account = "ezcloudadmin",
                    ACpass = "Einv@oi@vn#pt20",
                    convert = NOT_CONVERT_TCVN3_TO_UNICODE,
                    username = "ezcloudws",
                    password = "123456aA@",
                    pattern = "01GTKT0/001",
                    serial = "AA/20E",
                    xmlInvData = xml
                },
            };
            model.invoice.key = KEY;
            return importAndPublishInvRequest;
        }

        /// <summary>
        /// Request điều chỉnh hóa đơn
        /// </summary>
        /// <param name="model"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public AdjustInvoiceActionRequest ConvertAdjustInvoiceRequest(EzInvoiceRequest model, int action)
        {
            List<Product> itemProducts = GetListProuct();
            AdjustInv invoice = new AdjustInv
            {
                key = Newkey,
                CusCode = "MAKH",
                CusName = "Tên Khách Hàng",
                CusAddress = "CusAddress",
                CusPhone = "0963096426",
                CusTaxCode = "123456",
                PaymentMethod = "TM",
                CusBankName = "Tên Tài khoản",
                CusBankNo = "123456",
                CusSignStatus = "",
                ArisingDate = "08/10/2020",
                EmailDeliver = "toan.phan@ezcloud.vn",
                Buyer = "Phan Thanh Toàn điều chỉnh thông tin",
                ComBankName = "Tài khoản đơn vị",
                ComBankNo = "6454",
                ComFax = "",
                ComName = "ComName",
                ComPhone = "12232423423",
                ComTaxCode = "12232423423",
                ComAddress = "ComAddress",
                CreateBy = "CreateBy",
                CreateDate = "08/10/2020",
                GrossValue = "0",
                GrossValue0 = "0",
                VatAmount0 = "0",
                GrossValue5 = "0",
                VatAmount5 = "0",
                GrossValue10 = "0",
                VatAmount10 = "0",
                GrossValue_NonTax = "0",
                Total = "2000000",
                DiscountRate = "0",
                DiscountAmount = "0",
                VATRate = "10",
                VATAmount = "200000",
                Amount = "2200000",
                AmountInWords = MoneyToText.DocTienBangChu(110000, ""),
                CurrencyUnit = "VND",
                ExchangeRate = "1",
                ConvertedAmount = "1",
                Extra = "",
                Extra1 = "",
                Extra2 = "",
                KindOfService = "09",
                Name = "Name",
                Note = "Note",
                PaymentStatus = "0",
                ProcessInvNote = "",
                PublishBy = "",
                ResourceCode = "",
                Products = itemProducts,
                Type = ConvertInvoiceActionToTypeAdjust(action),
            };

            var xml = _serializeXml.Serialize(invoice);


            AdjustInvoiceActionRequest importAndPublishInvRequest = new AdjustInvoiceActionRequest
            {
                Body = new AdjustInvoiceActionRequestBody
                {
                    Account = "ezcloudadmin",
                    ACpass = "Einv@oi@vn#pt20",
                    convert = NOT_CONVERT_TCVN3_TO_UNICODE,
                    username = "ezcloudws",
                    pass = "123456aA@",
                    pattern = "01GTKT0/001",
                    serial = "AA/20E",
                    xmlInvData = xml,
                    fkey = KEY,
                },
            };
            model.invoice.key = Newkey;
            return importAndPublishInvRequest;
        }

        /// <summary>
        /// Request thay thế hóa đơn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ReplaceInvoiceActionRequest ConvertReplaceInvoiceRequest(EzInvoiceRequest model)
        {
            List<Product> itemProducts = GetListProuct();
            ReplaceInv invoice = new ReplaceInv
            {
                key = Newkey,
                CusCode = "MAKH",
                CusName = "Tên Khách Hàng",
                CusAddress = "CusAddress",
                CusPhone = "0963096426",
                CusTaxCode = "123456",
                PaymentMethod = "TM",
                CusBankName = "Tên Tài khoản",
                CusBankNo = "123456",
                CusSignStatus = "",
                ArisingDate = "08/10/2020",
                EmailDeliver = "toan.phan@ezcloud.vn",
                Buyer = "Phan Thanh Toàn điều chỉnh thông tin",
                ComBankName = "Tài khoản đơn vị",
                ComBankNo = "6454",
                ComFax = "",
                ComName = "ComName",
                ComPhone = "12232423423",
                ComTaxCode = "12232423423",
                ComAddress = "ComAddress",
                CreateBy = "CreateBy",
                CreateDate = "08/10/2020",
                GrossValue = "0",
                GrossValue0 = "0",
                VatAmount0 = "0",
                GrossValue5 = "0",
                VatAmount5 = "0",
                GrossValue10 = "0",
                VatAmount10 = "0",
                GrossValue_NonTax = "0",
                Total = "2000000",
                DiscountRate = "0",
                DiscountAmount = "0",
                VATRate = "10",
                VATAmount = "200000",
                Amount = "2200000",
                AmountInWords = MoneyToText.DocTienBangChu(110000, ""),
                CurrencyUnit = "VND",
                ExchangeRate = "1",
                ConvertedAmount = "1",
                Extra = "",
                Extra1 = "",
                Extra2 = "",
                KindOfService = "09",
                Name = "Name",
                Note = "Note",
                PaymentStatus = "0",
                ProcessInvNote = "",
                PublishBy = "",
                ResourceCode = "",
                Products = itemProducts,
            };

            var xml = _serializeXml.Serialize(invoice);

            ReplaceInvoiceActionRequest importAndPublishInvRequest = new ReplaceInvoiceActionRequest
            {
                Body = new  ReplaceInvoiceActionRequestBody
                {
                    Account = "ezcloudadmin",
                    ACpass = "Einv@oi@vn#pt20",
                    convert = NOT_CONVERT_TCVN3_TO_UNICODE,
                    username = "ezcloudws",
                    pass = "123456aA@",
                    pattern = "01GTKT0/001",
                    serial = "AA/20E",
                    xmlInvData = xml,
                    fkey = KEY,
                },
            };
            model.invoice.key = Newkey;
            return importAndPublishInvRequest;
        }

        /// <summary>
        /// Request in chuyển đổi
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public convertForStoreFkeyRequest ConvertModelConvertForStoreRequest(EzInvoiceRequest model)
        {
            return new convertForStoreFkeyRequest
            {
                Body = new convertForStoreFkeyRequestBody
                {
                    fkey = KEY,
                    userName = "ezcloudws",
                    userPass = "123456aA@",
                }
            };
        }

        /// <summary>
        /// Request tải file PDF
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public downloadInvPDFFkeyRequest ConvertDownloadPDFRequest(EzInvoiceRequest model)
        {
            return new downloadInvPDFFkeyRequest
            {
                Body = new  downloadInvPDFFkeyRequestBody
                {
                    fkey = KEY,
                    userName = "ezcloudws",
                    userPass = "123456aA@",
                }
            };
        }

        private static VnptInvoice Invoice(List<Product> itemProducts)
        {
            return new VnptInvoice
            {
                CusCode = "MAKH",
                CusName = "Tên Khách Hàng",
                CusAddress = "CusAddress",
                CusPhone = "0963096426",
                CusTaxCode = "123456",
                PaymentMethod = "TM",
                CusBankName = "Tên Tài khoản",
                CusBankNo = "123456",
                CusSignStatus = "",
                ArisingDate = "14/10/2020",
                EmailDeliver = "toan.phan@ezcloud.vn",
                Buyer = "Buyer",
                ComBankName = "Tài khoản đơn vị",
                ComBankNo = "6454",
                ComFax = "",
                ComName = "ComName",
                ComPhone = "12232423423",
                ComTaxCode = "12232423423",
                ComAddress = "ComAddress",
                CreateBy = "CreateBy",
                CreateDate = "14/10/2020",
                GrossValue = "0",
                GrossValue0 = "0",
                VatAmount0 = "0",
                GrossValue5 = "0",
                VatAmount5 = "0",
                GrossValue10 = "0",
                VatAmount10 = "0",
                GrossValue_NonTax = "0",
                Total = "1000000",
                DiscountRate = "0",
                DiscountAmount = "0",
                VATRate = "10",
                VATAmount = "100000",
                Amount = "1100000",
                AmountInWords = MoneyToText.DocTienBangChu(110000, ""),
                CurrencyUnit = "VND",
                ExchangeRate = "1",
                ConvertedAmount = "1",
                Extra = "",
                Extra1 = "",
                Extra2 = "",
                KindOfService = "09",
                Name = "Name",
                Note = "Note",
                PaymentStatus = "1",
                ProcessInvNote = "",
                PublishBy = "",
                ResourceCode = "",
                Products = itemProducts
            };
        }

        private static List<Product> GetListProuct()
        {
            List<Product> itemProducts = new List<Product>();
            itemProducts.Add(new Product
            {
                ProdName = "Sản phẩm 1",
                ProdQuantity = "1",
                Code = "Code1",
                Amount = "1100000",
                IsSum = "0",
                ProdPrice = "1000000",
                DiscountAmount = "0",
                VATAmount = "100000",
                VATRate = "10",
                Discount = "0",
                Total = "1000000",
                ProdUnit = "unit",
            });
            itemProducts.Add(new Product
            {
                ProdName = "Sản phẩm 2",
                ProdQuantity = "1",
                Code = "Code1",
                Amount = "1100000",
                IsSum = "0",
                ProdPrice = "1000000",
                DiscountAmount = "0",
                VATAmount = "100000",
                VATRate = "10",
                Discount = "0",
                Total = "1000000",
                ProdUnit = "unit2",
            });
            return itemProducts;
        }

        private string ConvertInvoiceActionToTypeAdjust(int invoiceAction)
        {
            string type = "2";
            switch (invoiceAction)
            {
                case (int)INVOICE_ACTION.ADJUST_INCREASE:
                    type = "2";
                    break;
                case (int)INVOICE_ACTION.ADJUST_DECREASE:
                    type = "3";
                    break;
                case (int)INVOICE_ACTION.ADJUST_INFOR:
                    type = "4";
                    break;
                default:
                    break;
            }
            return type;
        }
    }
}
