﻿using System;
using NetDevPack.Messaging;
namespace HairSalon.Domain.Events
{
    public class BillRegisterdEvent : Event
    {
        public BillRegisterdEvent(Guid id, string billCode, DateTime arrivaldate, DateTime arrivalTime, DateTime departureTime, decimal dscPcnt,
                    decimal dscAmount, string note, DateTime createdTime, string createdUser, DateTime checkinTime,
                    string checkinUser, DateTime checkoutTime, string checkoutUser, int status, Guid chairId, string guestName,
                    string guestPhone, Guid customerId, Guid salonId, Guid staffMain, Guid staffSub, Guid staffShampoo)
        {
            Id =id;
            BillCode = billCode;
            Arrivaldate = arrivaldate;
            ArrivalTime = arrivalTime;
            DepartureTime = departureTime;
            DscPcnt = dscPcnt;
            DscAmount = dscAmount;
            Note = note;
            CreatedTime = createdTime;
            CreatedUser = createdUser;
            CheckinTime = checkinTime;
            CheckinUser = checkinUser;
            CheckoutTime = checkoutTime;
            CheckoutUser = checkoutUser;
            Status = status;
            ChairId = chairId;
            GuestName = guestName;
            GuestPhone = guestPhone;
            CustomerId = customerId;
            SalonId = salonId;
            StaffMain = staffMain;
            StaffSub = staffSub;
            StaffShampoo = staffShampoo;
        }
        public Guid Id { get; private set; }
        public string BillCode { get; private set; }
        public DateTime Arrivaldate { get; private set; }
        public DateTime ArrivalTime { get; private set; }
        public DateTime DepartureTime { get; private set; }
        public decimal DscPcnt { get; private set; }
        public decimal DscAmount { get; private set; }
        public string Note { get; private set; }
        public DateTime CreatedTime { get; private set; }
        public string CreatedUser { get; private set; }
        public DateTime CheckinTime { get; private set; }
        public string CheckinUser { get; private set; }
        public DateTime CheckoutTime { get; private set; }
        public string CheckoutUser { get; private set; }
        public int Status { get; private set; }
        public Guid ChairId { get; private set; }
        public string GuestName { get; private set; }
        public string GuestPhone { get; private set; }
        public Guid CustomerId { get; private set; }
        public Guid SalonId { get; private set; }
        public Guid StaffMain { get; private set; }
        public Guid StaffSub { get; private set; }
        public Guid StaffShampoo { get; private set; }
    }
}
