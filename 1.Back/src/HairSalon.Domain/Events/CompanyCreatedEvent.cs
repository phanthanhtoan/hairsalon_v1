﻿using System;
using NetDevPack.Messaging;

namespace HairSalon.Domain.Events
{
    public class CompanyCreatedEvent : Event
    {
        public CompanyCreatedEvent(long id, string name, string email, string taxNumber)
        {
            Id = id;
            Name = name;
            Email = email;
            TaxNumber = taxNumber;
            AggregateId = Guid.NewGuid();
        }
        public long Id { get; set; }

        public string Name { get; private set; }

        public string Email { get; private set; }

        public string TaxNumber { get; private set; }
    }
}