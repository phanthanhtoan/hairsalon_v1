﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace HairSalon.Domain.Events
{
    public class CompanyEventHandler :
        INotificationHandler<CompanyCreatedEvent>
    {
        public Task Handle(CompanyCreatedEvent message, CancellationToken cancellationToken)
        {
            // Send some greetings e-mail

            return Task.CompletedTask;
        }

    }
}