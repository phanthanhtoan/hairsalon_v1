﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class Source : Entity, IAggregateRoot
    {
        public Source(string code, string name, bool isActive)
        {
            Id = Guid.NewGuid();
            Code = code;
            Name = name;
            IsActive = isActive;
        }
        protected Source() { }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public bool IsActive { get; private set; }
    }
}
