﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class IGroup : Entity, IAggregateRoot
    {
        public IGroup(string code, string name, bool isActice, Guid salonId)
        {
            Id = Guid.NewGuid();
            Code = code;
            Name = name;
            IsActice = isActice;
            SalonId = salonId;
        }
        protected IGroup() { }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public bool IsActice { get; private set; }
        public Guid SalonId { get; private set; }
    }
}
