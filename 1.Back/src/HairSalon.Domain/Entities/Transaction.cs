﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class Transaction : Entity, IAggregateRoot
    {
        public Transaction(Guid billId, string transactionCode, decimal amount, string note, string posteduser, DateTime postedTime,
            string lastChangeUser, DateTime lastChangeTime, Guid salonId)
        {
            Id = Guid.NewGuid();
            BillId = billId;
            TransactionCode = transactionCode;
            Amount = amount;
            Note = note;
            Posteduser = posteduser;
            PostedTime = postedTime;
            LastChangeUser = lastChangeUser;
            LastChangeTime = lastChangeTime;
            SalonId = salonId;
        }
        protected Transaction() { }

        public Guid BillId { get; private set; }
        public string TransactionCode { get; private set; }
        public decimal Amount { get; private set; }
        public string Note { get; private set; }
        public string Posteduser { get; private set; }
        public DateTime PostedTime { get; private set; }
        public string LastChangeUser { get; private set; }
        public DateTime LastChangeTime { get; private set; }
        public Guid SalonId { get; private set; }
    }
}
