﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class Staff : Entity, IAggregateRoot
    {
        public Staff(string code, string name, string address, string phone,
            string email, string facebook, string website, Guid staffGroupId, decimal basicsalary,
            decimal commission, DateTime beginWork, bool isActive, Guid salonId)
        {
            Id = Guid.NewGuid();
            Code = code;
            Name = name;
            Address = address;
            Phone = phone;
            Email = email;
            Facebook = facebook;
            Website = website;
            StaffGroupId = staffGroupId;
            Basicsalary = basicsalary;
            Commission = commission;
            BeginWork = beginWork;
            IsActive = isActive;
            SalonId = salonId;
        }
        protected Staff() { }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public string Address { get; private set; }
        public string Phone { get; private set; }
        public string Email { get; private set; }
        public string Facebook { get; private set; }
        public string Website { get; private set; }
        public Guid StaffGroupId { get; private set; }
        public decimal Basicsalary { get; private set; }
        public decimal Commission { get; private set; }
        public DateTime BeginWork { get; private set; }
        public bool IsActive { get; private set; }
        public Guid SalonId { get; private set; }
    }
}
