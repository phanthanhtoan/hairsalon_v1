﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class BillDetail : Entity, IAggregateRoot
    {
        public BillDetail(Guid billId, Guid itemId, decimal price, decimal quantity, string detailNote)
        {
            Id = Guid.NewGuid();
            BillId = billId;
            ItemId = itemId;
            Price = price;
            Quantity = quantity;
            DetailNote = detailNote;
        }
        protected BillDetail() { }

        public Guid BillId { get; private set; }
        public Guid ItemId { get; private set; }
        public decimal Price { get; private set; }
        public decimal Quantity { get; private set; }
        public string DetailNote { get; private set; }
    }
}
