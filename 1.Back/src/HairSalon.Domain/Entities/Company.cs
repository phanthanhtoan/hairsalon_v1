﻿using System;
using NetDevPack.Domain;

namespace HairSalon.Domain.Entities
{
    public class Company : Entity, IAggregateRoot
    {
        public Company(long id, string name, string email, string taxNumber)
        {
            Id = id;
            Name = name;
            Email = email;
            TaxNumber = taxNumber;
        }

        public Company(string name, string email, string taxNumber)
        {
            Name = name;
            Email = email;
            TaxNumber = taxNumber;
        }

        new public long Id { get; private set; }

        // Empty constructor for EF
        protected Company() { }

        public string Name { get; private set; }

        public string Email { get; private set; }

        public string TaxNumber { get; private set; }
    }
}