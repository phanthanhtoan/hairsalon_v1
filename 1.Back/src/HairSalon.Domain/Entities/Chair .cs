﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class Chair : Entity, IAggregateRoot
    {
        public Chair(string code, string name, bool isActive, int chairTop,
            int chairLeft, int chairWidth, int chairHeight, Guid salonId)
        {
            Id = Guid.NewGuid();
            Code = code;
            Name = name;
            IsActive = isActive;
            ChairTop = chairTop;
            ChairLeft = chairLeft;
            ChairWidth = chairWidth;
            ChairHeight = chairHeight;
            SalonId = salonId;
        }
        protected Chair() { }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public bool IsActive { get; private set; }
        public int ChairTop { get; private set; }
        public int ChairLeft { get; private set; }
        public int ChairWidth { get; private set; }
        public int ChairHeight { get; private set; }
        public Guid SalonId { get; }
    }
}