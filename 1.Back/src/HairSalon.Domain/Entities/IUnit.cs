﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class IUnit : Entity, IAggregateRoot
    {
        public IUnit(string code, string name, bool isActice, Guid salonId)
        {
            Id = Guid.NewGuid();
            Code = code;
            Name = name;
            IsActice = isActice;
            SalonId = salonId;
        }
        protected IUnit() { }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public bool IsActice { get; private set; }
        public Guid SalonId { get; private set; }
    }
}
