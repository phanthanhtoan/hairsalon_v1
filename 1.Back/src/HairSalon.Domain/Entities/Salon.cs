﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class Salon: Entity, IAggregateRoot
    {
        public Salon(string code, string name, string address, string phone,
            string email, string facebook, string website)
        {
            Id = Guid.NewGuid();
            Code = code;
            Name = name;
            Address = address;
            Phone = phone;
            Email = email;
            Facebook = facebook;
            Website = website;
        }

        // Empty constructor for EF
        protected Salon() { }
        public string Code { get; private set; }
        public string Name { get; private set; }
        public string Address { get; private set; }
        public string Phone { get; private set; }
        public string Email { get; private set; }
        public string Facebook { get; private set; }
        public string Website { get; private set; }
    }
}
