﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class StaffGroup : Entity, IAggregateRoot
    {
        public StaffGroup(string code, string name, bool isActive, Guid salonId)
        {
            Id = Guid.NewGuid();
            Code = code;
            Name = name;
            IsActive = isActive;
            SalonId = salonId;
        }
        protected StaffGroup() { }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public bool IsActive { get; private set; }
        public Guid SalonId { get; private set; }
    }
}
