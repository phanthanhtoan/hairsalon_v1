﻿using System;
using NetDevPack.Domain;

namespace HairSalon.Domain.Entities
{
    public class Customer : Entity, IAggregateRoot
    {
        public Customer(Guid id, string name, string phone, string email, DateTime birthDate, Guid salonId)
        {
            Id = id;
            Name = name;
            Phone = phone;
            Email = email;
            BirthDate = birthDate;
            SalonId = salonId;
        }
        // Empty constructor for EF
        protected Customer() { }

        public string Name { get; private set; }
        public string Phone { get; private set; }
        public string Email { get; private set; }
        public DateTime BirthDate { get; private set; }
        public Guid SalonId { get; private set; }
    }
}