﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class TransactionCode : Entity, IAggregateRoot
    {
        public TransactionCode(string code, string name, int type, bool isActive, Guid salonId)
        {
            Id = Guid.NewGuid();
            Code = code;
            Name = name;
            Type = type;
            IsActive = isActive;
            SalonId = salonId;
        }
        protected TransactionCode() { }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public int Type { get; private set; }
        public bool IsActive { get; private set; }
        public Guid SalonId { get; private set; }
    }
}
