﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class BillHairDresser : Entity, IAggregateRoot
    {
        public BillHairDresser(Guid billId, Guid staffId, string note)
        {
            Id = Guid.NewGuid();
            BillId = billId;
            StaffId = staffId;
            Note = note;
        }
        protected BillHairDresser() { }

        public Guid BillId { get; private set; }
        public Guid StaffId { get; private set; }
        public string Note { get; private set; }
    }
}
