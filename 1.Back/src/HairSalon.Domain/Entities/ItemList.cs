﻿using NetDevPack.Domain;
using System;

namespace HairSalon.Domain.Entities
{
    public class ItemList : Entity, IAggregateRoot
    {
        public ItemList(string code, string name, decimal price, bool openPrice,
            Guid unitId, Guid itemGroupId, bool isActive, Guid salonId)
        {
            Id = Guid.NewGuid();
            Code = code;
            Name = name;
            Price = price;
            OpenPrice = openPrice;
            UnitId = unitId;
            ItemGroupId = itemGroupId;
            IsActive = isActive;
            SalonId = salonId;
        }
        protected ItemList() { }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public decimal Price { get; private set; }
        public bool OpenPrice { get; private set; }
        public Guid UnitId { get; private set; }
        public Guid ItemGroupId { get; private set; }
        public bool IsActive { get; private set; }
        public Guid SalonId { get; private set; }
    }
}
