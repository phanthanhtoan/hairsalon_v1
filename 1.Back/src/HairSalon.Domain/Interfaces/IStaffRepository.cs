﻿using HairSalon.Domain.Entities;
using NetDevPack.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HairSalon.Domain.Interfaces
{
    public interface IStaffRepository : IRepository<Staff>
    {
        Task<IEnumerable<Staff>> GetAll();
    }
}
