﻿using HairSalon.Domain.Entities;
using NetDevPack.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
public interface IBillDetailRepository : IRepository<BillDetail>
{
    Task<IEnumerable<BillDetail>> GetByBillID(Guid BillID);
    void AddRange(List<BillDetail> billDetails);
    void RemoveRange(List<BillDetail> billDetails);
}