﻿using HairSalon.Domain.Entities;
using NetDevPack.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
public interface IBillRepository : IRepository<Bill>
{
    Task<Bill> GetByID(Guid BillID);
    void Add(Bill bill);
    void Update(Bill bill);
    void Remove(Bill bill);
}
