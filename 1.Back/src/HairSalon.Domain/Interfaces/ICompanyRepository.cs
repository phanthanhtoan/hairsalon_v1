﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HairSalon.Domain.Entities;
using NetDevPack.Data;

namespace HairSalon.Domain.Interfaces
{
    public interface ICompanyRepository : IRepository<Company>
    {
        Task<Company> GetById(long id);

        Task<Company> GetByTaxNumber(string taxNumber);

        Task<IEnumerable<Company>> GetAll();

        void Add(Company company);
        void Update(Company company);
        void Remove(Company company);
    }
}