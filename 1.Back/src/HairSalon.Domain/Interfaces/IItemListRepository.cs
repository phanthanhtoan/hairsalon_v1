﻿using HairSalon.Domain.Entities;
using NetDevPack.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HairSalon.Domain.Interfaces
{
    public interface IItemListRepository : IRepository<ItemList>
    {
        Task<IEnumerable<ItemList>> GetAll();

        void Add(Customer customer);
        void Update(Customer customer);
        void Remove(Customer customer);
    }
}
