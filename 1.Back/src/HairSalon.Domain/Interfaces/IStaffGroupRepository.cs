﻿using HairSalon.Domain.Entities;
using NetDevPack.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HairSalon.Domain.Interfaces
{
    public interface IStaffGroupRepository : IRepository<StaffGroup>
    {
        Task<IEnumerable<StaffGroup>> GetAll();
    }
}
