﻿using System;
using NetDevPack.Messaging;

namespace HairSalon.Domain.Commands
{
    public abstract class CompanyCommand : Command
    {
        public long Id { get; protected set; }

        public string Name { get; protected set; }

        public string Email { get; protected set; }

        public string TaxNumber { get; protected set; }
    }
}