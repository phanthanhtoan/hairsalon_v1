﻿using NetDevPack.Messaging;
using System;
namespace HairSalon.Domain.Commands
{
    public abstract class BillDetailCommand : Command
    {
        public Guid Id { get; protected set; }
        public Guid BillId { get; protected set; }
        public Guid ItemId { get; protected set; }
        public decimal Price { get; protected set; }
        public decimal Quantity { get; protected set; }
        public string DetailNote { get; protected set; }
    }
}
