﻿using System;
using NetDevPack.Messaging;

namespace HairSalon.Domain.Commands
{
    public abstract class CustomerCommand : Command
    {
        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
        public string Phone { get; protected set; }
        public string Email { get; protected set; }
        public DateTime BirthDate { get; protected set; }
        public Guid SalonId { get; protected set; }
    }
}