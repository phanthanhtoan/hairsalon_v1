﻿using System;
using HairSalon.Domain.Commands.Validations;

namespace HairSalon.Domain.Commands
{
    public class RegisterNewBillCommand : BillComand
    {
        public RegisterNewBillCommand(string billCode, DateTime arrivaldate, DateTime arrivalTime, DateTime departureTime, decimal dscPcnt,
                    decimal dscAmount, string note, DateTime createdTime, string createdUser, DateTime checkinTime,
                    string checkinUser, DateTime checkoutTime, string checkoutUser, int status, Guid chairId, string guestName,
                    string guestPhone, Guid customerId, Guid salonId, Guid staffMain, Guid staffSub, Guid staffShampoo)
        {
            BillCode = billCode;
            Arrivaldate = arrivaldate;
            ArrivalTime = arrivalTime;
            DepartureTime = departureTime;
            DscPcnt = dscPcnt;
            DscAmount = dscAmount;
            Note = note;
            CreatedTime = createdTime;
            CreatedUser = createdUser;
            CheckinTime = checkinTime;
            CheckinUser = checkinUser;
            CheckoutTime = checkoutTime;
            CheckoutUser = checkoutUser;
            Status = status;
            ChairId = chairId;
            GuestName = guestName;
            GuestPhone = guestPhone;
            CustomerId = customerId;
            SalonId = salonId;
            StaffMain = staffMain;
            StaffSub = staffSub;
            StaffShampoo = staffShampoo;
        }

        public override bool IsValid()
        {
            ValidationResult = new RegisterNewBillCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
