﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HairSalon.Domain.Events;
using HairSalon.Domain.Interfaces;
using HairSalon.Domain.Entities;
using FluentValidation.Results;
using MediatR;
using NetDevPack.Messaging;

namespace HairSalon.Domain.Commands
{
    public class CompanyCommandHandler : CommandHandler,
        IRequestHandler<CreateCompanyCommand, ValidationResult>
    {
        private readonly ICompanyRepository _companyRepository;

        public CompanyCommandHandler(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public async Task<ValidationResult> Handle(CreateCompanyCommand message, CancellationToken cancellationToken)
        {
            if (!message.IsValid()) return message.ValidationResult;

            var company = new Company(message.Name, message.Email, message.TaxNumber);

            if (await _companyRepository.GetByTaxNumber(company.TaxNumber) != null)
            {
                AddError("The customer e-mail has already been taken.");
                return ValidationResult;
            }

            company.AddDomainEvent(new CompanyCreatedEvent(company.Id, company.Name, company.Email, company.TaxNumber));

            _companyRepository.Add(company);

            return await Commit(_companyRepository.UnitOfWork);
        }

        public void Dispose()
        {
            _companyRepository.Dispose();
        }
    }
}