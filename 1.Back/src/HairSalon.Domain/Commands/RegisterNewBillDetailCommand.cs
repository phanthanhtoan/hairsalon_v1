﻿using HairSalon.Domain.Commands.Validations;
using System;

namespace HairSalon.Domain.Commands
{
    public class RegisterNewBillDetailCommand : BillDetailCommand
    {
        public RegisterNewBillDetailCommand(Guid billId, Guid itemId, decimal price, decimal quantity, string detailNote)
        {
            Id = Guid.NewGuid();
            BillId = billId;
            ItemId = itemId;
            Price = price;
            Quantity = quantity;
            DetailNote = detailNote;
        }
        public override bool IsValid()
        {
            ValidationResult = new RegisterNewBillDetailCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
