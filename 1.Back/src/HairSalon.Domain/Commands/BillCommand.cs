﻿using NetDevPack.Messaging;
using System;

namespace HairSalon.Domain.Commands
{
    public abstract class BillComand : Command
    {
        public Guid Id { get; protected set; }
        public string BillCode { get; protected set; }
        public DateTime Arrivaldate { get; protected set; }
        public DateTime ArrivalTime { get; protected set; }
        public DateTime DepartureTime { get; protected set; }
        public decimal DscPcnt { get; protected set; }
        public decimal DscAmount { get; protected set; }
        public string Note { get; protected set; }
        public DateTime CreatedTime { get; protected set; }
        public string CreatedUser { get; protected set; }
        public DateTime CheckinTime { get; protected set; }
        public string CheckinUser { get; protected set; }
        public DateTime CheckoutTime { get; protected set; }
        public string CheckoutUser { get; protected set; }
        public int Status { get; protected set; }
        public Guid ChairId { get; protected set; }
        public string GuestName { get; protected set; }
        public string GuestPhone { get; protected set; }
        public Guid CustomerId { get; protected set; }
        public Guid SalonId { get; protected set; }
        public Guid StaffMain { get; protected set; }
        public Guid StaffSub { get; protected set; }
        public Guid StaffShampoo { get; protected set; }
    }
}
