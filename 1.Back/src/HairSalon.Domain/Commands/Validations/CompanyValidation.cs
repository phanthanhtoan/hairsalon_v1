﻿using System;
using FluentValidation;

namespace HairSalon.Domain.Commands.Validations
{
    public abstract class CompanyValidation<T> : AbstractValidator<T> where T : CompanyCommand
    {
        protected void ValidateName()
        {
            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Please ensure you have entered the Name")
                .Length(2, 150).WithMessage("The Name must have between 2 and 150 characters");
        }

        protected void ValidateTaxNumber()
        {
            RuleFor(c => c.TaxNumber)
                .NotEmpty()
                .Length(10, 15)
                .WithMessage("The Tax Number must have between 10 and 15 characters");
        }

        protected void ValidateEmail()
        {
            RuleFor(c => c.Email)
                .NotEmpty()
                .EmailAddress();
        }

        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(0);
        }

    }
}