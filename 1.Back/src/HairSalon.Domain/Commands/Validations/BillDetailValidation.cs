﻿using System;
using FluentValidation;

namespace HairSalon.Domain.Commands.Validations
{
    public abstract class BillDetailValidation<T> : AbstractValidator<T> where T : BillDetailCommand
    {
        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }
    }
}
