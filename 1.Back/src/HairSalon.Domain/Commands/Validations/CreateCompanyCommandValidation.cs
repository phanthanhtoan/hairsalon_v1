﻿namespace HairSalon.Domain.Commands.Validations
{
    public class CreateCompanyCommandValidation : CompanyValidation<CreateCompanyCommand>
    {
        public CreateCompanyCommandValidation()
        {
            ValidateName();
            ValidateTaxNumber();
            ValidateEmail();
        }
    }
}