﻿using System;
using FluentValidation;

namespace HairSalon.Domain.Commands.Validations
{
    public abstract class BillValidation<T> : AbstractValidator<T> where T : BillComand
    {
        protected void ValidateGuestName()
        {
            RuleFor(c => c.GuestName)
                .NotEmpty().WithMessage("Vui lòng nhập tên khách")
                .Length(2, 100).WithMessage("Tên khách có ít từ 2 đến 100 ký tự");
        }
        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(Guid.Empty);
        }
    }
}
