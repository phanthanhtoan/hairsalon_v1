﻿
namespace HairSalon.Domain.Commands.Validations
{
    public class RegisterNewBillCommandValidation : BillValidation<RegisterNewBillCommand>
    {
        public RegisterNewBillCommandValidation()
        {
            ValidateGuestName();
        }
    }
}