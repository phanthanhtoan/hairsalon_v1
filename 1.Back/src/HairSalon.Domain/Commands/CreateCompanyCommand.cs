﻿using System;
using HairSalon.Domain.Commands.Validations;

namespace HairSalon.Domain.Commands
{
    public class CreateCompanyCommand : CompanyCommand
    {
        public CreateCompanyCommand(string name, string email, string taxNumber)
        {
            Name = name;
            Email = email;
            TaxNumber = taxNumber;
        }

        public override bool IsValid()
        {
            ValidationResult = new CreateCompanyCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}