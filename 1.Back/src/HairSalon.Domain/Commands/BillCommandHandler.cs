﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HairSalon.Domain.Events;
using HairSalon.Domain.Interfaces;
using HairSalon.Domain.Entities;
using FluentValidation.Results;
using MediatR;
using NetDevPack.Messaging;

namespace HairSalon.Domain.Commands
{
    public class BillCommandHandler: CommandHandler,
        IRequestHandler<RegisterNewBillCommand, ValidationResult>
    {
        private readonly IBillRepository _billRepository;

        public BillCommandHandler(IBillRepository billRepository)
        {
            _billRepository = billRepository;
        }

        public async Task<ValidationResult> Handle(RegisterNewBillCommand message, CancellationToken cancellationToken)
        {
            if (!message.IsValid()) return message.ValidationResult;

            var bill = new Bill(message.BillCode, message.Arrivaldate, message.ArrivalTime,
                message.DepartureTime, message.DscPcnt, message.DscAmount, message.Note, message.CreatedTime, message.CreatedUser,
                message.CheckinTime, message.CheckinUser, message.CheckoutTime, message.CheckinUser, message.Status, message.ChairId,
                message.GuestName, message.GuestPhone, message.CustomerId, message.SalonId, message.StaffMain, message.StaffSub, message.StaffShampoo);

            //if (await _customerRepository.GetByEmail(customer.Email) != null)
            //{
            //    AddError("The customer e-mail has already been taken.");
            //    return ValidationResult;
            //}

            bill.AddDomainEvent(new BillRegisterdEvent(bill.Id, bill.BillCode, bill.Arrivaldate, bill.ArrivalTime,
                bill.DepartureTime, bill.DscPcnt, bill.DscAmount, bill.Note, bill.CreatedTime, bill.CreatedUser,
                bill.CheckinTime, bill.CheckinUser, bill.CheckoutTime, bill.CheckinUser, bill.Status, bill.ChairId,
                bill.GuestName, bill.GuestPhone, bill.CustomerId, bill.SalonId, bill.StaffMain, bill.StaffSub, bill.StaffShampoo));

            _billRepository.Add(bill);

            return await Commit(_billRepository.UnitOfWork);
        }

        public void Dispose()
        {
            _billRepository.Dispose();
        }
    }
}
