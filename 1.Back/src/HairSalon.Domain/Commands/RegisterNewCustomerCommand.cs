﻿using System;
using HairSalon.Domain.Commands.Validations;

namespace HairSalon.Domain.Commands
{
    public class RegisterNewCustomerCommand : CustomerCommand
    {
        public RegisterNewCustomerCommand(string name, string phone, string email, DateTime birthDate, Guid salonId)
        {
            Name = name;
            Phone = phone;
            Email = email;
            BirthDate = birthDate;
            SalonId = salonId;
        }

        public override bool IsValid()
        {
            ValidationResult = new RegisterNewCustomerCommandValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}