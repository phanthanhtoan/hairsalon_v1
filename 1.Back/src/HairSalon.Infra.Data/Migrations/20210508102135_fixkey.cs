﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HairSalon.Infra.Data.Migrations
{
    public partial class fixkey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActice",
                table: "ItemList");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "ItemList",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "ItemList");

            migrationBuilder.AddColumn<bool>(
                name: "IsActice",
                table: "ItemList",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
