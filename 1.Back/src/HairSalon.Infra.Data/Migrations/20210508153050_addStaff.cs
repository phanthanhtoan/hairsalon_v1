﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HairSalon.Infra.Data.Migrations
{
    public partial class addStaff : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "StaffMain",
                table: "Bill",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "StaffShampoo",
                table: "Bill",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "StaffSub",
                table: "Bill",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StaffMain",
                table: "Bill");

            migrationBuilder.DropColumn(
                name: "StaffShampoo",
                table: "Bill");

            migrationBuilder.DropColumn(
                name: "StaffSub",
                table: "Bill");
        }
    }
}
