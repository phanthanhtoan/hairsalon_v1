﻿using HairSalon.Domain.Entities;
using HairSalon.Domain.Interfaces;
using HairSalon.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using NetDevPack.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HairSalon.Infra.Data.Repository
{
    public class StaffRepository : IStaffRepository
    {
        protected readonly HairSalonContext Db;
        protected readonly DbSet<Staff> DbSet;

        public StaffRepository(HairSalonContext context)
        {
            Db = context;
            DbSet = Db.Set<Staff>();
        }

        public IUnitOfWork UnitOfWork => Db;

        public async Task<IEnumerable<Staff>> GetAll()
        {
            return await DbSet.Where(e => e.IsActive).ToListAsync();
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
