﻿using HairSalon.Domain.Entities;
using HairSalon.Domain.Interfaces;
using HairSalon.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using NetDevPack.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HairSalon.Infra.Data.Repository
{
    public class ItemListRepository : IItemListRepository
    {
        protected readonly HairSalonContext Db;
        protected readonly DbSet<ItemList> DbSet;

        public ItemListRepository(HairSalonContext context)
        {
            Db = context;
            DbSet = Db.Set<ItemList>();
        }

        public IUnitOfWork UnitOfWork => Db;

        public async Task<IEnumerable<ItemList>> GetAll()
        {
            return await DbSet.Where(e => e.IsActive).OrderBy(e => e.Code).ToListAsync();
        }

        public void Add(Customer customer)
        {
            throw new NotImplementedException();
        }

        public void Remove(Customer customer)
        {
            throw new NotImplementedException();
        }

        public void Update(Customer customer)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
