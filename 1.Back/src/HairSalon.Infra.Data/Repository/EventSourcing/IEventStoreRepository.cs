﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HairSalon.Domain.Core.Events;

namespace HairSalon.Infra.Data.Repository.EventSourcing
{
    public interface IEventStoreRepository : IDisposable
    {
        void Store(StoredEvent theEvent);
        Task<IList<StoredEvent>> All(Guid aggregateId);
    }
}