﻿using HairSalon.Domain.Entities;
using HairSalon.Domain.Interfaces;
using HairSalon.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using NetDevPack.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HairSalon.Infra.Data.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        protected readonly HairSalonContext Db;
        protected readonly DbSet<Company> DbSet;
        private bool disposedValue;

        public CompanyRepository(HairSalonContext context)
        {
            Db = context;
            DbSet = Db.Set<Company>();
        }

        public IUnitOfWork UnitOfWork => Db;

        public void Add(Company company)
        {
            DbSet.Add(company);
        }

        public async Task<IEnumerable<Company>> GetAll()
        {
            return await DbSet.ToListAsync();
        }

        public async Task<Company> GetById(long id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task<Company> GetByTaxNumber(string taxNumber)
        {
            if (string.IsNullOrEmpty(taxNumber))
            {
                return null;
            }
            return await DbSet.AsNoTracking().FirstOrDefaultAsync(c => c.TaxNumber == taxNumber);
        }

        public void Remove(Company company)
        {
            DbSet.Remove(company);
        }

        public void Update(Company company)
        {
            DbSet.Update(company);
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
