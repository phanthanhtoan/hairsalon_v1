﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HairSalon.Domain.Interfaces;
using HairSalon.Domain.Entities;
using HairSalon.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using NetDevPack.Data;
using System.Linq;

namespace HairSalon.Infra.Data.Repository
{
    public class BIllDetailRepository : IBillDetailRepository
    {
        protected readonly HairSalonContext Db;
        protected readonly DbSet<BillDetail> DbSet;

        public BIllDetailRepository(HairSalonContext context)
        {
            Db = context;
            DbSet = Db.Set<BillDetail>();
        }
        public IUnitOfWork UnitOfWork => Db;

        public async Task<IEnumerable<BillDetail>> GetByBillID(Guid BillID)
        {
            return await DbSet.Where(e => e.BillId == BillID).ToListAsync();
        }

        public void AddRange(List<BillDetail> billDetails)
        {
            DbSet.AddRange(billDetails);
        }

        public void RemoveRange(List<BillDetail> billDetails)
        {
            DbSet.RemoveRange(billDetails);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
