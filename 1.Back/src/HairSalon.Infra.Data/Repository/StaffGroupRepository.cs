﻿using HairSalon.Domain.Entities;
using HairSalon.Domain.Interfaces;
using HairSalon.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using NetDevPack.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HairSalon.Infra.Data.Repository
{
    public class StaffGroupRepository : IStaffGroupRepository
    {
        protected readonly HairSalonContext Db;
        protected readonly DbSet<StaffGroup> DbSet;

        public StaffGroupRepository(HairSalonContext context)
        {
            Db = context;
            DbSet = Db.Set<StaffGroup>();
        }

        public IUnitOfWork UnitOfWork => Db;

        public async Task<IEnumerable<StaffGroup>> GetAll()
        {
            return await DbSet.Where(e => e.IsActive).OrderBy(e => e.Code).ToListAsync();
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
