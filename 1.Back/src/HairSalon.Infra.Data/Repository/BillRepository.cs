﻿using HairSalon.Domain.Entities;
using HairSalon.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using NetDevPack.Data;
using System;
using System.Threading.Tasks;

namespace HairSalon.Infra.Data.Repository
{
    public class BillRepository : IBillRepository
    {
        protected readonly HairSalonContext Db;
        protected readonly DbSet<Bill> DbSet;

        public BillRepository(HairSalonContext context)
        {
            Db = context;
            DbSet = Db.Set<Bill>();
        }

        public IUnitOfWork UnitOfWork => Db;

        public async Task<Bill> GetByID(Guid BillID)
        {
            return await DbSet.FindAsync(BillID);
        }

        public void Add(Bill bill)
        {
            DbSet.Add(bill);
        }

        public void Update(Bill bill)
        {
            DbSet.Update(bill);
        }
        public void Remove(Bill bill)
        {
            DbSet.Remove(bill);
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
