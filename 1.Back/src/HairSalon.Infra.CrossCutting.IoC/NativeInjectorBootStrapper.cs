﻿using HairSalon.Application.Interfaces;
using HairSalon.Application.Services;
using HairSalon.Domain.Commands;
using HairSalon.Domain.Core.Events;
using HairSalon.Domain.Events;
using HairSalon.Domain.Interfaces;
using HairSalon.Infra.CrossCutting.Bus;
using HairSalon.Infra.Data.Context;
using HairSalon.Infra.Data.EventSourcing;
using HairSalon.Infra.Data.Repository;
using HairSalon.Infra.Data.Repository.EventSourcing;
using FluentValidation.Results;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using NetDevPack.Mediator;

namespace HairSalon.Infra.CrossCutting.IoC
{
    public static class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // Domain Bus (Mediator)
            services.AddScoped<IMediatorHandler, InMemoryBus>();

            // Application
            services.AddScoped<ICustomerAppService, CustomerAppService>();
            services.AddScoped<ICompanyAppService, CompanyAppService>();
            services.AddScoped<IItemListAppService, ItemListAppService>();
            services.AddScoped<IStaffGroupAppService, StaffGroupAppService>();
            services.AddScoped<IStaffAppService, StaffAppService>();
            services.AddScoped<IBillAppService, BillAppService>();
            services.AddScoped<IBillDetailAppService, BillDetailAppService>();

            // Domain - Events
            services.AddScoped<INotificationHandler<CustomerRegisteredEvent>, CustomerEventHandler>();
            services.AddScoped<INotificationHandler<CustomerUpdatedEvent>, CustomerEventHandler>();
            services.AddScoped<INotificationHandler<CustomerRemovedEvent>, CustomerEventHandler>();

            services.AddScoped<INotificationHandler<CompanyCreatedEvent>, CompanyEventHandler>();

            // Domain - Commands
            services.AddScoped<IRequestHandler<RegisterNewCustomerCommand, ValidationResult>, CustomerCommandHandler>();
            services.AddScoped<IRequestHandler<UpdateCustomerCommand, ValidationResult>, CustomerCommandHandler>();
            services.AddScoped<IRequestHandler<RemoveCustomerCommand, ValidationResult>, CustomerCommandHandler>();

            services.AddScoped<IRequestHandler<CreateCompanyCommand, ValidationResult>, CompanyCommandHandler>();
            services.AddScoped<IRequestHandler<RegisterNewBillCommand, ValidationResult>, BillCommandHandler>();
            // services.AddScoped<IRequestHandler<RegisterNewBillDetailCommand, ValidationResult>, CustomerCommandHandler>();
            // Infra - Data
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<IItemListRepository, ItemListRepository>();
            services.AddScoped<IStaffGroupRepository, StaffGroupRepository>();
            services.AddScoped<IStaffRepository, StaffRepository>();
            services.AddScoped<IBillRepository, BillRepository>();
            services.AddScoped<IBillDetailRepository, BIllDetailRepository>();
            services.AddScoped<HairSalonContext>();

            // Infra - Data EventSourcing
            services.AddScoped<IEventStoreRepository, EventStoreSqlRepository>();
            services.AddScoped<IEventStore, SqlEventStore>();
            services.AddScoped<EventStoreSqlContext>();
        }
    }
}