﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using HairSalon.Application.Interfaces;
using HairSalon.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HairSalon.Services.API.Controllers
{
    [Route("api/itemlist")]
    [Authorize]
    public class ItemListController : ApiController
    {
        private readonly IItemListAppService _itemListAppService;

        public ItemListController(IItemListAppService itemListAppService)
        {
            _itemListAppService = itemListAppService;
        }

        [AllowAnonymous]
        [HttpGet("getall")]
        public async Task<IEnumerable<ItemListViewModel>> Get()
        {
            return await _itemListAppService.GetAll();
        }
    }
}
