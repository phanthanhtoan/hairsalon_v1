﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HairSalon.Application.EventSourcedNormalizers;
using HairSalon.Application.Interfaces;
using HairSalon.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetDevPack.Identity.Authorization;

namespace HairSalon.Services.API.Controllers
{
    [Route("api/bill-information")]
    [Authorize]
    public class BillInformationController : ApiController
    {
        private readonly IItemListAppService _itemListAppService;
        private readonly IStaffGroupAppService _staffGroupService;
        private readonly IStaffAppService _staffService;
        public BillInformationController(IItemListAppService itemListAppService,
                           IStaffGroupAppService staffGroupService,
                           IStaffAppService staffService)
        {
            _itemListAppService = itemListAppService;
            _staffGroupService = staffGroupService;
            _staffService = staffService;
        }

        [AllowAnonymous]
        [HttpGet("getall")]
        public async Task<BillInformationViewModel> Get()
        {
            BillInformationViewModel result = new BillInformationViewModel
            {
                ItemLists = await _itemListAppService.GetAll(),
                StaffGroups = await _staffGroupService.GetAll(),
                Staffs = await _staffService.GetAll(),
            };
            return result;
        }
    }
}
