using System;
using System.Threading.Tasks;
using HairSalon.Application.Interfaces;
using HairSalon.Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetDevPack.Identity.Authorization;

namespace HairSalon.UI.Web.Controllers
{
    [Authorize]
    public class CompanyController : BaseController
    {
        private readonly ICompanyAppService _companyAppService;

        public CompanyController(ICompanyAppService companyAppService)
        {
            _companyAppService = companyAppService;
        }
        [HttpGet("company-management/list-all")]
        public async Task<IActionResult> Index()
        {
            return View(await _companyAppService.GetAll());
        }

        [HttpGet("company-management/company-details/{id:long}")]
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null) return NotFound();

            var customerViewModel = await _companyAppService.GetById(id.Value);

            if (customerViewModel == null) return NotFound();

            return View(customerViewModel);
        }

        //[CustomAuthorize("Companies", "Write")]
        [HttpGet("company-management/create")]
        public IActionResult Create()
        {
            return View();
        }

        //[CustomAuthorize("Customers", "Write")]
        [HttpPost("company-management/create")]
        public async Task<IActionResult> Create(CompanyViewModel companyViewModel)
        {
            if (!ModelState.IsValid) return View(companyViewModel);

            if (ResponseHasErrors(await _companyAppService.Create(companyViewModel)))
                return View(companyViewModel);

            ViewBag.Success = "Company Created!";

            return View(companyViewModel);
        }

    }
}
