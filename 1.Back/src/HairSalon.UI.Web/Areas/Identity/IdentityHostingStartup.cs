﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(HairSalon.UI.Web.Areas.Identity.IdentityHostingStartup))]
namespace HairSalon.UI.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {});
        }
    }
}