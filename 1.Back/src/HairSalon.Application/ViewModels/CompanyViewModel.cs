﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HairSalon.Application.ViewModels
{
    public class CompanyViewModel
    {
        [Key]
        public long Id { get; set; }

        [Required(ErrorMessage = "The Name is Required")]
        [MinLength(2)]
        [MaxLength(100)]
        [DisplayName("Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The E-mail is Required")]
        [EmailAddress]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The Tax Number is Required")]
        [MinLength(10)]
        [MaxLength(15)]
        [DisplayName("Tax Number")]
        public string TaxNumber { get; set; }
    }
}
