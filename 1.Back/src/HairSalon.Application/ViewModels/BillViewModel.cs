﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HairSalon.Application.ViewModels
{
    public class BillViewModel
    {
        public Guid Id { get; set; }
        public string BillCode { get; set; }
        public DateTime Arrivaldate { get; set; }
        public DateTime ArrivalTime { get; set; }
        public DateTime DepartureTime { get; set; }
        public decimal DscPcnt { get; set; }
        public decimal DscAmount { get; set; }
        public string Note { get; set; }
        public DateTime CreatedTime { get; set; }
        public string CreatedUser { get; set; }
        public DateTime CheckinTime { get; set; }
        public string CheckinUser { get; set; }
        public DateTime CheckoutTime { get; set; }
        public string CheckoutUser { get; set; }
        public int Status { get; set; }
        public Guid ChairId { get; set; }
        public string GuestName { get; set; }
        public string GuestPhone { get; set; }
        public Guid CustomerId { get; set; }
        public Guid SalonId { get; set; }
        public Guid StaffMain { get; set; }
        public Guid StaffSub { get; set; }
        public Guid StaffShampoo { get; set; }
    }
}
