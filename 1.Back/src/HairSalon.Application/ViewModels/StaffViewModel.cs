﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HairSalon.Application.ViewModels
{
    public class StaffViewModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Facebook { get; set; }
        public string Website { get; set; }
        public Guid StaffGroupId { get; set; }
        public decimal Basicsalary { get; set; }
        public decimal Commission { get; set; }
        public DateTime BeginWork { get; set; }
        public bool IsActive { get; set; }
        public Guid SalonId { get; set; }
    }
}
