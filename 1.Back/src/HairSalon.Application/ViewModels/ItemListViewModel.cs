﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace HairSalon.Application.ViewModels
{
    public class ItemListViewModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool OpenPrice { get; set; }
        public Guid UnitId { get; set; }
        public Guid ItemGroupId { get; set; }
        public bool IsActice { get; set; }
        public Guid SalonId { get; set; }
    }
}
