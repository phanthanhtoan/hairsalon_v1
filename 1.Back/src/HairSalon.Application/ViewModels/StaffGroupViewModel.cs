﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HairSalon.Application.ViewModels
{
    public  class StaffGroupViewModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public Guid SalonId { get; set; }
    }
}
