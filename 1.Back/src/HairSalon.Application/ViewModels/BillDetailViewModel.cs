﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HairSalon.Application.ViewModels
{
    public class BillDetailViewModel
    {
        public Guid Id { get; set; }
        public Guid BillId { get; set; }
        public Guid ItemId { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public string DetailNote { get; set; }
    }
}
