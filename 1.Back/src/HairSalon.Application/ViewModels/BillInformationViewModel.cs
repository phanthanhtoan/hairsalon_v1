﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HairSalon.Application.ViewModels
{
    public class BillInformationViewModel
    {
        public IEnumerable<ItemListViewModel> ItemLists { get; set; }
        public IEnumerable<StaffGroupViewModel> StaffGroups { get; set; }
        public IEnumerable<StaffViewModel> Staffs { get; set; }
    }
}
