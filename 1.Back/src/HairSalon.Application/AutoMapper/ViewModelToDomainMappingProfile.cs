﻿using AutoMapper;
using HairSalon.Application.ViewModels;
using HairSalon.Domain.Commands;

namespace HairSalon.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<CustomerViewModel, RegisterNewCustomerCommand>()
                .ConstructUsing(c => new RegisterNewCustomerCommand(c.Name, c.Phone, c.Email, c.BirthDate, c.SalonId));
            CreateMap<CustomerViewModel, UpdateCustomerCommand>()
                .ConstructUsing(c => new UpdateCustomerCommand(c.Id, c.Name, c.Email, c.BirthDate));
            CreateMap<CompanyViewModel, CreateCompanyCommand>()
                .ConstructUsing(c => new CreateCompanyCommand(c.Name, c.Email, c.TaxNumber));
        }
    }
}
