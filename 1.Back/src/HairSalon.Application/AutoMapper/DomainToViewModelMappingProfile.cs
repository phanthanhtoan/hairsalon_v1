﻿using AutoMapper;
using HairSalon.Application.ViewModels;
using HairSalon.Domain.Entities;

namespace HairSalon.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Customer, CustomerViewModel>();
            CreateMap<Company, CompanyViewModel>();
            CreateMap<ItemList, ItemListViewModel>();
            CreateMap<StaffGroup, StaffGroupViewModel>();
            CreateMap<Staff, StaffViewModel>();
            CreateMap<Bill, BillViewModel>();
            CreateMap<BillDetail, BillDetailViewModel>();
        }
    }
}
