﻿using FluentValidation.Results;
using HairSalon.Application.ViewModels;
using System;
using System.Threading.Tasks;

namespace HairSalon.Application.Interfaces
{
    public interface IBillAppService : IDisposable
    {
        Task<BillViewModel> GetByID(Guid BillID);
        Task<ValidationResult> Register(BillViewModel billViewModel);
        Task<ValidationResult> Update(BillViewModel billViewModel);
        Task<ValidationResult> Remove(Guid id);
    }
}
