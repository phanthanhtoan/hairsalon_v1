﻿using HairSalon.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HairSalon.Application.Interfaces
{
    public interface IStaffAppService : IDisposable
    {
        Task<IEnumerable<StaffViewModel>> GetAll();
    }
}
