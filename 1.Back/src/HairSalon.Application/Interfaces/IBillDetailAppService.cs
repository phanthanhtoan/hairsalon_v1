﻿using FluentValidation.Results;
using HairSalon.Application.ViewModels;
using System;
using System.Threading.Tasks;

namespace HairSalon.Application.Interfaces
{
    public interface IBillDetailAppService : IDisposable
    {
        Task<BillDetailViewModel> GetByID(Guid BillID);
        Task<ValidationResult> Register(BillDetailViewModel billViewModel);
        Task<ValidationResult> Update(BillDetailViewModel billViewModel);
        Task<ValidationResult> Remove(Guid id);
    }
}
