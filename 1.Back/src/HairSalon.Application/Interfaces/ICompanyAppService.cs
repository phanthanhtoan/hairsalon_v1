﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HairSalon.Application.EventSourcedNormalizers;
using HairSalon.Application.ViewModels;
using FluentValidation.Results;

namespace HairSalon.Application.Interfaces
{
    public interface ICompanyAppService : IDisposable
    {
        Task<IEnumerable<CompanyViewModel>> GetAll();
        Task<CompanyViewModel> GetById(long id);

        Task<ValidationResult> Create(CompanyViewModel companyViewModel);
        //Task<ValidationResult> Update(CompanyViewModel companyViewModel);
        //Task<ValidationResult> Remove(long id);
    }
}
