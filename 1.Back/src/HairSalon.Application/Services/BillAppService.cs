﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using HairSalon.Application.Interfaces;
using HairSalon.Application.ViewModels;
using HairSalon.Domain.Commands;
using HairSalon.Infra.Data.Repository.EventSourcing;
using FluentValidation.Results;
using NetDevPack.Mediator;

namespace HairSalon.Application.Services
{
    public class BillAppService : IBillAppService
    {
        private readonly IMapper _mapper;
        private readonly IBillRepository _billRepository;
        private readonly IEventStoreRepository _eventStoreRepository;
        private readonly IMediatorHandler _mediator;

        public BillAppService(IMapper mapper,
                              IBillRepository billRepository,
                              IEventStoreRepository eventStoreRepository,
                              IMediatorHandler mediator)
        {
            _mapper = mapper;
            _billRepository = billRepository;
            _eventStoreRepository = eventStoreRepository;
            _mediator = mediator;
        }
        public async Task<BillViewModel> GetByID(Guid BillID)
        {
            return _mapper.Map<BillViewModel>(await _billRepository.GetByID(BillID));
        }

        public async Task<ValidationResult> Register(BillViewModel billViewModel)
        {
            var registerCommand = _mapper.Map<RegisterNewBillCommand>(billViewModel);
            return await _mediator.SendCommand(registerCommand);
        }

        public Task<ValidationResult> Remove(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<ValidationResult> Update(BillViewModel billViewModel)
        {
            throw new NotImplementedException();
        }
        
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
