﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using HairSalon.Application.EventSourcedNormalizers;
using HairSalon.Application.Interfaces;
using HairSalon.Application.ViewModels;
using HairSalon.Domain.Commands;
using HairSalon.Domain.Interfaces;
using HairSalon.Infra.Data.Repository.EventSourcing;
using FluentValidation.Results;
using NetDevPack.Mediator;

namespace HairSalon.Application.Services
{
    public class CompanyAppService : ICompanyAppService
    {
        private readonly IMapper _mapper;
        private readonly ICompanyRepository _companyRepository;
        private readonly IEventStoreRepository _eventStoreRepository;
        private readonly IMediatorHandler _mediator;

        public CompanyAppService(IMapper mapper,
                                  ICompanyRepository companyRepository,
                                  IMediatorHandler mediator,
                                  IEventStoreRepository eventStoreRepository)
        {
            _mapper = mapper;
            _companyRepository = companyRepository;
            _mediator = mediator;
            _eventStoreRepository = eventStoreRepository;
        }

        public async Task<IEnumerable<CompanyViewModel>> GetAll()
        {
            return _mapper.Map<IEnumerable<CompanyViewModel>>(await _companyRepository.GetAll());
        }

        public async Task<CompanyViewModel> GetById(long id)
        {
            return _mapper.Map<CompanyViewModel>(await _companyRepository.GetById(id));
        }

        public async Task<ValidationResult> Create(CompanyViewModel companyViewModel)
        {
            var command = _mapper.Map<CreateCompanyCommand>(companyViewModel);
            return await _mediator.SendCommand(command);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
