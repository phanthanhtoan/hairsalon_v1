﻿using AutoMapper;
using HairSalon.Application.Interfaces;
using HairSalon.Application.ViewModels;
using HairSalon.Domain.Interfaces;
using HairSalon.Infra.Data.Repository.EventSourcing;
using NetDevPack.Mediator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HairSalon.Application.Services
{
    public class StaffGroupAppService : IStaffGroupAppService
    {
        private readonly IMapper _mapper;
        private readonly IStaffGroupRepository  _staffGroupRepository;
        private readonly IEventStoreRepository _eventStoreRepository;
        private readonly IMediatorHandler _mediator;

        public StaffGroupAppService(IMapper mapper,
                                  IStaffGroupRepository staffGroupRepository,
                                  IMediatorHandler mediator,
                                  IEventStoreRepository eventStoreRepository)
        {
            _mapper = mapper;
            _staffGroupRepository = staffGroupRepository;
            _mediator = mediator;
            _eventStoreRepository = eventStoreRepository;
        }

        public async Task<IEnumerable<StaffGroupViewModel>> GetAll()
        {
            return _mapper.Map<IEnumerable<StaffGroupViewModel>>(await _staffGroupRepository.GetAll());
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}