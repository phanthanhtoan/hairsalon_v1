﻿using AutoMapper;
using HairSalon.Application.Interfaces;
using HairSalon.Application.ViewModels;
using HairSalon.Domain.Interfaces;
using HairSalon.Infra.Data.Repository.EventSourcing;
using NetDevPack.Mediator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HairSalon.Application.Services
{
    public class ItemListAppService : IItemListAppService
    {
        private readonly IMapper _mapper;
        private readonly IItemListRepository _itemListRepository;
        private readonly IEventStoreRepository _eventStoreRepository;
        private readonly IMediatorHandler _mediator;

        public ItemListAppService(IMapper mapper,
                                  IItemListRepository itemListRepository,
                                  IMediatorHandler mediator,
                                  IEventStoreRepository eventStoreRepository)
        {
            _mapper = mapper;
            _itemListRepository = itemListRepository;
            _mediator = mediator;
            _eventStoreRepository = eventStoreRepository;
        }

        public async Task<IEnumerable<ItemListViewModel>> GetAll()
        {
            return _mapper.Map<IEnumerable<ItemListViewModel>>(await _itemListRepository.GetAll());
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
