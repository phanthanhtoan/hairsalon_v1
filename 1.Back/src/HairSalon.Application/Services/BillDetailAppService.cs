﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using HairSalon.Application.Interfaces;
using HairSalon.Application.ViewModels;
using HairSalon.Domain.Commands;
using HairSalon.Infra.Data.Repository.EventSourcing;
using FluentValidation.Results;
using NetDevPack.Mediator;


namespace HairSalon.Application.Services
{
    public class BillDetailAppService : IBillDetailAppService
    {
        private readonly IMapper _mapper;
        private readonly IBillDetailRepository _billDetailRepository;
        private readonly IEventStoreRepository _eventStoreRepository;
        private readonly IMediatorHandler _mediator;

        public BillDetailAppService(IMapper mapper,
                              IBillDetailRepository billDetailRepository,
                              IEventStoreRepository eventStoreRepository,
                              IMediatorHandler mediator)
        {
            _mapper = mapper;
            _billDetailRepository = billDetailRepository;
            _eventStoreRepository = eventStoreRepository;
            _mediator = mediator;
        }
        public async Task<BillDetailViewModel> GetByID(Guid BillID)
        {
            return _mapper.Map<BillDetailViewModel>(await _billDetailRepository.GetByBillID(BillID));
        }

        public async Task<ValidationResult> Register(BillDetailViewModel billDetailViewModel)
        {
            var registerCommand = _mapper.Map<RegisterNewBillCommand>(billDetailViewModel);
            return await _mediator.SendCommand(registerCommand);
        }

        public Task<ValidationResult> Remove(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<ValidationResult> Update(BillDetailViewModel billDetailViewModel)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
