﻿using AutoMapper;
using HairSalon.Application.Interfaces;
using HairSalon.Application.ViewModels;
using HairSalon.Domain.Interfaces;
using HairSalon.Infra.Data.Repository.EventSourcing;
using NetDevPack.Mediator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HairSalon.Application.Services
{
    public class StaffAppService : IStaffAppService
    {
        private readonly IMapper _mapper;
        private readonly IStaffRepository _staffRepository;
        private readonly IEventStoreRepository _eventStoreRepository;
        private readonly IMediatorHandler _mediator;

        public StaffAppService(IMapper mapper,
                            IStaffRepository staffRepository,
                            IMediatorHandler mediator,
                            IEventStoreRepository eventStoreRepository)
        {
            _mapper = mapper;
            _staffRepository = staffRepository;
            _mediator = mediator;
            _eventStoreRepository = eventStoreRepository;
        }

        public async Task<IEnumerable<StaffViewModel>> GetAll()
        {
            return _mapper.Map<IEnumerable<StaffViewModel>>(await _staffRepository.GetAll());
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
