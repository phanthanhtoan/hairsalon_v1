USE [HairSalon]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[BillId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BillCode] [varchar](20) NULL,
	[Arrivaldate] [datetime] NULL,
	[ArrivalTime] [datetime] NULL,
	[DepartureTime] [datetime] NULL,
	[DscPcnt] [numeric](5, 2) NULL,
	[DscAmount] [numeric](18, 0) NULL,
	[Note] [nvarchar](500) NULL,
	[CreatedTime] [datetime] NULL,
	[CreatedUser] [varchar](10) NULL,
	[CheckinTime] [datetime] NULL,
	[CheckinUser] [varchar](10) NULL,
	[CheckoutTime] [datetime] NULL,
	[CheckoutUser] [varchar](10) NULL,
	[Status] [int] NOT NULL,
	[ChairId] [numeric](18, 0) NULL,
	[GuestName] [nvarchar](100) NULL,
	[GuestPhone] [varchar](20) NULL,
	[CustomerId] [numeric](18, 0) NULL,
	[StaffMain] [numeric](18, 0) NULL,
	[StaffShampoo] [numeric](18, 0) NULL,
	[StaffSub] [numeric](18, 0) NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillDetail]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillDetail](
	[BillDetailId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BillId] [numeric](18, 0) NULL,
	[ItemId] [numeric](18, 0) NULL,
	[Price] [numeric](18, 0) NULL,
	[Quantity] [int] NULL,
	[DetailNote] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Chair]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chair](
	[ChairId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ChairCode] [varchar](20) NULL,
	[ChairName] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[ChairTop] [int] NOT NULL,
	[ChairLeft] [int] NOT NULL,
	[ChairWidth] [int] NOT NULL,
	[ChairHeight] [int] NOT NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[ChairId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IGroup]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IGroup](
	[IGroupId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IGroupCode] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActice] [bit] NOT NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[IGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemList]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemList](
	[ItemId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ItemCode] [varchar](20) NULL,
	[ItemName] [nvarchar](100) NULL,
	[Price] [numeric](18, 0) NULL,
	[OpenPrice] [bit] NULL,
	[UnitId] [numeric](18, 0) NULL,
	[ItemGroupId] [numeric](18, 0) NULL,
	[SalonId] [numeric](18, 0) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IUnit]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IUnit](
	[UnitId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UnitCode] [varchar](20) NULL,
	[UnitName] [nvarchar](100) NULL,
	[IsActice] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[LocationId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[LocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Salon]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salon](
	[SalonId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[Address] [nvarchar](100) NULL,
	[Phone] [varchar](20) NULL,
	[Email] [nvarchar](100) NULL,
	[Facebook] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[SalonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Source]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Source](
	[SourceId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[SourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[StaffId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[Address] [nvarchar](100) NULL,
	[Phone] [varchar](20) NULL,
	[Email] [nvarchar](100) NULL,
	[Facebook] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
	[StaffGroupId] [numeric](18, 0) NULL,
	[Basicsalary] [numeric](18, 0) NULL,
	[Commission] [numeric](5, 2) NULL,
	[BeginWork] [datetime] NULL,
	[IsActive] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[StaffId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StaffGroup]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StaffGroup](
	[StaffGroupId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[StaffGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[TransactionId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BillId] [numeric](18, 0) NULL,
	[TransactionCode] [varchar](20) NULL,
	[Amount] [numeric](18, 0) NULL,
	[Note] [nvarchar](200) NULL,
	[PostedUser] [varchar](20) NULL,
	[PostedTime] [datetime] NULL,
	[LastChangeUser] [varchar](20) NULL,
	[LastChangeTime] [datetime] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransactionCode]    Script Date: 5/19/2021 5:57:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionCode](
	[TrnCodeId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[Type] [int] NULL,
	[IsActive] [bit] NULL,
	[SalonId] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[TrnCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a99e70df-74ee-4cf2-b1f3-361ed2835fc8', N'toanpt@gmail.com', 0, N'APHI50Io2Rbz8526MUsi1Mymum01sT/S9pTKjWajL0OKSK0bQ3mAF8Awlp/aucTMBw==', N'7051c9cc-9230-43a3-bac2-fd5ba53507ff', NULL, 0, 0, NULL, 0, 0, N'toanpt')
GO
SET IDENTITY_INSERT [dbo].[Bill] ON 

INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(1 AS Numeric(18, 0)), NULL, CAST(N'2021-05-17T03:49:11.783' AS DateTime), CAST(N'2021-05-17T03:49:11.783' AS DateTime), CAST(N'2021-05-17T03:49:11.783' AS DateTime), NULL, NULL, N'Ghi chú', CAST(N'2021-05-17T03:50:44.937' AS DateTime), N'AD', CAST(N'2021-05-17T04:29:15.270' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Phan Toàn', N'0963096426', NULL, NULL, NULL, NULL, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(2 AS Numeric(18, 0)), NULL, CAST(N'2021-05-16T17:00:00.000' AS DateTime), CAST(N'2021-05-17T20:21:00.000' AS DateTime), CAST(N'2021-05-17T20:21:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T04:00:02.177' AS DateTime), N'AD', CAST(N'2021-05-17T04:00:02.177' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Anh Khánh', N'0123456789', NULL, CAST(1 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(3 AS Numeric(18, 0)), NULL, CAST(N'2021-05-17T04:27:06.997' AS DateTime), CAST(N'2021-05-17T11:27:00.000' AS DateTime), CAST(N'2021-05-17T11:27:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T04:27:28.023' AS DateTime), N'AD', CAST(N'2021-05-17T04:28:24.560' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Chị Trang', N'178956242', NULL, CAST(1 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(4 AS Numeric(18, 0)), NULL, NULL, CAST(N'2021-05-17T20:21:00.000' AS DateTime), CAST(N'2021-05-17T20:21:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T04:29:59.067' AS DateTime), N'AD', CAST(N'2021-05-17T04:31:24.917' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Chị Lương Như Quỳnh', N'456789523', NULL, NULL, NULL, NULL, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(5 AS Numeric(18, 0)), NULL, NULL, CAST(N'2021-05-17T20:21:00.000' AS DateTime), CAST(N'2021-05-17T20:21:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T04:32:26.323' AS DateTime), N'AD', CAST(N'2021-05-17T04:32:34.583' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Chị Hà', N'7895623742', NULL, NULL, NULL, NULL, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(6 AS Numeric(18, 0)), NULL, CAST(N'2021-05-17T04:31:21.443' AS DateTime), CAST(N'2021-05-17T11:31:00.000' AS DateTime), CAST(N'2021-05-17T11:31:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T04:33:26.827' AS DateTime), N'AD', CAST(N'2021-05-17T04:36:16.790' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Chị Nhung', N'1212121212', NULL, NULL, NULL, NULL, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(7 AS Numeric(18, 0)), NULL, NULL, CAST(N'2021-05-17T20:21:00.000' AS DateTime), CAST(N'2021-05-17T20:21:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T04:35:53.090' AS DateTime), N'AD', CAST(N'2021-05-17T04:35:54.533' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Anh Hà văn hoàng', N'23213213213', NULL, NULL, NULL, NULL, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(8 AS Numeric(18, 0)), NULL, CAST(N'2021-05-17T07:13:14.217' AS DateTime), CAST(N'2021-05-17T07:13:14.217' AS DateTime), CAST(N'2021-05-17T07:13:14.217' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T07:13:31.170' AS DateTime), N'AD', NULL, NULL, NULL, NULL, 1, NULL, N'Thử xem tạo được không', N'12123213213', NULL, CAST(1 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(9 AS Numeric(18, 0)), NULL, CAST(N'2021-05-17T07:53:59.007' AS DateTime), CAST(N'2021-05-17T14:53:00.000' AS DateTime), CAST(N'2021-05-17T14:53:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T08:02:10.070' AS DateTime), N'AD', CAST(N'2021-05-17T08:02:10.070' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Khách hàng mới', N'12663566', NULL, NULL, NULL, NULL, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(10 AS Numeric(18, 0)), NULL, NULL, CAST(N'2021-05-17T20:21:00.000' AS DateTime), CAST(N'2021-05-17T20:21:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T08:06:33.097' AS DateTime), N'AD', CAST(N'2021-05-17T08:06:33.097' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Khách mua sp mới', N'456789', NULL, NULL, NULL, NULL, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(11 AS Numeric(18, 0)), NULL, NULL, CAST(N'2021-05-17T20:21:00.000' AS DateTime), CAST(N'2021-05-17T20:21:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-17T10:10:23.513' AS DateTime), N'AD', CAST(N'2021-05-17T10:10:23.513' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Phan toàn', N'2132131212', NULL, CAST(1 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(12 AS Numeric(18, 0)), NULL, NULL, CAST(N'2021-05-18T20:21:00.000' AS DateTime), CAST(N'2021-05-18T20:21:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-18T02:15:51.093' AS DateTime), N'AD', CAST(N'2021-05-18T02:15:51.093' AS DateTime), N'AD', NULL, NULL, 2, NULL, N'Phan Toàn', N'0963096426', NULL, CAST(1 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(13 AS Numeric(18, 0)), NULL, CAST(N'2021-05-19T09:46:11.633' AS DateTime), CAST(N'2021-05-19T16:46:00.000' AS DateTime), CAST(N'2021-05-19T16:46:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-19T09:46:20.797' AS DateTime), N'AD', NULL, NULL, NULL, NULL, 1, NULL, N'Toanpt', N'12121212121', NULL, NULL, NULL, NULL, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Bill] ([BillId], [BillCode], [Arrivaldate], [ArrivalTime], [DepartureTime], [DscPcnt], [DscAmount], [Note], [CreatedTime], [CreatedUser], [CheckinTime], [CheckinUser], [CheckoutTime], [CheckoutUser], [Status], [ChairId], [GuestName], [GuestPhone], [CustomerId], [StaffMain], [StaffShampoo], [StaffSub], [SalonId]) VALUES (CAST(14 AS Numeric(18, 0)), NULL, CAST(N'2021-05-19T09:52:34.317' AS DateTime), CAST(N'2021-05-19T16:52:00.000' AS DateTime), CAST(N'2021-05-19T16:52:00.000' AS DateTime), NULL, NULL, N'', CAST(N'2021-05-19T09:52:43.030' AS DateTime), N'AD', NULL, NULL, NULL, NULL, 1, NULL, N'Phan Toàn', N'2323232', NULL, NULL, NULL, NULL, CAST(1 AS Numeric(18, 0)))
SET IDENTITY_INSERT [dbo].[Bill] OFF
GO
SET IDENTITY_INSERT [dbo].[BillDetail] ON 

INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(6 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), CAST(500000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(7 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(500000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(8 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), CAST(60000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(9 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), CAST(20000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(17 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), CAST(500000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(18 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), CAST(60000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(19 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), CAST(20000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(21 AS Numeric(18, 0)), CAST(9 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(500000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(22 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), CAST(1000000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(26 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), CAST(500000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(27 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), CAST(500000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(28 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), CAST(100000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(85 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), CAST(500000 AS Numeric(18, 0)), 2, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(86 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), CAST(1000000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(87 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), CAST(10000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(88 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), CAST(60000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(89 AS Numeric(18, 0)), CAST(13 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), CAST(100000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(90 AS Numeric(18, 0)), CAST(13 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(200000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(91 AS Numeric(18, 0)), CAST(13 AS Numeric(18, 0)), CAST(9 AS Numeric(18, 0)), CAST(20000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(92 AS Numeric(18, 0)), CAST(14 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), CAST(100000 AS Numeric(18, 0)), 1, NULL)
INSERT [dbo].[BillDetail] ([BillDetailId], [BillId], [ItemId], [Price], [Quantity], [DetailNote]) VALUES (CAST(93 AS Numeric(18, 0)), CAST(14 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(200000 AS Numeric(18, 0)), 1, NULL)
SET IDENTITY_INSERT [dbo].[BillDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[IGroup] ON 

INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(1 AS Numeric(18, 0)), N'CAT', N'Cắt', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(2 AS Numeric(18, 0)), N'NHUOM', N'Nhuộm', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(3 AS Numeric(18, 0)), N'UON', N'Uốn', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(4 AS Numeric(18, 0)), N'GOI', N'Gội', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IGroup] ([IGroupId], [IGroupCode], [Name], [IsActice], [SalonId]) VALUES (CAST(5 AS Numeric(18, 0)), N'HAP', N'Hấp', 1, CAST(1 AS Numeric(18, 0)))
SET IDENTITY_INSERT [dbo].[IGroup] OFF
GO
SET IDENTITY_INSERT [dbo].[ItemList] ON 

INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(1 AS Numeric(18, 0)), N'SP01', N'Sản phẩm đầu tiền', CAST(100000 AS Numeric(18, 0)), 1, CAST(1 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(2 AS Numeric(18, 0)), N'SP02', N'Gội đầu tắm trắng', CAST(200000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(3 AS Numeric(18, 0)), N'SP03', N'Đắp mặt nạ', CAST(20000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(4 AS Numeric(18, 0)), N'SP04', N'Cắt tóc 60k', CAST(60000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(5 AS Numeric(18, 0)), N'SP04', N'Nhuộm 1 màu tùy ý', CAST(500000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(6 AS Numeric(18, 0)), N'SP06', N'Tên sản phầm này khá dài nè', CAST(500000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(7 AS Numeric(18, 0)), N'SP07', N'Sản phẩm mới', CAST(100000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(8 AS Numeric(18, 0)), N'SP08', N'Mới 2', CAST(10000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(9 AS Numeric(18, 0)), N'SP09', N'Mới 3', CAST(20000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(10 AS Numeric(18, 0)), N'Sp10', N'Mới nữa nè', CAST(1000000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(11 AS Numeric(18, 0)), N'SP11', N'Thêm sản phẩm này check lỗi', CAST(10000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
INSERT [dbo].[ItemList] ([ItemId], [ItemCode], [ItemName], [Price], [OpenPrice], [UnitId], [ItemGroupId], [SalonId], [IsActive]) VALUES (CAST(12 AS Numeric(18, 0)), N'SP12', N'Thêm sản phẩm', CAST(1000000 AS Numeric(18, 0)), NULL, CAST(1 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), 1)
SET IDENTITY_INSERT [dbo].[ItemList] OFF
GO
SET IDENTITY_INSERT [dbo].[IUnit] ON 

INSERT [dbo].[IUnit] ([UnitId], [UnitCode], [UnitName], [IsActice], [SalonId]) VALUES (CAST(1 AS Numeric(18, 0)), N'Luot', N'Lượt', 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[IUnit] ([UnitId], [UnitCode], [UnitName], [IsActice], [SalonId]) VALUES (CAST(2 AS Numeric(18, 0)), N'Chai', N'Chai', 1, CAST(1 AS Numeric(18, 0)))
SET IDENTITY_INSERT [dbo].[IUnit] OFF
GO
SET IDENTITY_INSERT [dbo].[Salon] ON 

INSERT [dbo].[Salon] ([SalonId], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website]) VALUES (CAST(1 AS Numeric(18, 0)), N'FAMILY', N'Hair Salon Family', N'Ngô Xá - Cẩm Khê - Phú Thọ', N'0963096426', N'family@gmail.com', N'family.FaceBook.com', N'www.family.com')
SET IDENTITY_INSERT [dbo].[Salon] OFF
GO
SET IDENTITY_INSERT [dbo].[Staff] ON 

INSERT [dbo].[Staff] ([StaffId], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (CAST(1 AS Numeric(18, 0)), N'NV01', N'Nguyễn Văn Bình', N'Địa chỉ', N'1234566789', N'email', N'facebook', NULL, CAST(1 AS Numeric(18, 0)), CAST(500000 AS Numeric(18, 0)), CAST(10.00 AS Numeric(5, 2)), CAST(N'2021-05-16T17:00:00.000' AS DateTime), 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Staff] ([StaffId], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (CAST(2 AS Numeric(18, 0)), N'NV02', N'Trần Văn Lâm', NULL, NULL, NULL, NULL, NULL, CAST(2 AS Numeric(18, 0)), CAST(600000 AS Numeric(18, 0)), CAST(15.00 AS Numeric(5, 2)), CAST(N'2021-05-16T17:00:00.000' AS DateTime), 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Staff] ([StaffId], [Code], [Name], [Address], [Phone], [Email], [Facebook], [Website], [StaffGroupId], [Basicsalary], [Commission], [BeginWork], [IsActive], [SalonId]) VALUES (CAST(3 AS Numeric(18, 0)), N'NV03', N'Nhân viên khác', NULL, NULL, NULL, NULL, NULL, CAST(3 AS Numeric(18, 0)), CAST(500000 AS Numeric(18, 0)), CAST(10.00 AS Numeric(5, 2)), CAST(N'2021-05-16T17:00:00.000' AS DateTime), 1, CAST(1 AS Numeric(18, 0)))
SET IDENTITY_INSERT [dbo].[Staff] OFF
GO
SET IDENTITY_INSERT [dbo].[Transaction] ON 

INSERT [dbo].[Transaction] ([TransactionId], [BillId], [TransactionCode], [Amount], [Note], [PostedUser], [PostedTime], [LastChangeUser], [LastChangeTime], [SalonId]) VALUES (CAST(1 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), NULL, CAST(2070000 AS Numeric(18, 0)), N'toanpt test', N'AD', CAST(N'2021-05-18T03:46:33.343' AS DateTime), NULL, NULL, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[Transaction] ([TransactionId], [BillId], [TransactionCode], [Amount], [Note], [PostedUser], [PostedTime], [LastChangeUser], [LastChangeTime], [SalonId]) VALUES (CAST(2 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), N'TM', CAST(2070000 AS Numeric(18, 0)), N'toanpt test lần 2', N'AD', CAST(N'2021-05-18T03:48:13.020' AS DateTime), NULL, NULL, CAST(1 AS Numeric(18, 0)))
SET IDENTITY_INSERT [dbo].[Transaction] OFF
GO
SET IDENTITY_INSERT [dbo].[TransactionCode] ON 

INSERT [dbo].[TransactionCode] ([TrnCodeId], [Code], [Name], [Type], [IsActive], [SalonId]) VALUES (CAST(1 AS Numeric(18, 0)), N'TM', N'Tiền mặt', 1, 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[TransactionCode] ([TrnCodeId], [Code], [Name], [Type], [IsActive], [SalonId]) VALUES (CAST(2 AS Numeric(18, 0)), N'CK', N'Chuyển khoản', 1, 1, CAST(1 AS Numeric(18, 0)))
INSERT [dbo].[TransactionCode] ([TrnCodeId], [Code], [Name], [Type], [IsActive], [SalonId]) VALUES (CAST(3 AS Numeric(18, 0)), N'TM/CK', N'Tiền mặt/Chuyển khoản', 1, 1, CAST(1 AS Numeric(18, 0)))
SET IDENTITY_INSERT [dbo].[TransactionCode] OFF
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
